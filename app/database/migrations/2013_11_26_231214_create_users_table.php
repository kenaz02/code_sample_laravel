<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->string('fullname')->default("");
			$table->string('email')->default("");
			$table->string('password')->default("");
			$table->string('avatar')->default("");
			$table->integer('late')->default(0);
			$table->integer('ontime')->default(0);


			$table->integer('reminder1')->default(0);
			$table->integer('reminder2')->default(0);

			$table->string('tmpkey')->default("");
			$table->boolean('activated')->default(false);

			$table->string('device_token')->default("");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
