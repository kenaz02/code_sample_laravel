<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('sender');
			$table->integer('recipient');
			$table->string('name');
			$table->date('date');
			$table->time('time');
			$table->string('location');
			$table->string('lat');
			$table->string('lng');
			$table->string('rewards');
			$table->integer('reward')->default(0);
			$table->integer('repeating');
			$table->integer('ontime_count')->default(0);
			$table->integer('flag')->default(0);
			$table->string('message')->default("");
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
