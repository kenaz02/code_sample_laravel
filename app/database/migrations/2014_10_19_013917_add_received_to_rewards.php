<?php

use Illuminate\Database\Migrations\Migration;

class AddReceivedToRewards extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rewards', function($table)
		{
			$table->integer('received')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rewards', function($table)
		{
			 $table->dropColumn('received');
		});		
	}

}