<?php

use Illuminate\Database\Migrations\Migration;

class UserHasInviteReference extends Migration {

	public function up()
	{
		Schema::table('users', function($table)
		{
			$table->integer('invitation')->default(0);
		});
	}

	public function down()
	{
		Schema::table('users', function($table)
		{
			 $table->dropColumn('invitation');
		});		
	}

}