<?php

use Illuminate\Database\Migrations\Migration;

class MultiplePartnerInvitations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invitations', function($table)
		{
			$table->increments('id');
			$table->timestamps();
			$table->integer('inviter')->default(0);
			$table->integer('invitee')->default(0);
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invitations');
	}

}