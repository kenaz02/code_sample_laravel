<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
		Eloquent::unguard();
		$this->call('UserTableSeeder');
		$this->call('InvitationTableSeeder');
		$this->call('EventsTableSeeder');
		$this->call('MessagesTableSeeder');
		$this->call('RewardsTableSeeder');
	}

}