<?php

class InvitationTableSeeder extends Seeder {

    public function run()
    {
        Invitation::truncate();
        $invitee = User::where("email","=","invitee@gmail.com")
        ->first()
        ->id;



        $inviter1 = User::where("email","=","inviter1@gmail.com")
        ->first()
        ->id;
        Invitation::create([
		   'invitee' => $invitee,
		   'inviter' => $inviter1
		]);

        $inviter2 = User::where("email","=","inviter2@gmail.com")
        ->first()
        ->id;
        Invitation::create([
		   'invitee' => $invitee,
		   'inviter' => $inviter2
		]);
    }
}