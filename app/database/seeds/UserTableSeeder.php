<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        User::truncate();
        User::create([
		   'fullname' => 'Jackson Guineepig Beale',
		   'email' => 'invitee@gmail.com',
		   'password' => Hash::make("test12345678"),

		   'created_at' => '',
		   'updated_at' => '',

		   'partner' => 0,
		   'invitation' => 0,

		   'activated' => 1,
		   'tmpkey' => ''
		]);
        User::create([
		   'fullname' => 'Inviter-one User',
		   'email' => 'inviter1@gmail.com',
		   'password' => Hash::make("test12345678"),

		   'created_at' => '',
		   'updated_at' => '',

		   'partner' => 0,
		   'invitation' => 0,

		   'tmpkey' => '',
		   'activated' => 1
		   
		]);
        User::create([
		   'fullname' => 'Inviter-two User',
		   'email' => 'inviter2@gmail.com',
		   'password' => Hash::make("test12345678"),

		   'created_at' => '',
		   'updated_at' => '',

		   'partner' => 0,
		   'invitation' => 0,

		   'tmpkey' => '',
		   'activated' => 1
		   
		]);
    }
}