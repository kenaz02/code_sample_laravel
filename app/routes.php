<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*=============================
=            Users            =
=============================*/
Route::get("/uat", function(){
	return View::make('main');
});

//Route::get('/handleCrontab', "EventController@reminder");

Route::resource('/users', 'UserController');
Route::post('/activate', 'UserController@activate');
Route::post('/login', 'UserController@login');
Route::post('/lostpwd', 'UserController@lostpwd');

Route::group(array('before' => 'auth'), function()
{
	Route::post('/settoken', 'UserController@settoken');
	Route::post('/setpwd', 'UserController@setpwd');
	Route::get('/disconnect', 'UserController@disconnect');
	Route::get('/logout', 'UserController@logout');

	Route::post('/profile', 'UserController@profile');
	Route::post('/reminders', 'UserController@reminders');

	Route::resource('/invitations', 'InvitationController');
	Route::get("/invitations/{id}/accept", "InvitationController@accept");
	Route::get("/invitations/{id}/deny", "InvitationController@deny");

	Route::resource('/events', 'EventController');
	Route::get("/events/{id}/delete", "EventController@destroy");
	Route::post("/events/{id}/suggest","EventController@suggest");
	Route::post("/events/{id}/reject", "EventController@reject");
	Route::get("/events/{id}/cancel", "EventController@cancel");
	Route::post("/events/{id}/update","EventController@update");

	//todo list
	////fix expand into multiple cards if( 1 of 3 etc...)
	Route::post("/events/{id}/accept", "EventController@accept"); 
	Route::post("/events/{id}/heldup", "EventController@heldup");

	Route::get("/events/{id}/ontime", "EventController@ontime");
	Route::get("/events/{id}/late", "EventController@late"); 

	Route::post("/rewards/{id}/received", "RewardsController@received");
	Route::post("/rewards/{id}/not_received", "RewardsController@not_received");

});


Route::resource('messages', 'MessagesController');

Route::resource('rewards', 'RewardsController');