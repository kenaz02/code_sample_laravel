<?php

class RewardsController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('rewards.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('rewards.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('rewards.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('rewards.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}
	
	public function getReward($id){
		$reward = Reward::where("id", "=", $id)
		->where("user","=",Auth::user()->id)
		->orWhere("partner","=",Auth::user()->id)
		->where("id", "=", $id);
		
		if($reward->count()) return $reward->first();
		else return 0;
	}

	public function received($id){

		//$status = Input::get('status');
		//echo $status.'xxfv';
		if($reward = $this->getReward($id))
		{
			$reward->received = 1;
			$reward->save();
		}
		return Response::json(null, 200);	
	}
		
	public function not_received($id){
		if($reward = $this->getReward($id))
		{
			$reward->received = 2;
			$reward->save();
		}
		return Response::json(null, 200);	
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
