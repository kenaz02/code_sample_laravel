<?php

class InvitationController extends BaseController {

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	public function index()
	{
	    $invitations = Auth::user()->invitations();
	    return Response::json($invitations, 200);
	}

	public function store(){
		$form = array(
			"email" => strtolower(e(Input::get('email'))),
		);
		$form["email"] = str_replace(' ', '', $form["email"]);
		$validator = Validator::make( $form, Invitation::$validationRules["invite"] );
		if ($validator->fails()) 
			return Response::json(null, 400);

		//fix Auth::user() can't have a an existing partner
		//fix can only send one invitation at a time
		if( $form["email"] == Auth::user()->email ) 
			return Response::json(null, 400);

        $firstname_inv = explode(" ", Auth::user()->fullname);

		$invitation = Invitation::send($form["email"], $firstname_inv[0]);
		if( ! $invitation ) return Response::json(null,400);
		Auth::user()->invitation = $invitation->id;
		Auth::user()->save();

		return Response::json($invitation, 201);
	}
	/**
	 * [isUsers description]
	 * @param  [type]  $id [description]
	 * @return boolean     [description]
	 */

	public function accept($id){
		$invitation = Invitation::where("invitee", "=", Auth::user()->id)
		->where("id", "=", $id);
		if( !$invitation->count() )
			return Response::json(null, 400);

		$invitation = $invitation->first();

		$invitee = Auth::user();
		$inviter = $invitation->inviter();
		$invitee->partner = $inviter->id;
		$inviter->partner = $invitee->id;
		$inviter->invitation = 0;
		$inviter->save();
		$invitee->save();

		$invitation->delete();
		return Response::json(null, 200);

	}

	public function deny($id){
		$invitation = Invitation::where("invitee", "=", Auth::user()->id)
		->where("id", "=", $id);
		if( !$invitation->count() )
			return Response::json(null, 400);
		
		$invitation = $invitation->first();
		$inviter = $invitation->inviter();
		$inviter->invitation = 0;
		$invitation->delete();
		return Response::json(null, 200);
	}

	public function destroy($id)
	{
		$invitation = Invitation::where("id","=",$id)
		->where("inviter", "=", Auth::user()->id);

		if( $invitation->count() ){
			$invitation->first()->delete();
			Auth::user()->invitation = 0;
			Auth::user()->save();
		}
		
		return Response::json(null, 200);
	}

}