<?php

class UserController extends BaseController {

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	public function index()
	{
   
	    // Make sure current user owns the requested resource
	    if( ! Auth::check() ) return Response::json(null, 403);

	   	$data = Auth::user()->getData();

	    return Response::json($data, 200);
	}

	public function store()
	{
		$form = array(
			"email" => strtolower(e(Input::get('email'))),
			"fullname" => e(Input::get('fullname'))
		);
		$form["email"] = str_replace(' ', '', $form["email"]);
		$validator = Validator::make( $form, User::$validationRules["store"] );

		if ($validator->fails())
			return Response::json(null, 400);
		if(User::where('email', '=', $form["email"])->where('activated', '=', 1)->count())
			return Response::json(null, 400);

		$user = User::where('email', '=', $form["email"]);
		if( $user->count() ) $user = $user->first(); else $user = new User();
		$user->email = $form["email"];
		$user->fullname = $form["fullname"];
		$tmpkey = $user->generateTemporaryKey();


		//Auth::loginUsingId($user->id);
		$fnarr = explode(" ", $user->fullname);
		$data = array(
			"user" => $user,
			"firstname" => $fnarr[0],
			"tmpkey" => $tmpkey
		);

		Mail::queue('emails.auth.activate', $data, function($message) use ($user)
		{
		    $message->to($user->email)->subject('Activate Held Up');
		});

		return Response::json(null, 201);

	}


	public function lostpwd()
	{
		$form = array(
			"email" => strtolower(e(Input::get('email')))
		);
		$form["email"] = str_replace(' ', '', $form["email"]);
		$validator = Validator::make( $form, User::$validationRules["lostpwd"] );
		if ($validator->fails()) 
			return Response::json(null, 400);

		$user = User::where('email', '=', $form["email"]);
		if( $user->count() ) $user = $user->first(); else return Response::json(null, 400);
		$user->email = $form["email"];
		$tmpkey = $user->generateTemporaryKey();

		$data = array(
			"user" => $user,
			"tmpkey" => $tmpkey
		);

		Mail::queue('emails.auth.fpw', $data, function($message) use ($user)
		{
		    $message->to($user->email)->subject('Held Up Password Reset');
		});

		return Response::json(null, 201);

	}


	public function profile(){
     
		$form = array(
			"avatar" => Input::get('avatar'),
			"name" => e(Input::get('name'))
		);
                
		$user = Auth::user();
                 
		if( $form["avatar"] != "" ){
			list($type, $form["avatar"]) = explode(';', $form["avatar"]);
			list(, $form["avatar"])      = explode(',', $form["avatar"]);
			file_put_contents('images/avatars/'. $user->id .'.jpg', base64_decode($form['avatar']));
			$user->avatar = 'images/avatars/'. $user->id .'.jpg';
			$user->save();
		}
		if( $form["name"] != "" ){
			$user->fullname = $form["name"];
			$user->save();
		}
			


		return Response::json(null, 200);
	}


	public function reminders(){
		$form = array(
			"reminder-first" => Input::get('reminder-first'),
			"reminder-secondary" => Input::get('reminder-secondary')
		);
		$user = Auth::user();
		if( $form["reminder-first"] != "" ){
			$user->reminder1 = $form["reminder-first"];
			$user->save();
		}
		if( $form["reminder-secondary"] != "" ){
			$user->reminder2 = $form["reminder-secondary"];
			$user->save();
		}
		return Response::json(null, 200);
	}

	public function activate()
	{
		$form = array(
			"key" => e(Input::get('key')),
		);

		$form["key"] = str_replace(' ', '', $form["key"]);
		$validator = Validator::make( $form, User::$validationRules["activate"] );
		if ($validator->fails()) 
			return Response::json(null, 400);

		$user;
		if( !$user = User::findByTemporaryKey($form["key"]) )
			return Response::json(null, 400);

		$user->activated = 1;
		$user->save();
		Auth::login($user);

		$data = Auth::user()->getData();
		return Response::json($data, 200);

	}

	public function setpwd()
	{
		$form = array(
			"password" => e(Input::get('password'))
		);

		$validator = Validator::make( $form, User::$validationRules["setpwd"] );
		if ($validator->fails()) 
			return Response::json(null, 400);
		$user = Auth::user();
		$user->password = Hash::make($form["password"]);
		if( !$user->activated )
			$user->activate();
		else $user->save();
		$data = Auth::user()->getData();
		return Response::json($data, 200);
	}


	public function settoken()
	{
		$user = Auth::user();
		$user->device_token = Input::get('token');
		$user->save();
		return Response::json(null, 200);
	}

	public function login()
	{
		$form = array(
			"email" => strtolower(e(Input::get('email'))),
			"password" => e(Input::get('password'))
		);

		$form["email"] = str_replace(' ', '', $form["email"]);
		$validator = Validator::make( $form, User::$validationRules["login"] );

		if (! $validator->fails())
		if (Auth::attempt(array('email' => $form["email"], 'password' => $form["password"])))
		{
			if( Auth::user()->activated ){
				$data = Auth::user()->getData();
				return Response::json($data, 200);
			}
		}

		return Response::json(null, 403);
	}

	public function disconnect(){
		$user = Auth::user();
		$partner = $user->partner();

		if( $partner )
		{

			$ev = EventModel::where("sender","=",Auth::user()->id)
			->orWhere("recipient","=",Auth::user()->id);
			$ev->delete();
			$rw = Reward::where("user","=",Auth::user()->id)
			->orWhere("partner","=",Auth::user()->id);
			$rw->delete();
			$partner->ontime = 0;
			$partner->late = 0;

			$partner->partner = 0;
			$partner->save();

			$user->ontime = 0;
			$user->late = 0;
			$user->partner = 0;
			$user->save();

			return Response::json(null, 200);
		}
		return Response::json(null, 400);
	}

	public function logout()
	{
		Auth::logout();
		return Response::json(null, 200);
	}

}