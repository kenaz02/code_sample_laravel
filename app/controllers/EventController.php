<?php

class EventController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	    $events = Auth::user()->events();
            dd(11);
	    return Response::json($events, 200);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$form = [
			'name'        => e(Input::get('name')),
			'date'    => e(Input::get('date')),
			'time'    => e(Input::get('time')),
			'location'    => e(Input::get('location')),
			'lat' => e(Input::get('lat')),
			'lng' => e(Input::get('lng')),
			'reward1'     => e(Input::get('reward1')),
			'reward2'     => e(Input::get('reward2')),
			'reward3'     => e(Input::get('reward3')),
			'repeating'   => e(Input::get('repeating'))
		];

		$validator = Validator::make( $form, EventModel::$validationRules["store"] );
		if ($validator->fails()) 
			return Response::json(null, 400);

		$event            = new EventModel();
		$event->sender    = Auth::user()->id;
		$event->recipient  = Auth::user()->partner;
		$event->name      = $form["name"];
		$event->date      = $form["date"];
		$event->time      = $form["time"];
		$event->location  = $form["location"];
		$event->lat       = $form["lat"];
		$event->lng       = $form["lng"];
		$rewards          = [
			$form["reward1"], 
			$form["reward2"], 
			$form["reward3"]
		];
		//print_r($rewards);
		$rewards = array_values(array_filter($rewards));
		//print_r($rewards);
		$event->rewards   = json_encode($rewards);
		$event->repeating = $form["repeating"];
		$event->setFlag( array( "sent" => "pending", "recv" => "proposal" ) );
		$event->save();

        //Send push notification
        //$audience["alias"] = Auth::user()->partner; //UrbanAirship
        $partner = User::find(Auth::user()->partner);
        $audience = $partner->device_token;
        $event_name = 'Incoming! Your S.O. has sent you a new invitation.'."\n'".$event->name."'";
        pushNotification($event_name, $audience);

		return Response::json($event->toArray(), 201);
	}

	public function update($id){
		$form = [
			'name'        => e(Input::get('name')),
			'date'    => e(Input::get('date')),
			'time'    => e(Input::get('time')),
			'location'    => e(Input::get('location')),
			'lat' => e(Input::get('lat')),
			'lng' => e(Input::get('lng'))
		];

		$event = EventModel::where("sender","=",Auth::user()->id)->where("id","=",$id);
		if( !  $event->count() )
			return Response::json(null, 400);
		$event = $event->first();

		if( $form["date"] != null && $form["date"] != $event->date){
			if( $event->flag & EventModel::$flagoption["recv"]["idle"] ){
				$event->resetFlag();
				$event->setFlag( array( "sent" => "idle", "recv" => "changed" ) );
			}
			if( $event->flag & EventModel::$flagoption["sent"]["idle"] )
				$event->flag |= EventModel::$flagoption["msg"]["changedDatetime"];
			$event->date      = $form["date"];
            $event_name = "Can't make the date. Let's try for another!"."\n'".$event->name."'";
		}
		if( $form["time"] != null && $form["time"] != $event->time ){
			if( $event->flag & EventModel::$flagoption["recv"]["idle"] ){
				$event->resetFlag();
				$event->setFlag( array( "sent" => "idle", "recv" => "changed" ) );
			}
			if( $event->flag & EventModel::$flagoption["sent"]["idle"] )
				$event->flag |= EventModel::$flagoption["msg"]["changedDatetime"];
			$event->time      = $form["time"];
            $event_name = "Can't make the date. Let's try for another!"."\n'".$event->name."'";
		}
		if( $form["location"] != null && $form["location"] != $event->location ){

			if( $event->flag & EventModel::$flagoption["recv"]["idle"] ){
				$event->resetFlag();
				$event->setFlag( array( "sent" => "idle", "recv" => "changed" ) );
			}
			if( $event->flag & EventModel::$flagoption["sent"]["idle"] )
				$event->flag |= EventModel::$flagoption["msg"]["changedLocation"];
			$event->location  = $form["location"];
            $event_name = "New location request! Your S.O. wants to try somewhere a little different."."\n'".$event->name."'";
		}
		
		if( $form["lat"] != null )
		$event->lat       = $form["lat"];
		if( $form["lng"] != null )
		$event->lng       = $form["lng"];

		$event->save();

        //Send push notification
        //$audience["alias"] = Auth::user()->partner; //UrbanAirship
        $partner = User::find(Auth::user()->partner);
        $audience = $partner->device_token;
        pushNotification($event_name, $audience);

        return Response::json($event->toArray(), 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id){
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */


	public function getEvent($id){
		$event = EventModel::where("id", "=", $id)
		->where("sender","=",Auth::user()->id)
		->orWhere("recipient","=",Auth::user()->id)
		->where("id", "=", $id);
		
		if($event->count()) return $event->first();
		else return 0;
	}
	
	public function destroy($id){
		if($event = $this->getEvent($id))
		{
			$event->resetFlag();

			if($event->isSender())
				$event->setFlag(array("sent" => "deleted", "recv"=>"deleted"));

			$event->save();
		}
		return Response::json(null, 200);
	}

	public function accept($id){
		if($event = $this->getEvent($id))
		{
			$event->resetFlag();
			$event->setFlag(array("sent"=>"idle", "recv"=>"idle"));

			$val = intval(e(Input::get("reward-$id")));

			$event->reward = json_decode($event->rewards)[$val-1];

			$event->save();

            //Send push notification
            //$event_name = 'Reward '.$event->rewards.' accepted for event: '.$event->name;
            //$audience["alias"] = Auth::user()->partner;
            //pushNotification($event_name, $audience);

			return Response::json(null, 200);
		}
		return Response::json(null, 400);
		
	}
	
	public function late($id){
		if($event = $this->getEvent($id))
		{
			$partner = Auth::user()->partner();
			$partner->late++;
			$partner->save();
			$event->resetFlag();
			$event->setFlag(array("sent"=>"idle", "recv"=>"late"));
			$event->save();

            //Send push notification
            //$audience["alias"] = Auth::user()->partner; //UrbanAirship
            $partner = User::find(Auth::user()->partner);
            $audience = $partner->device_token;
            $event_name = "Bad news darling. Looks like you were Too Late. Better luck next time!"."\n'".$event->name."'";
            pushNotification($event_name, $audience);
        }
		return Response::json(null, 200);
	}

	public function onTime($id){
		if($event = $this->getEvent($id))
		{
			$event->ontime_count++;
			$event->resetFlag();
			$event->setFlag(array("sent"=>"idle", "recv"=>"idle"));
			if( $event->ontime_count > $event->repeating ){
				$event->resetFlag();
				$event->setFlag(array("sent"=>"idle", "recv"=>"ontime"));
				$event->save();
				$partner = Auth::user()->partner();
				$partner->ontime++;
				$partner->save();
				$reward = new Reward();
				$reward->user = Auth::user()->id;
				$reward->partner = Auth::user()->partner;
				$reward->name = $event->reward;
				$reward->event = $event->name;
				$reward->date = $event->date . " " . $event->time;
				$reward->save();
			}
			$event->save();

            //Send push notification
            //$audience["alias"] = Auth::user()->partner; //UrbanAirship
            $partner = User::find(Auth::user()->partner);
            $audience = $partner->device_token;
            $event_name = "Well Done You! You were On Time."."\n'".$event->name."'";
            pushNotification($event_name, $audience);
		}
		return Response::json(null, 200);
	}

	public function cancel($id){

		if($event = $this->getEvent($id))
		{
			$event->resetFlag();

			if($event->isSender())
				$event->setFlag(array("sent" => "idle", "recv"=>"deleted"));

			$event->save();

			return Response::json(null, 200);
		}
	}

	public function reject($id){
		if($event = $this->getEvent($id))
		{
            //$reject = 0;
            if ($event->flag == 129)
                $event_name     = "What happened here? Your S.O. has deleted an event. :-("."\n'".$event->name."'";
            if ($event->flag == 66){
                //$event_name = 'Reward '.$event-> rewards.' rejected for event: '.$event->name.'. Reason: ';
                $event_name = "Your S.O. has other places to go. But, I'm sure they've got a good reason..."."\n'".$event->name."'";
                //$reject = 1;
            }
			$event->resetFlag();
			if($event->isSender()){
				$event->setFlag(array("sent"=>"idle", "recv"=>"deleted"));
            }
			else{
				$event->setFlag(array("sent"=>"deleted", "recv"=>"idle"));
            }
			$event->message = e(Input::get('message'));
			$event->save();

            //if ($reject == 1)
            //    $event_name = $event_name.$event->message;

            //Send push notification
            //$audience["alias"] = Auth::user()->partner; //UrbanAirship
            $partner = User::find(Auth::user()->partner);
            $audience = $partner->device_token;
            pushNotification($event_name, $audience);

		}
		return Response::json(null, 200);		
	}

	public function heldup($id){
		if($event = $this->getEvent($id))
		{
			$event->resetFlag();
			$event->setFlag(array("sent"=>"heldup", "recv"=>"idle"));
			$event->message = e(Input::get('message'));
			$event->save();

            //Send push notification
            $event_name = str_replace('&rsquo;','’',$event->message)."\n'".$event->name."'";
            //$audience["alias"] = Auth::user()->partner; //UrbanAirship
            $partner = User::find(Auth::user()->partner);
            $audience = $partner->device_token;
            pushNotification($event_name, $audience);

		}
		return Response::json(null, 200);	
	}

	public function suggest($id){
		if($event = $this->getEvent($id))
		{
			$form = [
				'reward1'        => e(Input::get('reward1')),
			];
			$rewards         = [
				$form["reward1"]
			];
			$event->rewards   = json_encode($rewards);
			$event->resetFlag();

			if($event->isSender())
				$event->setFlag(array("sent"=>"pending", "recv"=>"proposal"));
			else
				$event->setFlag(array("sent"=>"proposal", "recv"=>"pending"));

			$event->save();

            //Send push notification
            $event_name = "The sweetner wasn't sweet enough, sweet cheeks - let's renegotiate!"."\n'".$event->name."'";
            //$audience["alias"] = Auth::user()->partner; //UrbanAirship
            $partner = User::find(Auth::user()->partner);
            $audience = $partner->device_token;
            pushNotification($event_name, $audience);

            return Response::json(null, 200);
		}
		return Response::json(null, 400);
	}

    /*
	public function reminderCrontab()
    {
        //Get date and time
        //$today_date = date("Y-m-d", strtotime(date("Y-m-d")));
        //$today_time = date("H:i", strtotime(date("H:i")));
        $date = date("Y-m-d H:i", strtotime(date("Y-m-d H:i").'+ 10 hours'));
        //error_log($date);
        $today_date = substr($date, 0, 10);
        $today_time = substr($date, -5);
        //error_log($today_date);
        //error_log($today_time);
        //$today_date = date("Y-m-d", strtotime('2014-11-17'));
        //$today_time = date("H:i", strtotime(date("19:42")));
        //error_log($today_date);
        //error_log($today_time);


        for ($indexReminder = 2; $indexReminder <= 9; $indexReminder++) {
            //error_log('index '.$indexReminder);
            $new_date = $today_date;
            $new_time = $today_time;
            switch ($indexReminder) {
                //case 1: {break;} //Exact time of event
                case 2: {$new_time = date('H:i', strtotime($today_time . ' + 5 minutes'));break;}
                case 3: {$new_time = date('H:i', strtotime($today_time . ' + 15 minutes'));break;}
                case 4: {$new_time = date('H:i', strtotime($today_time . ' + 30 minutes'));break;}
                case 5: {$new_time = date('H:i', strtotime($today_time . ' + 1 hour'));break;}
                case 6: {$new_time = date('H:i', strtotime($today_time . ' + 2 hours'));break;}
                case 7: {$new_date = date('Y-m-d', strtotime($today_date . ' + 1 day'));break;}
                case 8: {$new_date = date('Y-m-d', strtotime($today_date . ' + 2 days'));break;}
                case 9: {$new_date = date('Y-m-d', strtotime($today_date . ' + 1 week'));break;}
            }

            //error_log($new_date);
            //error_log($new_time);

            $events = DB::select('SELECT events.name, events.sender, events.recipient, events.date, events.time,
                                        sender.reminder1 as reminder1sender, sender.reminder2 as reminder2sender,
                                        recipient.reminder1 as reminder1recipient, recipient.reminder2 as reminder2recipient
                                    FROM events
                                    LEFT JOIN users as sender ON events.sender = sender.id
                                    LEFT JOIN users as recipient ON events.recipient = recipient.id
                                    WHERE events.flag != 1028
                                        AND events.date = ?
                                        AND events.time = ?
                                        AND (reminder1sender = ? OR reminder2sender = ? OR reminder1recipient = ? OR reminder2recipient = ?)',
                                    array($new_date, $new_time, $indexReminder, $indexReminder, $indexReminder, $indexReminder));

            //error_log(count($events));
            foreach ($events AS $event) {
                if ($event->reminder1sender == $indexReminder or $event->reminder2sender == $indexReminder){
                        switch ($indexReminder) {
                            //case 1: {$reminderS = ' now!';}
                            case 2: {$reminderS = 'in 5 minutes!';break;}
                            case 3: {$reminderS = 'in 15 minutes!';break;}
                            case 4: {$reminderS = 'in 30 minutes!';break;}
                            case 5: {$reminderS = 'in 1 hour!';break;}
                            case 6: {$reminderS = 'in 2 hours!';break;}
                            case 7: {$reminderS = 'in 1 day!';break;}
                            case 8: {$reminderS = 'in 2 days!';break;}
                            case 9: {$reminderS = 'in 1 week!';break;}
                        }
                        //Send push notification
                        $event_name = 'Reminder: '.$event->name.' '.$reminderS;
                        $audience["alias"] = $event->sender;
                        pushNotification($event_name, $audience);
                }
                if ($event->reminder1recipient == $indexReminder or $event->reminder2recipient == $indexReminder){
                        switch ($indexReminder) {
                            //case 1: {$reminderR = ' now!';}
                            case 2: {$reminderR = 'in 5 minutes!';break;}
                            case 3: {$reminderR = 'in 15 minutes!';break;}
                            case 4: {$reminderR = 'in 30 minutes!';break;}
                            case 5: {$reminderR = 'in 1 hour!';break;}
                            case 6: {$reminderR = 'in 2 hours!';break;}
                            case 7: {$reminderR = 'in 1 day!';break;}
                            case 8: {$reminderR = 'in 2 days!';break;}
                            case 9: {$reminderR = 'in 1 week!';break;}
                        }
                        //Send push notification
                        $event_name = 'Reminder: '.$event->name.' '.$reminderR;
                        $audience["alias"] = $event->recipient;
                        pushNotification($event_name, $audience);
                }
            }



        }

        //Send reminder only to sender, remind to say if the partner is ontime or late
        $new_date = $today_date;
        $new_time = $today_time;
        error_log($new_date);
        error_log($new_time);
        //Date and time = Exact date and time of event
        $events = DB::select('SELECT events.name, events.sender, events.date, events.time
                              FROM events
                              WHERE events.flag != 1028
                                AND events.date = ?
                                AND events.time = ?', array($new_date, $new_time));

        //error_log(count($events));
        foreach ($events AS $event) {
            //Send push notification
            $event_name = "Don't forget - you have some work to do!<br />Let your S.O. know if they made it on time.";
            $audience["alias"] = $event->sender;
            pushNotification($event_name, $audience);
        }



    }
    */
}
