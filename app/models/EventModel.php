<?php

class EventModel extends Eloquent {
	protected $table = 'events';
	static $validationRules = [
		"store" => [
			'name'        => 'required',
			'date'    => 'required',
			'time'    => 'required',
			'location'    => 'required',
			'lat' => 'required',
			'lng' => 'required',
			'reward1'     => 'required',
			//'reward2'     => 'required',
			//'reward3'     => 'required',
			'repeating'   => 'required'
		]
  ];

  static $flagoption = [
  	"sent" => [
  		"pending" => 1,
  		"proposal" => 2,
  		"idle" => 4,
  		"past" => 8,
      "deleted" => 16,
      "heldup" => 32
  	],
  	"recv" => [
  		"pending" => 64,
  		"proposal" => 128,
  		"idle" => 256,
  		"changed" => 512,
      "deleted" => 1024,
      "ontime" => 2048,
      "late" => 4096, 
  	],
    "msg" => [
      "changedLocation" => 8192,
      "changedDatetime" => 16384
    ]
  ];

  public function resetFlag(){
    $this->flag = 0;
  }
  
  public function setFlag($flags){
    foreach( $flags as $section => $flag ){
      $this->flag |= static::$flagoption[$section][$flag];
    }
  }
  
  public function isSender(){
  	return Auth::user()->id == $this->sender;
  }
  
  public function isRecipient(){
  	return Auth::user()->id == $this->recipient;
  }
  
}
