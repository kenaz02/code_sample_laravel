<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {
	static $validationRules = array(
		"store" => array(
	        'fullname' => 'required',
	        'email' => 'required|email'		
		),
		"lostpwd" => array(
	        'email' => 'required|email'		
		),
		"activate" => array(
			'key' => 'required|min:4',
		),

		"setpwd" => array(
	        'password' => 'required|min:8'	
		),
		"invite" => array(
	        'email' => 'required|email'
		),
		"login" => array(
			'password' => 'required|min:8',
			'email' => 'required|email'	
		)


    );


	static $states = array(
		"pending" => 1,
	);


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'tmpkey');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}


	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

    public function partner()
    {
    	$partner = User::where("partner", '=', Auth::user()->id)->get();
	    if( ! count($partner) ) 
    	return 0;	
    	$partner = $partner->first();

        return $partner;
    }

    public function invitations()
    {
    	$invitations = Invitation::where("invitee", '=', $this->id)->get();
   	    foreach($invitations as &$invitation){
	    	$invitation->inviter = User::find($invitation->inviter)->toArray();
	    }
	    
	    if( ! count($invitations) ) 
	    	return 0;
        return $invitations;
    }

    public function invitation()
    {
    	$invitation = Invitation::where("inviter", '=', $this->id)->get();
	    if( ! count($invitation) ) 
    	return 0;	
    	$invitation = $invitation->first();

	    $invitation->invitee = User::find($invitation->invitee)->toArray();

        return $invitation;
    }

    public function generateTemporaryKey()
    {
		$tmpkey = str_random(4);
		$this->tmpkey = $tmpkey;
		$this->save();
		return $tmpkey;
    }

    public static function findByTemporaryKey($key)
    {
    	$user = User::where( "tmpkey", "=", $key );
    	if( $user->count())
    		return $user->first();
    	return NULL;
    }

    public function activate()
    {
		$this->activated = true;
		$this->save();
    	return $this->activated;
    }


    public function events()
    {
    	$events = EventModel::where("sender", '=', $this->id)->
    	orWhere("recipient",'=',$this->id)->get();

	    if( ! count($events) ) 
	    	return 0;
        return $events;
    }
    public function rewards()
    {
    	$rewards = Reward::where("user", '=', $this->id)->
    	orWhere("partner",'=',$this->id)->get();

	    if( ! count($rewards) ) 
	    	return 0;
        return $rewards;
    }
    public function getData(){
    	$data = Auth::user()->toArray();
    	if(  $this->invitations() )
	   	$data["invitations"] = $this->invitations()->toArray();
	  	else $data["invitations"] = 0;

	   	if(  $this->invitation() )
	   	$data["invitation"] = $this->invitation()->toArray();
	    else $data["invitation"] = 0;

	   	if(  $this->partner() )
	   	$data["partner"] = $this->partner()->toArray();
	   	else $data["partner"] = 0;

	   	if(  $this->events() )
	   	$data["events"] = $this->events()->toArray();
	    else $data["events"] = 0;

	   	if(  $this->rewards() )
	   	$data["rewards"] = $this->rewards()->toArray();
	    else $data["rewards"] = 0;

	   	return $data;
    }

}