<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Invitation extends Eloquent {
	static $validationRules = array(
		"invite" => array(
	        'email' => 'required|email'
		)
    );

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'invitations';

	public function invitee()
    {
        return User::where("id", '=', $this->invitee)->first();
    }
	public function inviter()
    {
        return User::where("id", '=', $this->inviter)->first();
    }

    public static function send($email, $invitername)
    {

        $invitee = User::where("email", "=", $email);

    	if( ! $invitee->count() ){

    		$data = array(
				"user" => Auth::user(),
                "firstname" => $invitername,
                "email" => $email
			);
	 		Mail::send('emails.invitation', $data, function($message) use ($email)
			{
			    $message->to($email)->subject('Your Held Up Invitation');
			});

			$invitee = new User();
			$invitee->email = $email;
			$invitee->activated = 0;
			$invitee->save();
    	}
    	else
    	{
    		$invitee = $invitee->first();
    		if($invitee->partner)
    			return null;
    	}


    	$invitation = new Invitation();
    	$invitation->invitee = $invitee->id;
    	$invitation->inviter = Auth::user()->id;
    	$invitation->save();

		return $invitation;
    }

}