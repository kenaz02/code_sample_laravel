<!doctype html>
<html class="no-js ui-mobile-rendering" lang="en">
<head>
	<title>Backbone.js, Require.js, and jQuery Mobile</title>
	<meta name="description" content="">

	<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=320, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/> 	<!-- <link rel="stylesheet" href="css/vendor/jquery.mobile-1.4.0-rc.1.css" /> -->

	<link rel="stylesheet" href="css/fonts/ss-symbolicons-block.css">

	<link rel="stylesheet" href="css/vendor/gridcore.css" />
	<link rel="stylesheet" href="css/vendor/animations.css" />
	<link rel="stylesheet" href="css/theme/theme.css" />

    <script src="js/libs/exif.js"></script>
    <script src="js/libs/binaryajax.js"></script>
	<link rel="stylesheet" href="css/fonts/font-awesome.min.css">

    <script src="js/libs/require.js" data-main="js/app"></script>
</head>
<body>
	<div class="footer">
		<input id="upload" type="file" accept="image/*" capture="camera">
		<div class="r">
			<a href="#dashboard" class="size active">
				<i class="ss-home"></i>
			</a>
			<a href="#statistics" class="size">
				<i class="ss-piechart"></i>
			</a>
			<a href="#rewards" class="size">
				<i class="ss-trophy"></i>
			</a>
			<a href="#settings" class="size">
				<i class="ss-settings"></i>
			</a>
		</div>
	</div>
</body>
</html>
