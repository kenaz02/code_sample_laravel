<meta http-equiv="content-type" content="text/html; charset=utf-8" /><style type="text/css">
    /* iPad Text Smoother */  div, p, a, li, td { -webkit-text-size-adjust:none; }
    /* This is the color to change the links #0290fa; */    .ReadMsgBody  {width: 100%; background-color: #ffffff;}
  .ExternalClass  {width: 100%; background-color: #ffffff;}
  body  {width: 100%; background-color: #F7F7F7; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
  html  {width: 100%; }
    @media only screen and (max-width: 640px)   		   {  		body{width:auto!important;}
  		  		  		table[class=scaleForMobile]		{width: 100%!important; padding-left: 30px!important; padding-right: 30px!important; clear: both;}
  		table[class=fullWidth]			{width: 100%!important;}
  		table[class=mobileCenter]		{width: 100%!important; text-align: center!important; border: 0px!important; clear: both;}
  		td[class=mobileCenter]			{width: 440px !important; text-align: center!important; }
  		td[class=eraseForMobile]		{width: 0; display:none !important;}
  		table[class=eraseForMobile]		{width: 0; display:none !important;}
  		td[class=navTd]					{width: 33%!important; text-align: center!important; padding: 0px!important;}
  		img[class=imageScale]			{width: 100%!important;}
  		td[class=pad1]					{width: 100%; height: 1px!important;}
  		td[class=textCenter]			{width: 100%!important; text-align: center!important;}
  		td[class=TopTextHeadline]		{font-size: 28px!important; text-align: center!important}
  		td[class=topTextHeadline2]		{width: 100%!important; font-size: 44px!important; text-align: center!important;}
  		td[class=pad2]					{width: 100%; height: 10px!important;}
  		td[class=pad3]					{width: 100%; height: 40px!important;}
  		td[class=pad4]					{width: 100%; height: 15px!important;}
  		td[class=pad5]					{width: 100%; height: 25px!important;}
  		table[class=bigImageMC]			{width: 100%!important; height: auto!important;}
  		table[class=tableMC]			{width: 100%!important; height: auto!important; clear: both;}
  		.bigImageMC img					{width: 100%!important; height: auto!important;}
  		  		  		}
	  		  		  @media only screen and (max-width: 479px)   		   {  		body{width:auto!important;}
  		  		table[class=scaleForMobile]		{width: 100%!important; padding-left: 30px!important; padding-right: 30px!important; clear: both;}
  		table[class=fullWidth]			{width: 100%!important;}
  		table[class=mobileCenter]		{width: 100%!important; text-align: center!important; border: 0px!important; clear: both;}
  		td[class=mobileCenter]			{width: 280px!important; text-align: center!important; }
  		span[class=eraseForMobile]		{width: 0; display:none !important;}
  		td[class=eraseForMobile]		{width: 0; display:none !important;}
  		table[class=eraseForMobile]		{width: 0; display:none !important;}
  		td[class=navTd]					{width: 33%!important; text-align: center!important; padding: 0px!important;}
  		img[class=imageScale]			{width: 100%!important;}
  		img[class=button]				{margin-bottom: 20px; margin-right: 20px; margin-left: 20px; margin-top: 35px;}
  		img[class=mobile]				{width: 80%!important;}
  		td[class=topTextHeadline]		{width: 100%!important; font-size: 24px!important; text-align: center!important; line-height: 34px!important;}
  		td[class=topTextHeadline2]		{width: 100%!important; font-size: 40px!important; text-align: center!important; line-height: 44px!important;}
  		table[class=bigImageMC]			{width: 100%!important; height: auto!important;}
  		table[class=tableMC]			{width: 100%!important; height: auto!important; clear: both;}
  		.tableMC img					{width: 80%!important; height: auto;}
  		.bigImageMC img					{width: 100%!important; height: auto!important;}
  		  		}
      </style>
<table style="background-color: rgba(0, 0, 0, 0)" width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody>
<tr>
<td style="background-color: rgba(0, 0, 0, 0)" width="100%" valign="top">
</td>
</tr>
<tr>
</tr>
</tbody>
</table>
<table style="background-color: rgba(0, 0, 0, 0)" width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody>
<tr>
<td style="background-color: rgba(0, 0, 0, 0)" width="100%" valign="top">
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" name="1">
  	<tbody>
<tr>
  		<td width="100%" valign="top" bgcolor="#ffffff">
  		  			<!-- Nav Wrapper -->
</tr>
<tr>
</tr>
</tbody>
</table>
<table style="background-color: rgba(0, 0, 0, 0)" width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody>
<tr>
<td style="background-color: rgba(0, 0, 0, 0)" width="100%" valign="top">
<table bgcolor="#f7f7f7" width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter" name="2">
  	<tbody>
<tr>
  		<td height="40">
&nbsp;								  		</td>
  	</tr>
  </tbody>
</table>
</td>
</tr>
<tr>
</tr>
</tbody>
</table>
<table style="background-color: rgba(0, 0, 0, 0)" width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody>
<tr>
<td style="background-color: rgba(0, 0, 0, 0)" width="100%" valign="top">
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" name="6">
  	<tbody>
<tr>
  		<td width="100%" valign="top" bgcolor="#f7f7f7">
  		  			<!-- 2 Columns Text Left, Image Right -->
  			<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
  				<tbody>
<tr>
  					<td width="100%">
  						  						<!-- 2 Columns  -->
  						<table bgcolor="#ffffff" width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-radius: 6px; border-bottom-right-radius: 10px; 0px 1px 2px; box-shadow: 0px 2px 4px rgba(0,0,0,0.2);" class="scaleForMobile">
  							<!-- Headline -->
  							<tbody>
<tr>
  								<td width="600">
  								  									<!-- Column 1 -->
  									<table width="298" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="mobileCenter">
  										<tbody>
<tr>
  											<td width="100%" height="30">
</td>
  										</tr>
  										<tr>
  											<td width="100%">
  												<!-- Headline 1 -->
  												<table width="298" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
  													<tbody>
<tr>
  														<td width="30" class="eraseForMobile">
</td>
  														<td width="238" style="font-size: 50px; color: #090909; text-align: left; font-weight: 100; font-family: Helvetica, Arial, sans-serif; line-height: 52px;" class="topTextHeadline2">
  															<a href="#" style="text-decoration: none; color: #090909;">
  																Hello {{$firstname}}											</a>
									  														</td>
  														<td width="30" class="eraseForMobile">
</td>
  													</tr>
  												</tbody>
</table>
  											</td>
  										</tr>
  										<tr>
  											<td width="100%" height="30">
</td>
  										</tr>
  										<tr>
  											<td width="100%">
  												<!-- Text 1 -->
  												<table width="298" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
  													<tbody>
<tr>
  														<td width="30" class="eraseForMobile">
</td>
  														<td width="238" style="font-size: 14px; color: #515151; text-align: left; font-weight: normal; font-family: Helvetica, Arial, sans-serif; line-height: 22px;" class="textCenter">

                                                        Thank you for signing with Held Up, the little 'life hack' app that’s here to save your day!
                                                        <br />
                                                        Are you always kicking your heels, waiting for your Significant Other to show up?
                                                        <br />
                                                        Set a date with your SO, add a little reward and have some cheeky fun.
                                                        <br />
                                                        Because let's face it, everyone is motivated by a little competition and reward. Get your Game On!

                              </td>
  														<td width="30" class="eraseForMobile">
</td>
  													</tr>
  												</tbody>
</table>
  											</td>
  										</tr>
  										<tr>
  											<td width="100%" height="25">
</td>
  										</tr>
  										<tr>
  											<td width="100%">
  												<!-- Blue Text 2 -->
  												<table width="298" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
  													<tbody>
<tr>
  														<td width="30" class="eraseForMobile">
</td>
  														<td width="238" style="font-size: 14px; color: #dd1e35; text-align: left; font-weight: normal; font-family: Lucida Grande, Helvetica, Arial, sans-serif; line-height: 22px;">
  															  															<!-- Blue 2 -->

							  														</td>
  														<td width="30" class="eraseForMobile">
</td>
  													</tr>
  												</tbody>
</table>
  											</td>
  										</tr>
  										<tr>
  											<td width="100%" height="30" class="eraseForMobile">
</td>
  										</tr>
  									</tbody>
</table>
<!-- Comlumn 1 -->
  									  									<!-- Column 2 Image -->
  									<table width="298" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="tableMC">
  										<tbody>
<tr>
  											<td width="100%" height="30">
</td>
  										</tr>
  										<tr>
  											<td width="100%" valign="bottom" style="line-height: 1px;">
  												<center>

<img class="mobile" src="http://heldup.pluxd.com.au/images/email/img1.jpg" alt="" border="0" style="">

</center>
  											</td>
  										</tr>
  										<tr>
  											<td width="100%" height="0" bgcolor="#e9e9e9" class="pad1">
</td>
  										</tr>
  									</tbody>
</table>
<!-- Comlumn 2 Image -->
  																										  								</td>
  							</tr>
  						</tbody>
</table>
  						  						<!-- Space -->
  						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
  							<tbody>
<tr>
  								<td height="30">
								  								</td>
  							</tr>
  						</tbody>
</table>
  						  					</td>
  				</tr>
  			</tbody>
</table>
<!-- End 2 Columns Text Left, Image Right Wrapper -->
  		  		</td>
  	</tr>
  </tbody>
</table>
</td>
</tr>
<tr>
</tr>
</tbody>
</table>
<table style="background-color: rgba(0, 0, 0, 0)" width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody>
<tr>
<td style="background-color: rgba(0, 0, 0, 0)" width="100%" valign="top">
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" name="9">
  	<tbody>
<tr>
  		<td width="100%" valign="top" bgcolor="#f7f7f7">
  		  			<!-- 2 Columns Text + Small Image -->
  			<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
  				<tbody>
<tr>
  					<td width="100%">
  						  						<!-- 2 Columns  -->
  						<table bgcolor="#ffffff" width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-radius: 6px; border-bottom-right-radius: 10px; 0px 1px 2px; box-shadow: 0px 2px 4px rgba(0,0,0,0.2);" class="scaleForMobile">
  							<!-- 1st Content -->
  							<tbody>
<tr>
  								<td width="600" valign="middle">
  								  									<!-- Column 1 -->
  									<table width="188" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="mobileCenter">
  										<tbody>
<tr>
  											<td width="100%" height="30" class="pad3">
</td>
  										</tr>
  										<tr>
  											<td width="100%">
  												<!-- Small Image 1 -->
  												<table width="188" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
  													<tbody>
<tr>
  														<td width="30" class="eraseForMobile">
</td>
  														<td width="118" class="textCenter">
  															
<img src="http://heldup.pluxd.com.au/images/email/img2.png" alt="" border="0" style="">
									  													
</td>
  														<td width="30" class="eraseForMobile">
</td>
  													</tr>
  												</tbody>
</table>
  											</td>
  										</tr>
  										<tr>
  											<td width="100%" height="30" class="eraseForMobile">
</td>
  										</tr>
  									</tbody>
</table>
<!-- Comlumn 1 -->
  									  									<!-- Column 2 Image -->
  									<table width="408" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="mobileCenter">
  										<tbody>
<tr>
  											<td width="100%" height="35" class="pad5">
</td>
  										</tr>
  										<tr>
  											<td width="100%">
  												<!-- Headline 1 -->
  												<table width="408" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
  													<tbody>
<tr>
  														<td width="10" class="eraseForMobile">
</td>
  														<td width="368" style="font-size: 15px; color: #090909; text-align: left; font-weight: bold; font-family: Lucida Grande, Helvetica, Arial, sans-serif; line-height: 23px;" class="textCenter">
  															<div style="text-decoration: none; color: #090909;">
  																Activate your account 															</div>
                                  <div style="font-size: 14px; color: #515151; text-align: left; font-weight: normal; font-family: Helvetica, Arial, sans-serif; line-height: 22px;" class="textCenter">
                                To activate your account, copy the following activation code, return to Held Up on your mobile and enter your Activation Key.</div>
									  														</td>
  														<td width="30" class="eraseForMobile">
</td>
  													</tr>
  												</tbody>
</table>
  											</td>
  										</tr>
  										<tr>
  											<td width="100%" height="15">
</td>
  										</tr>
  										<tr>
  											<td width="100%">
  												<!-- Text 1 -->
  												<table width="408" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
  													<tbody>
<tr>
  														<td width="10" class="eraseForMobile">
</td>
  														<td width="368" style="font-size: 35px; color: #dd1e35; text-align: left; font-weight: normal; font-family: Helvetica, Arial, sans-serif; line-height: 22px;" class="textCenter">
                                                                                        <div style="text-decoration: none; color: #dd1e35" class="colorResult">
                                            <span style="font-size: 9px; vertical-align: top; color: #dd1e35;" class="colorResult">
</span>
                                              {{$tmpkey}}                                            </div>  															
															<br>
<br>
  															<!-- Blue 4 -->
  															<table width="364" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
  																<tbody>
<tr>
  																	<td valign="top" width="364" class="textCenter">
  																		  																	</td>
  																</tr>
  															</tbody>
</table>
  															  								  														</td>
  														<td width="30" class="eraseForMobile">
</td>
  													</tr>
  												</tbody>
</table>
  											</td>
  									</tbody>
</table>
<!-- End Comlumn 2 Image -->
  																										  								</td>
  							</tr>
  						</tbody>
</table>
  						  						<!-- Space -->
  						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
  							<tbody>
<tr>
  								<td height="30">
								  								</td>
  							</tr>
  						</tbody>
</table>
  						  					</td>
  				</tr>
  			</tbody>
</table>
<!-- End 3 Columns Text + Small Image Wrapper -->
  		  		</td>
  	</tr>
  </tbody>
</table>
</td>
</tr>
<tr>
</tr>
</tbody>
</table>
<table style="background-color: rgba(0, 0, 0, 0)" width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody>
<tr>
<td style="background-color: rgba(0, 0, 0, 0)" width="100%" valign="top">
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" name="4">
  	<tbody>
<tr>
  		<td width="100%" valign="top" bgcolor="#f7f7f7">
  		  			<!-- 3 Columns Wrapper -->
  			<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
  				<tbody>
<tr>
  					<td width="100%">
  						  						<!-- 3 Columns -->
  						<table bgcolor="#ffffff" width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border-radius: 6px; border-bottom-right-radius: 10px; 0px 1px 2px; box-shadow: 0px 2px 4px rgba(0,0,0,0.2);" class="mobileCenter">
  							<!-- Headline -->
  							<tbody>
<tr>
  								<td width="600">
  									  									<!-- Column 3 -->
  									<table width="598" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align: center;" class="mobileCenter">
  										<tbody>
<tr>
  											<td width="100%" height="20">
</td>
  										</tr>
  										<tr>
  											<td width="100%">
  												<center>
<a href="http://www.heldup-app.com">
<img src="http://heldup.pluxd.com.au/images/email/heldup.png" alt="" border="0" style="">
</a>
</center>
  											</td>
  										</tr>
  										<tr>

  										</tr>
  										<tr>
  											<td width="100%">
  											</td>
  										</tr>
  										<tr>
  										</tr>
  										<tr>
  											<td width="100%">
  											</td>
  										</tr>
  										<tr>
  										</tr>
  									</tbody>
</table>
<!-- Comlumn 3 -->
  																	  								</td>
  							</tr>
  						</tbody>
</table>
  						  						<!-- Space -->
  						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
  							<tbody>
<tr>
  								<td height="30">
								  								</td>
  							</tr>
  						</tbody>
</table>
  						  					</td>
  				</tr>
  			</tbody>
</table>
<!-- End 3 Columns Wrapper -->
  		  		</td>
  	</tr>
  </tbody>
</table>
</td>
</tr>
<tr>
</tr>
</tbody>
</table>
<table style="background-color: rgba(0, 0, 0, 0)" width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody>
<tr>
<td style="background-color: rgba(0, 0, 0, 0)" width="100%" valign="top">
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" name="b">
  	<tbody>
<tr>
  		<td width="100%" valign="top" bgcolor="#f7f7f7">
  		  			<!-- Footer Wrapper -->
  			<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
  				<tbody>
<tr>
  					<td width="100%">
  					  						<!-- Space -->
  						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
  							<tbody>
<tr>
  								<td height="20">
								  								</td>
  							</tr>
  						</tbody>
</table>
  						  						<!-- CopyRight -->
  						<table width="298" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
  							<!-- Headline -->
  							<tbody>
<tr>
  								<td width="288" style="font-size: 12px; color: #515151; text-align: left; font-weight: normal; font-family: Helvetica, Arial, sans-serif; line-height: 20px;" class="textCenter">
  								  									Copyright © 2014 Held Up. All Rights Reserved.  																	  								</td>
  							</tr>
  							<tr>
  								<td width="288" class="pad4">
</td>
  							</tr>
  						</tbody>
</table>
  						  						<!-- CopyRight -->
  						<table width="298" border="0" cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
  							<!-- Headline -->
  							<tbody>
<tr>
  								<td width="20">
</td>
  								<td width="278" style="font-size: 12px; color: #515151; text-align: right; font-weight: normal; font-family: Helvetica, Arial, sans-serif; line-height: 20px;" class="textCenter">
  								  									<a href="#" style="text-decoration: none; color: #dd1e35" class="colorResult">
Support</a>
  									&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;  									<a href="#" style="text-decoration: none; color: #dd1e35" class="colorResult">
Privacy</a>
  									&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;  									<a href="mailto:info@heldupapp.com" style="text-decoration: none; color: #dd1e35" class="colorResult">
info@heldupapp.com</a>
  																	  								</td>
  							</tr>
  						</tbody>
</table>
  						  						<!-- Space -->
  						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
  							<tbody>
<tr>
  								<td height="30">
								  								</td>
  							</tr>
  						</tbody>
</table>
  						  					</td>
  				</tr>
  			</tbody>
</table>
<!-- End Footer Wrapper -->
  		  		</td>
  	</tr>
  </tbody>
</table>
</td>
</tr>
<tr>
</tr>
</tbody>
</table>
<table style="background-color: rgba(0, 0, 0, 0)" width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody>
<tr>
<td style="background-color: rgba(0, 0, 0, 0)" width="100%" valign="top">
<table bgcolor="#f7f7f7" width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter" name="c">
  	<tbody>
<tr>
  		<td width="100%" height="40">
									  		</td>
  	</tr>
  	<tr>
  		<td width="100%" height="10">
									  		</td>
  	</tr>
  </tbody>
</table>
</td>
</tr>
<tr>
</tr>
</tbody>
</table>
