<?php

class SetPasswordTest extends TestCase {

    /*======================================
    =            Initialization            =
    ======================================*/
    
    public function setUp()
    {
        parent::setUp();
        $this->prepareForTests();
    }


    private function prepareForTests()
    {
        $this->persona = array(
            "email" => "usertest.punklogic@gmail.com",
            "fullname" => "Jackson Beale"
        );
    }

    private function getPersonaUser()
    {
        Auth::logout();
        User::where('email', $this->persona["email"])
        ->delete();
        $user = new User();
        $user->fullname = $this->persona["fullname"];
        $user->email = $this->persona['email'];
        $user->save();
        return $user;
    }
    public function testRouteSetPassword()
    {
        $crawler = $this->client->request('POST', '/setpwd', 
            array( "password" => "FAIL" ));
        $this->assertResponseStatus(400);

        $user = $this->getPersonaUser();
        $validkey = $user->generateTemporaryKey();


        $response = $this->call('POST', '/activate', array( "key" => $validkey ));
        $this->assertResponseStatus(200);  


        $response = $this->call('POST', '/setpwd', array( "password" => "gotta_work_this_time_around" ) );
        $this->assertResponseStatus(200);  

        $jusr = json_decode($response->getContent());
        $this->assertEquals($user->id, $jusr->id);
        //var_dump($jusr);
    }

}