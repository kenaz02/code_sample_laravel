<?php

class LoginTest extends TestCase {



    /*======================================
    =            Initialization            =
    ======================================*/
    
    public function setUp()
    {
        parent::setUp();
        $this->prepareForTests();
    }


    private function prepareForTests()
    {
        $this->persona = array(
            "email" => "invitee@gmail.com",
            "password" => "test12345678"
        );
    }

    
    /*-----  End of Initialization  ------*/



    public function testLogin()
    {
       $data = array(
            "email" => $this->persona["email"],
            "password" => "fail_password"
        );
        $crawler = $this->client->request('POST', '/login', $data);
        $this->assertResponseStatus(403);     

        $user = User::where('email', $this->persona["email"])->first();
        $data = array(
            "email" => $this->persona["email"],
            "password" => $this->persona["password"]
        );

        $crawler = $this->client->request('POST', '/login', $data);
        $this->assertResponseStatus(200);  
    }

}