<?php

class InvitationControllerTest extends TestCase {



    /*======================================
    =            Initialization            =
    ======================================*/
    
    public function setUp()
    {
        $app = $this->createApplication();
        $app->make('artisan')->call('db:seed');

        parent::setUp();
        $this->prepareForTests();

    }


    private function prepareForTests()
    {
        $this->persona = array(
            "email"    => "invitee@gmail.com",
            "fullname" => "Jackson Beale"
        );

        $user = User::where('email', $this->persona["email"])->first();
        Auth::login($user);
    }

    
    /*-----  End of Initialization  ------*/

    public function testStore(){
        $crawler = $this->client->request('POST', '/invitations',[
            "email" => Auth::user()->email
        ]);
        $this->assertResponseStatus(400); //inviting myself is illegal       

        $crawler = $this->client->request('POST', '/invitations',[
            "email" => "test@gmail.com"
        ]);
        $this->assertResponseStatus(201);

        $this->assertTrue( !! User::where("email", "=", "test@gmail.com")->count() );

        $this->assertTrue( Auth::user()->invitation != 0, "Invitation has not been posted");
    }

    public function testIndex(){
        $response = $this->call('GET', '/invitations');
        $this->assertResponseStatus(200);  

        $jarr = json_decode($response->getContent());
        $this->assertTrue($jarr[0]->inviter->email == "inviter1@gmail.com");
        $this->assertTrue($jarr[1]->inviter->email == "inviter2@gmail.com");
    }

    public function testAccept(){
        $response = $this->call('GET', '/invitations');
        $this->assertResponseStatus(200);  
        $jarr = json_decode($response->getContent());
 
        $this->call( 'GET', '/invitations/123456789/accept' );
        $this->assertResponseStatus(400);  

        $this->call( 'GET', '/invitations/'. $jarr[0]->id . "/accept" );
        $this->assertResponseStatus(200);  

        $this->assertTrue(Auth::user()->partner == $jarr[0]->inviter->id);
    }

    public function testDeny(){
        $response = $this->call('GET', '/invitations');
        $this->assertResponseStatus(200);  
        $jarr = json_decode($response->getContent());
 
        $this->call( 'GET', '/invitations/123456789/deny' );
        $this->assertResponseStatus(400);  

        $this->call( 'GET', '/invitations/'. $jarr[1]->id . "/deny" );
        $this->assertResponseStatus(200);  
        $this->assertFalse( !! Invitation::where("id","=",$jarr[1]->id)->count());
    }

    public function testDestroy(){
        $response = $this->call('POST', '/invitations/'.Auth::user()->invitation, ["_method" => "delete"]);
        $this->assertResponseStatus(200); 
        $this->assertTrue(Auth::user()->invitation == 0);
    }

}