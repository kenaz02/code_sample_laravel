<?php

class UserControllerTest extends TestCase {



    /*======================================
    =            Initialization            =
    ======================================*/
    
    public function setUp()
    {
        parent::setUp();
        $this->prepareForTests();
    }


    private function prepareForTests()
    {
        $this->persona = array(
            "email"    => "invitee@gmail.com",
            "fullname" => "Jackson Beale"
        );

        $user = User::where('email', $this->persona["email"])->first();
        Auth::login($user);
    }

    
    /*-----  End of Initialization  ------*/


    public function testIndex(){
        $response = $this->call('GET', '/users');
        $this->assertResponseStatus(200);  

        $jarr = json_decode($response->getContent());
    }

}