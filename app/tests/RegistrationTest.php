<?php

class RegistrationTest extends TestCase {


    /*======================================
    =            Initialization            =
    ======================================*/
    
    public function setUp()
    {
        parent::setUp();
        $this->prepareForTests();
    }


    private function prepareForTests()
    {
        $this->persona = array(
            "email" => "usertest.punklogic@gmail.com",
            "fullname" => "Jackson Beale"
        );
    }

    
    /*-----  End of Initialization  ------*/
    
    
    /*===============================
    =            Helpers            =
    ===============================*/

    protected function insertPersona()
    {
        
    }

    protected function deletePersona()
    {

    }


    
    /*-----  End of Helpers  ------*/
    
    


    /**
    
        TODO:
         -Regex validates email address is correct format
         -Name field is mandatory
         -Email Address field is mandatory
    
    **/
    
	public function testStoreValidationEmail()
    {
        $data = array(
            array(
                $this->persona["fullname"],
                'email' => 'gfetcogmail.com'
            ), 
            array(
                $this->persona["fullname"],
                'email' => ''
            ), 
            array(
                $this->persona["fullname"],
                'email' => 'gfetco@gmail.com'
            ) 
        );

        foreach($data as $d)
        {
            $crawler = $this->client->request('POST', '/users', $d);
            $this->assertResponseStatus(400);
        }
    }

	public function testStoreValidationFullname()
    {
        $data = array(
            'fullname' => '',
            $this->persona["email"]
        );

        $crawler = $this->client->request('POST', '/users', $data);
        $this->assertResponseStatus(400);
    }


    /**
    
        TODO:
        -The user can enter first name or full name
        -Form validation messages display under the fields
    
    **/
    public function testTmpKey()
    {
        $user = new User();
        $key = $user->generateTemporaryKey();
        $this->assertTrue($key == $user->tmpkey);
    }

    public function testStore()
    {
        User::where('email', $this->persona["email"])
        ->delete();

        $crawler = $this->client->request('POST', '/users', $this->persona);

        $this->assertResponseStatus(201);       

        $user =  User::where('email', $this->persona["email"])->first();

        $this->assertFalse( !!$user->activated, "activated Boolean is not FALSE.");
        $user->delete();
    }


  
}
