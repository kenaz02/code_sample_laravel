<?php

class ActivateTest extends TestCase {

    /*======================================
    =            Initialization            =
    ======================================*/
    
    public function setUp()
    {
        parent::setUp();
        $this->prepareForTests();
    }


    private function prepareForTests()
    {
        $this->persona = array(
            "email" => "usertest.punklogic@gmail.com",
            "fullname" => "Jackson Beale"
        );
    }

    private function getPersonaUser()
    {
        Auth::logout();
        User::where('email', $this->persona["email"])
        ->delete();
        $user = new User();
        $user->fullname = $this->persona["fullname"];
        $user->email = $this->persona['email'];
        $user->save();
        return $user;
    }

    public function testUnitActivate()
    {
        $user = new User();
        $key = $user->generateTemporaryKey();
        $this->assertTrue($user->activate($key));
    }

    public function testFindByTemporaryKey()
    {
        $user = new User();
        $key = $user->generateTemporaryKey();
        

        $this->assertTrue($user->id == User::findByTemporaryKey($key)->id);
    }

    public function testRouteActivate(){

        $crawler = $this->client->request('POST', '/activate', 
            array( "key" => "FAIL" ));
        $this->assertResponseStatus(400);

        $user = $this->getPersonaUser();
        $validkey = $user->generateTemporaryKey();


        $response = $this->call('POST', '/activate', array( "key" => $validkey ));
        $this->assertResponseStatus(200);  

        $jusr = json_decode($response->getContent());
        $this->assertEquals($user->id, $jusr->id);
        //var_dump($jusr);
    }
}