
======
##
##Introduction
...
##How to use
1. Make sure that you've installed PHP >= 55 && Apache (>= 2 recommended)
2. Edit /etc/hosts and add whatever hostname you want to use, preferably `127.0.0.1 heldup`
   For windows user: C:\Windows\System32\Drivers\etc\hosts
3. Apply the Public Directory to your VHOST files, usually located at /etc/apache2/(find appropiate vhost file)
   OR if you use XAMPP or anything similar find configuration directory then it should almost always be located at extra/vhosts,
   AND that it points to public directory which is within the projects root directory
4. Make sure laravel has access(permissions) to the projects root directory
5. Add the corresponding files or directories to
	1. app/storage/views (dir)
	2. app/storage/logs
	3. app/storage/views
	4. add your sqlite3 database files, through `sqlite3`: test.sqlite, production.sqlite
	5. migrate the databases with `php55 artisan migrate`; NOTE: Databases should be empty while doing this
	6. Optional: seed the databases: `php55 artisan db:seed`
	7. Finally make sure you have a configuration file setup at public/js/config.js with the following:
```
define(["jquery", "jqueryvalidate"], function( $ ) {

    var Config = {
    	mode: "testing", /*`testing` or `production`*/
        network : {
        	testing: {
	          	"root" : "http://127.0.0.1"
        	},
        	production: {
	          	"root" : "http://example.com"
        	},
        	url : function(url){
				return this[Config.mode].root + url;
			},
        }
    };
   	return Config;
});

```
Note: Adjust the configuration according to your preference