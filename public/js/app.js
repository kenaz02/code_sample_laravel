require.config( {

	paths: {
		"text": "libs/text",
		"async": "libs/async",
		'goog': 'libs/goog',
		"jquery": "libs/jquery",
		"jqueryvalidate": "libs/jqueryvalidate",
		"underscore": "libs/lodash",
		"backbone": "libs/backbone",
		"gmaps":   "libs/gmaps",
		"scrollfix": "libs/scrollfix"
	},
	waitSeconds: 60 * 60,
	shim: {
		"jqueryvalidate": ["jquery"],
		"backbone": {
			"deps": [ "underscore", "jquery" ],
			"exports": "Backbone"  //attaches "Backbone" to the window object
		},
		"googleapi": {
			"exports": "googleapi"
		},
		"gmaps": {
			"deps": [ "google" ],
			"exports": "GMaps"
		}
	} // end Shim Configuration

} );
// Includes File Dependencies
require([ "jquery", "backbone", "router", "views/dialogs/AlertDialogView", "language"], function( $, Backbone, Router, AlertDialogView, Language ) {
	
    _.templateSettings = {
      interpolate: /\{\{(.+?)\}\}/g,
	  evaluate: /\<\%(.+?)\%\>/g
    };
	//document.addEventListener('touchmove', function(event) {
	//     event.preventDefault();
	//}, false);
	$(function(){
		$.ajaxSetup({
	        error: function(jqXHR, exception) 
	        {
	            if (jqXHR.status === 0) {
	                //alert('Not connect.\n Verify Network.');
	                var adv = new AlertDialogView(Language.view["default"].dialogs["connectionlost"]);
	                adv.popup( "open" );

	            } else if (jqXHR.status == 404) {
	                //alert('Requested page not found. [404]');
	                var adv = new AlertDialogView(Language.view["default"].dialogs["notfound"]);
	                adv.popup( "open" );

	            } else if (jqXHR.status == 500) {
	                //alert('Internal Server Error [500].');
	                var adv = new AlertDialogView(Language.view["default"].dialogs["systemerror"]);
	                adv.popup( "open" );

	            } else if (exception === 'parsererror') {
	                //alert('Requested JSON parse failed.');
	            } else if (exception === 'timeout') {
	                //alert('Time out error.');
	                var adv = new AlertDialogView(Language.view["default"].dialogs["timeout"]);
	                adv.popup( "open" );

	            } else if (exception === 'abort') {
	                //alert('Ajax request aborted.');
	                var adv = new AlertDialogView(Language.view["default"].dialogs["aborted"]);
	                adv.popup( "open" );

	            } else {
	                //alert('Uncaught Error.\n' + jqXHR.responseText);
	            }
	        }
    	});
		var proxiedSync = Backbone.sync;
		Backbone.sync = function(method, model, options) {
			options || (options = {});
			if( ! options.crossDomain ) {
				options.crossDomain = true;
			}
			if( ! options.xhrFields ) {
				options.xhrFields = {withCredentials: false};
			}
			return proxiedSync(method, model, options);
		};
		require( [], function() {
			// Instantiates a new Backbone.js Mobile Router
			this.router = new Router();
			var _this = this;
			window.changePage = function(href, direction,callback){
				_this.router.gotoURL(href, direction, callback);
			};
			window.router = this.router;
		});

	});
});

define('google', ['async!https://maps.googleapis.com/maps/api/js?sensor=true&libraries=places&v=3.exp'],
function(){
    // return the gmaps namespace for brevity
    return window.google;
});
