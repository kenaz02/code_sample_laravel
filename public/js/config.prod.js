define(["jquery", "jqueryvalidate"], function( $ ) {

    var Config = {
    	mode: "production", /*`testing` or `production`*/
        network : {
        	testing: {
	          	"root" : "http://heldup"
        	},
            staging: {
                "root" : "http://192.168.1.108"
            },
        	production: {
	          	"root" : "http://heldup.pluxd.com.au"
        	},
        	url : function(url){
				return this[Config.mode].root + url;
			},
        }
    };
   	return Config;
});
