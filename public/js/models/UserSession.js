// Category Model
// ==============

// Includes file dependencies
define([ "jquery", "backbone", "models/UserModel" ], function( $, Backbone, UserModel ) { 
	return new UserModel();
} );