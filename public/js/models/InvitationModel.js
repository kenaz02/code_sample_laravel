// Category Model
// ==============

// Includes file dependencies
define([ "jquery", "backbone", "models/BaseModel", "config" ], function( $, Backbone, BaseModel, Config ) {


    // The Model constructor
    var Model = BaseModel.extend( {
    	url: function(){
        	return Config.network.url('/invitations/') + this.id;
    	},
    	acceptpartner: function(callbacks){
    		this.getJSON(callbacks, this.url() + "/accept" );
    	},    		
    	rejectpartner: function(callbacks){
    		this.getJSON(callbacks, this.url() + "/deny" );
    	},


    } );

    // Returns the Model class
    return Model;

} );