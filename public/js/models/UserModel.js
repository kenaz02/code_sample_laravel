// Category Model
// ==============

// Includes file dependencies
define([ "jquery", "backbone", "models/BaseModel", "config" ], function( $, Backbone, BaseModel, Config ) {


    // The Model constructor
    var Model = BaseModel.extend( {

    	url: function(){
        	return Config.network.url('/users');
    	},
    	login: function(jsondata, callbacks){
            var __this = this;
            var success = callbacks["200"];

            callbacks["200"] = function(jsondata, _this){
                __this.set(jsondata);
                __this.interval();
                __this.set("loggedin", true);
                

                //Initialize Parse noitifcation plugin
                parsePlugin.initialize('PwvUADSoFQmJ9JhxV2Oo8MDqQlsYjpsafb1GYBHd', 'G0QGxOCkJpbO3jD9yuKT1L1QeYKS2uG5MmpZzqcN', function() {
                    //Get installationID from Parse
                    parsePlugin.getInstallationId(function(id) {
                        //Save installatioId in device_token field
                        __this.postJSON(
                            {
                                "token": id
                            },
                            {
                                200: function(){}
                            },
                            Config.network.url('/settoken')
                        );
                    }, function(e) {
                        alert('error get installationId');
                    });
                }, function(e) {
                    alert('error initialize plugin');
                });
                //End


                success(jsondata, _this);
            };

    		this.postJSON(jsondata, callbacks, Config.network.url('/login') );
    	},
    	signup: function(jsondata, callbacks){
    		this.postJSON(jsondata, callbacks, Config.network.url('/users') );
    	},
        lostpwd: function(jsondata, callbacks){
            this.postJSON(jsondata, callbacks, Config.network.url('/lostpwd') );
        },
        activate: function(jsondata, callbacks){
            var __this = this;
            var success = callbacks["200"];

            callbacks["200"] = function(jsondata, _this){
                __this.set(jsondata);
                __this.interval();
                __this.set("loggedin", true);


                //Initialize Parse noitifcation plugin
                parsePlugin.initialize('PwvUADSoFQmJ9JhxV2Oo8MDqQlsYjpsafb1GYBHd', 'G0QGxOCkJpbO3jD9yuKT1L1QeYKS2uG5MmpZzqcN', function() {
                    //Get installationID from Parse
                    parsePlugin.getInstallationId(function(id) {
                        //Save installatioId in device_token field
                        __this.postJSON(
                            {
                                "token": id
                            },
                            {
                                200: function(){}
                            },
                            Config.network.url('/settoken')
                        );
                    }, function(e) {
                        alert('error get installationId');
                    });
                }, function(e) {
                    alert('error initialize plugin');
                });
                //End



                success(jsondata, _this);
            };

            this.postJSON(jsondata, callbacks, Config.network.url('/activate') );
        },
    	setpwd: function(jsondata, callbacks){
    		this.postJSON(jsondata, callbacks, Config.network.url('/setpwd') );
    	},
        profile: function(jsondata, callbacks){
            this.postJSON(jsondata, callbacks, Config.network.url('/profile') );
        },
        reminders: function(jsondata, callbacks){
            this.postJSON(jsondata, callbacks, Config.network.url('/reminders') );
        },
        invite: function(jsondata, callbacks){
            this.postJSON(jsondata, callbacks, Config.network.url('/invitations') );
        },
        disconnect: function(callbacks){
            this.getJSON(callbacks, Config.network.url('/disconnect') );
        },
        logout: function(callbacks){
            this.getJSON(callbacks, Config.network.url('/logout') );
            this.clearInterval();
            this.clear();
            this.set("loggedin", false);
        },
        deleteInvitation: function(callbacks){
            var uri = '/invitations/' + this.get("invitation").id;
            this.deleteJSON(callbacks, Config.network.url(uri));
        },
        initialize: function() {
            this.clear();
            this.set("loggedin", false);
        },

        syncInstance: 0,
        clearInterval: function(){
            clearInterval(this.syncInstance);
        },
        forceUpdate: function(okRun){
            var _this = this;
            _this.fetch({
            	success: function(){
            		okRun();
                }
            });
        },
        interval: function(){
            var _this = this;
            this.syncInstance = setInterval(function(){
                _this.fetch({
                    success: function(){
                         $("body").trigger("updateInterval");
                    }
                });
            }, 10000);
        },

    } );

    // Returns the Model class
    return Model;

} );