// Category Model
// ==============

// Includes file dependencies
define([ "jquery", "backbone", "models/BaseModel", "config" ], function( $, Backbone, BaseModel, Config ) {

    // The Model constructor
    var Model = BaseModel.extend( {
    	url: function(){
        	return Config.network.url('/rewards/') + this.id;
    	},
    	not_received: function(jsondata, callbacks){
        	this.postJSON( jsondata, callbacks, this.url() + '/not_received' );
    	},
    	received: function(jsondata, callbacks){
        	this.postJSON(jsondata, callbacks, this.url() + '/received' );
    	}
    });

    return Model;
});