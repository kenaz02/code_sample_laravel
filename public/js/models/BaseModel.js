// Category Model
// ==============

// Includes file dependencies
define([ "jquery", "backbone", "views/dialogs/AlertDialogView", "language" ], function( $, Backbone, AlertDialogView, Language ) {


    // The Model constructor
    var Model = Backbone.Model.extend( {
        spinner : function(state){
            if( state == "show")
            {
                $(".pt-page-current").prepend("<div class='ui-loader'><i class='ss-settings'></i></div>");
                $(".ui-loader").show();
            }
            else
            {
                $(".ui-loader").remove();
            }
        },
    	postJSON: function(jsondata, statusCodeCallbacks, url){
            var _this = this;
            this.spinner("show");

			$.ajax({
                type: "POST",
                url: url || this.url(),
                data: jsondata,
                statusCode: statusCodeCallbacks,
            }).done(function(data){
                _this.spinner("hide");
            }).fail(function(data)
            {
                _this.spinner("hide");
            });
    	},
        getJSON: function(statusCodeCallbacks, url){
            var _this = this;
            this.spinner("show");
            $.ajax({
                type: "GET",
                url: url || this.url(),
                statusCode: statusCodeCallbacks,
            }).done(function(data){
                _this.spinner("hide");
            }).fail(function(data)
            {
                _this.spinner("hide");
            });
        },
        deleteJSON: function(statusCodeCallbacks, url){
            var _this = this;
            this.spinner("show");
            $.ajax({
                type: "DELETE",
                url: url || this.url(),
                statusCode: statusCodeCallbacks,
            }).done(function(data){
                _this.spinner("hide");
            }).fail(function(data)
            {
                _this.spinner("hide");
            });
        }            
    } );

    // Returns the Model class
    return Model;

} );