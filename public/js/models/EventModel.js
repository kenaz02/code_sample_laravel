// Category Model
// ==============

// Includes file dependencies
define([ "jquery", "backbone", "models/BaseModel", "config" ], function( $, Backbone, BaseModel, Config ) {


    // The Model constructor
    var Model = BaseModel.extend( {
    	url: function(){
        	return Config.network.url('/events/') + this.id;
    	},
        createevent: function(jsondata, callbacks){
            this.postJSON(jsondata, callbacks, Config.network.url('/events') );
        },
    	accept: function(jsondata, callbacks){
    		this.postJSON(jsondata, callbacks, this.url() + '/accept' );
    	},
        delete: function(callbacks){
            this.getJSON(callbacks, this.url() + '/delete' );
        },
        cancel: function(callbacks){
            this.getJSON(callbacks, this.url() + '/cancel' );
        },
        onTime: function(callbacks){
            this.getJSON(callbacks, this.url() + '/ontime' );
        },

        late: function(callbacks){
            this.getJSON(callbacks, this.url() + '/late' );
        },

        reject: function(jsondata, callbacks){
            this.postJSON( jsondata, callbacks, this.url() + '/reject' );
        },
        heldup: function(jsondata, callbacks){
            this.postJSON(jsondata, callbacks, this.url() + '/heldup' );
        },

        
        suggest: function(jsondata, callbacks){
            this.postJSON(jsondata, callbacks, this.url() + '/suggest' );
        },

        update: function(jsondata, callbacks){
            this.postJSON(jsondata, callbacks, this.url() + '/update' );
        }

    });
    // Returns the Model class
    return Model;

} );