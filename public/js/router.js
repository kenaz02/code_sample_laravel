// Mobile Router
// =============

// Includes file dependencies
define(["jquery","backbone", 

        "models/UserSession",
        "models/InvitationModel",
        "collections/InvitationCollection",

        "language", 
        "views/dialogs/AlertDialogView", 
        "views/dialogs/PromptDialogView",

        "views/ActivateView",
        "views/ConnectedView",
        "views/DummyView",
        "views/InvitationsView",
        "views/InviteView",
        "views/PendingView",
        "views/SetPasswordView",
        "views/SignupView",
        "views/LoginView",
        "views/DashboardView",
        "views/LostPasswordView",
        "views/CreateEventView",
        "views/EditEventView",
        "views/MapView",
        "views/NavigateView",
        "views/TOSView",
        "views/SettingsView",
        "views/SplashView",
        "views/DashboardPreviewView",
        "views/RewardsView",
        "views/StatisticsView",
        "views/ProfileView",
        "views/RemindersView"

        ],  function( $, Backbone, 

            UserSession,
            InvitationModel,
            InvitationCollection,

            Language,
            AlertDialogView,
            PromptDialogView,

            ActivateView,
            ConnectedView,
            DummyView,
            InvitationsView,
            InviteView,
            PendingView,
            SetPasswordView,
            SignupView,
            LoginView,
            DashboardView,
            LostPasswordView,
            CreateEventView,
            EditEventView,
            MapView,
            NavigateView,
            TOSView,
            SettingsView,
            SplashView,
            DashboardPreviewView,

            RewardsView,
            StatisticsView,
            ProfileView,
            RemindersView
         ) {

    // Extends Backbone.Router
    var Router = Backbone.Router.extend( {

        // The Router constructor
        initialize: function() {

            // Tells Backbone to start watching for hashchange events
            Backbone.history.start();
            $(".footer a").click(function(){
                $(this).parent().find("a").removeClass("active");
                $(this).addClass("active");
            });
        },
        sync: function(_this){
            if(UserSession.get("loggedin"))
            {
                if( UserSession.changed.partner == "0"  ){
                    var adv = new AlertDialogView(Language.router.partnerDisconnection);
                    adv.popup( "open" );
                    adv.$primary_btn.on("click", function(){
                        _this.connectedFilter();
                    });
                    
                }
            }
        },
        transition:{
            busy: false,
            queue : [],
            endNextPage : false,
            endCurrPage : false,
            animEndEvents : "webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend",
            call: function(options){
                var _this = this;

                $('body').prepend(options.$el);

                var outAnimClass = options.dir ? "pt-page-moveToLeft" :  "pt-page-moveToRight";

                _this.endNextPage = false;
                _this.endCurrPage = false;
                $(".pt-page-current")
                .addClass(outAnimClass)
                .on( _this.animEndEvents, function() {
                    $(this).remove();
                    $(this).off( _this.animEndEvents );
                    //_this.endNextPage = true;
                    //if( _this.endCurrPage )
                        _this.done(options.$el);
                });

                /*$nextPage
                .addClass("pt-page-moveFromRight")
                .on( _this.animEndEvents, function(){
                    $(this).removeClass("pt-page-moveFromRight");
                    $(this).off( _this.animEndEvents );
                    _this.endCurrPage = true;
                    if( _this.endNextPage )
                        _this.done($nextPage);
                });*/
                


            },
            goto: function($nextPage, direction){
                direction = direction? true : false;
                if(this.busy || this.queue.length ){
                    this.queue.push({$el: $nextPage, dir: direction});
                    return false;
                }
                this.busy = true;
                this.call({$el: $nextPage, dir: direction});
                return true;
            },

            done: function(page){
                page.addClass("pt-page-current");
                
                if( this.queue.length )
                    this.call(this.queue.shift()); //call next in queue
                else this.busy = false; // all done in queue
            }
        },
        direction: false,
        // Backbone.js Routes
        routes: {
            // When there is no hash bang on the url, the home method is called
            "" :            "index",
            "signup":       "signup",
            "setpwd" :      "setpwd",
            "changepwd" :   "changepwd",
            "activate" :    "activate",
            "invite" :      "invite",
            "connected" :   "connected",
            "pending":      "pending",
            "invitations" : "invitations",
            "login" :       "login",
            "dashboard" :   "dashboard",
            "dashboard/:id" :   "dashboard_goto",
            "createevent" : "createevent",
            "event/:id" : "event",
            "map/:id" : "map",
            "navigate/:id" : "navigate",
            "tos" :         "tos",
            "lostpwd" :     "lostpwd",
            "settings" :     "settings",
            "profile":       "profile",
            "rewards":       "rewards",
            "statistics": "statistics",
            "reminders": "reminders",
            "dummy/:file" : "dummy",
            "test/:file" :  "test",

            "preview/dashboard" :   "dashboardpreview"

        },
        dummy: function(file){
            this.changePage(new DummyView({ 
                name : file,
                filters : []
            }), this.direction);
        },
        test:  function(file){
            this.changePage(new DummyView({ 
                name : "tests/" + file,
                filters : []
            }), this.direction);
        },
        dashboardpreview:function () {
            this.changePage(new DashboardPreviewView({ 
                name  : "dashboard",
                filters : [ ]
            }), this.direction);
        },    


        index: function(){
            this.changePage(new SplashView({ 
                name  : "splash",
                filters : []
            }), this.direction);            
        },
        signup:function () {
            this.changePage(new SignupView({ 
                name  : "signup",
                filters : []
            }), this.direction);
        },   
        lostpwd:function () {
            this.changePage(new LostPasswordView({ 
                name  : "lostpwd",
                filters : []
            }), this.direction);
        },   
        login:function () {
            this.changePage(new LoginView({ 
                name  : "login",
                filters : []
            }), this.direction);
        },   
        dashboard:function () {
            this.changePage(new DashboardView({ 
                name  : "dashboard",
                filters : [ "activated", "connected" ],
            }), this.direction);
        },
        dashboard_goto:function (gid) {
            this.changePage(new DashboardView({ 
                name  : "dashboard",
                filters : [ "activated", "connected" ],
                id: gid
            }), this.direction);
        }, 
        setpwd:function () {
            this.changePage(new SetPasswordView({ 
                name : "setpwd",
                filters : ["activated"]
            }), this.direction);
        },   
        changepwd:function () {
            this.changePage(new SetPasswordView({ 
                name : "changepwd",
                filters : ["activated"]
            }), this.direction);
        },   
        activate:function () {
            this.changePage(new ActivateView({ 
                name : "activate",
                filters : []
            }), this.direction);
        },       
        invite:function () {
            this.changePage(new InviteView({ 
                name : "invite",
                filters : ["activated"]
            }), this.direction);
        },
        connected:function () {
            this.changePage(new ConnectedView({ 
                name : "connected",
                filters : ["activated", "connected"]
            }), this.direction);
        },
        createevent:function () {
            this.changePage(new CreateEventView({ 
                name : "createevent",
                filters : ["activated", "connected"]
            }), this.direction);
        },
        event:function (id) {
            this.changePage(new EditEventView({
                id: id,
                name : "editevent",
                filters : [/*"activated", "connected"*/]
            }), this.direction);
        },
        map:function (id) {
            this.changePage(new MapView({ 
                name : "map",
                id: id,
                filters : [/*"activated", "connected"*/]
            }), this.direction);
        },
        navigate:function (id) {
            this.changePage(new NavigateView({ 
                name : "map",
                id: id,
                filters : [/*"activated", "connected"*/]
            }), this.direction);
        },
        settings:function () {
            this.changePage(new SettingsView({ 
                name : "settings",
                filters : ["activated", "connected"]
            }), this.direction);
        },
        tos:function () {
            this.changePage(new TOSView({ 
                name : "tos",
                filters : []
            }), this.direction);
        },
        pending:function () {
            this.changePage(new PendingView({ 
                name : "pending",
                filters : ["activated"]
            }), this.direction);
        },
        invitations:function () {
            this.changePage(new InvitationsView({ 
                name : "invitations",
                filters : ["activated"]
            }), this.direction);
        },
        reminders:function () {
            this.changePage(new RemindersView({ 
                name : "reminders",
                filters : []
            }), this.direction);
        },
        profile:function () {
            this.changePage(new ProfileView({ 
                name : "profile",
                filters : ["activated", "connected"]
            }), this.direction);
        },
        rewards:function () {
            this.changePage(new RewardsView({ 
                name : "rewards",
                filters : [/*"activated", "connected"*/]
            }), this.direction);
        },
        statistics:function () {
            this.changePage(new StatisticsView({ 
                name : "statistics",
                filters : [/*"activated", "connected"*/]
            }), this.direction);
        },
        //filters: {
            "activatedFilter" : function(){
                if( ! UserSession.has("activated") || UserSession.get("activated") == "0" ){ 
                    this.gotoURL("#", "left");
                    return false;
                }
                return true;
            },
            "connectedFilter" : function(){
                if( UserSession.get("partner") == "0"  )
                {
                    if( UserSession.get('invitation') != "0" )
                        window.changePage("#pending", "left");
                    else if( UserSession.get('invitations') != "0" )
                        window.changePage("#invitations", "left");
                    else
                        window.changePage("#invite", "left");

                    return false;
                }           
                return true;
            },
        //},

        changePage: function (page, direction) {
            for( var i = 0; i < page.config.filters.length; i++){
                if( ! this[page.config.filters[i] + "Filter"]() ) return false;
            }


            $(page.el).attr('data-role', 'page');
            $(page.el).attr('id', page.config.name);
            if( ! $("[data-role=page]").length ){

                $('body').prepend(page.render().$el.addClass("pt-page-current"));
                return false;
            }
            var $el = page.render().$el;
            this.transition.goto( $el, direction);
            var _this= this;
            $el.on("afterRender", function(){
                $("body").on("updateInterval", function(){
                    _this.sync(_this);
                });
            });

            this.currentPageView = page;
        },

        gotoURL : function(url, direction){
            direction = direction == "left" ? true:false;
            this.direction = direction;
            window.location = url;
        }

    } );

    // Returns the Router class
    return Router;

} );