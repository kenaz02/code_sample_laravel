define(["jquery", "jqueryvalidate"], function($) {

  var Language = {
    router: {
      partnerDisconnection: {
        "title": "Sorry, your Significant Other...",
        "content": "has disconnected!",
        "primary_btn": "I’m fine with that.",
        "close": false,
        //close Button is `true` by default
        "icon": "ss-notifications" //`ss-notifcations` is set by default
      }
    },
    view: 
    { 
     activate: {
        validation: {
          rules: {},
          messages: {
            key: {
              required: "Please insert your activation key."
            }
          }
        },
        dialogs: {
          invalidKey: {
            title: "Invalid Key",
            content: "So sorry, but it would appear that this isn’t the correct activation key.<br>Please try again.",
            primary_btn: "Try Again",
            close: true,
            icon: "ss-notifications"
          }
        },
        strings: {
          titlebar: {
            title: "Activate",
            buttons: {
              left: "Back",
              right: "Done"
            }
          },
          form: {
            header: {
              title: "Please check your mailbox.",
              subtitle: "You will need to insert the activation key you received in your email."
            },
            placeholders: {
              key: "Insert your activation key!"
            }
          }
        }
      },


      profile: {
        validation: {
          rules: {},
          messages: {}
        },
        dialogs: {
          profileUpdated: {
            title: "Super job!",
            content: "Your profile has now been updated.",
            primary_btn: "OK",
            close: true,
            icon: "ss-notifications"
          }
        },
        strings: {
          titlebar: {
            title: "Profile",
            buttons: {
              left: "Done",
              right: "Update"
            }
          }
        }
      },

      tos: {
        strings: {
          titlebar: {
            title: "Terms of Service",
            buttons: {
              left: "Done"
            }
          },
          content: "<h1>Terms of Service</h1><p>Our terms for using the Held Up service are short and sweet:</p><p>Play nicely. Everyone is responsible for civilised behaviour and good manners.</p>"
        }
      },

      statistics: {
        strings: {
          titlebar: {
            title: "Held Up Statistics"
          },
          headerfooter: {
            users: "How am I doing?",
            partners: "Stats for"
          }
        }
      },

      splash: {
        strings: {
          buttons: {
            login: "Sign In",
            signup: "Create Account"
          }
        }
      },

      reward: {
          strings: {
              buttons: {
            	  received: "Received",
                  not_received: "Not received"
             },
          }
        },
            
      rewards: {
        validation: {
          rules: {},
          messages: {}
        },
        strings: {
          titlebar: {
            title: "Held Up Rewards",
            buttons: {
              right: "Create"
            }
          },
          headerfooter: {
            earnedrewards: "My Rewards",
            givenrewards: "Rewards for"
          }
        }
      },

      reminders: {
        validation: {
          rules: {},
          messages: {}
        },
        dialogs: {
          remindersUpdated: {
            title: "Your alerts have been updated.",
            content: "Now, go have some cheeky fun!",
            primary_btn: "OK",
            close: true,
            icon: "ss-notifications"
          }
        },
        notifications: {
          time_event: "TIME - hope you are just around the corner!",
          mins5: "5 minutes to go - better MOVE it sweet cheeks!",
          mins15: "15 minutes and counting. You can Do It!",
          mins30: "30 minutes to go - have you planned your escape route yet sweet cheeks?",
          hours1: "This is your 1 hour warning bell. WARNING. BELL.",
          hours2: "2 hours until your little rendezvous darling.",
          days1: "Don't forget your date with your S.O. tomorrow!",
          days2: "You have 48 hours until your next little rendezvous. Nice work!",
          days7: "My, aren't we organized! You have one week until your next event."
        },
        strings: {
          titlebar: {
            title: "Alerts",
            buttons: {
              left: "Done",
              right: "Update"
            }
          },
          form: {
            header: {
              title: "Event Alerts",
              subtitle: "<p>Set alert notifications for upcoming events to help you win your rewards!</p><p>You can change the alerts right here, at any time.</p>"
            },
            placeholders: {
              reminder: {
                title: "First Alert",
                list: ["None", "At time of event", "5 minutes before", "15 minutes before", "30 minutes before", "One hour before", "Two hours before", "One day before", "Two days before", "One week before"]
              },
              secondaryreminder: {
                title: "Final Alert!",
                list: ["None", "At time of event", "5 minutes before", "15 minutes before", "30 minutes before", "One hour before", "Two hours before", "One day before", "Two days before", "One week before"]
              }
            }
          }
        }
      },

      signup: {
        validation: {
          rules: {},
          messages: {
            fullname: {
              required: "Please enter your full name.",
              minlength: "At least {0} characters are required."
            },
            email: "Please enter a valid email address.",
            required: "We cannot play with an empty field."
          }
        },

        dialogs: {
          alreadyRegistered: {
            title: "Email in use.",
            content: "Sorry, it would appear that this email address is already in use.",
            primary_btn: "Try Again",
            icon: "ss-alert"
          }
        },

        strings: {
          titlebar: {
            title: "Sign up",
            buttons: {
              left: "Back",
              right: "Done"
            }
          },
          form: {
            header: {
              title: "A few little terms so you can get the most out of your Held Up experience.",
              subtitle: "By signing up you agree to our <br><a href='#tos'><u>Terms of Service.</u></a>"
            },
            placeholders: {
              fullname: "Your Name",
              email: "Your Email"
            },
            footer: {
              paragraph: "Already have an account? <a href='#login'>Please sign in.</a>"
            }
          }
        }
      },

      lostpwd: {
        validation: {
          rules: {},
          messages: {}
        },
        dialogs: {
          alreadyRegistered: {
            title: "This email address doesn't exist",
            content: "So sorry, but we simply cannot find that email anywhere in the system.",
            primary_btn: "Try Again",
            icon: "ss-alert"
          }
        },
        strings: {
          titlebar: {
            title: "Reset Password",
            buttons: {
              left: "Back",
              right: "Next"
            }
          },
          form: {
            header: {
              title: "Password Reset",
              subtitle: "Now don’t you panic, darling.<br>Take some deep breaths.<br>We will send you instructions on how to reset your password by email very shortly."
            },
            placeholders: {
              email: "Your Email"
            },
            footer: {
              paragraph: "Don't have an <a href='#signup'>Account?</a>"
            }
          }
        }
      },

      settings: {
        strings: {
          titlebar: {
            title: "Settings",
            buttons: {
              left: "Logout"
            }
          },
          buttons: {
            profile: "Your Profile",
            partnermgr: "Significant Other (S.O.) Connection",
            changepwd: "Change Password",
            reminders: "Event Alerts",
            tos: "Terms of Service"
          }
        }
      },

      dashboard: {
        validation: {
          rules: {},
          messages: {}
        },
        dialogs: {
          failsafeAccept: {
            title: "Are you very sure?",
            content: "Leave a little note for your S.O. below...",
            primary_btn: "Yes",
            icon: "ss-alert",
            close: "true"
          },
          failsafeDelete: {
            title: "Are you completely sure?",
            content: "Let your S.O. know why below...",
            primary_btn: "Yes",
            icon: "ss-alert",
            close: "true"
          },
          failsafeCancel: {
            title: "Are you absolutely sure?",
            content: "Let your S.O. know why below...",
            primary_btn: "Yes",
            icon: "ss-alert",
            close: "true"
          },
          failsafeOnTime: {
            title: "That’s fabulous!",
            content: "They deserve a reward. Tap ‘Yes’ to confirm.",
            primary_btn: "Yes",
            icon: "ss-alert",
            close: "true"
          },
          failsafeLate: {
            title: "Oh no!",
            content: "That’s too bad. No reward this time. Tap ‘No’ to confirm.",
            primary_btn: "No",
            icon: "ss-alert",
            close: "true"
          },
          modalSuggest: {
            title: "Are you totally sure?",
            content: "What does your little heart truly desire? Let them know below...",
            primary_btn: "Renegotiate!",
            placeholder: "Your reward suggestion.",
            icon: "ss-alert",
            close: "true"
          },
          modalReject: {
            title: "Are you positively sure?",
            content: "Let your S.O. know why below...",
            primary_btn: "No thanks.",
            icon: "ss-alert",
            placeholder: "(optional: That’s sad. Please explain why you have decided to miss this event, darling.)",
            close: "true"
          },
          modalHeldup: {
            title: "Held Up",
            content: "Let your sweetheart know you’re late!",
            primary_btn: "Held up",
            icon: "ss-alert",
            options: {
              mainlabel: "Held up",
              labels: ["15 minutes", "30 minutes", "45 minutes", "Not coming"],
              values: ["Sorry darling, but your lil’ Lurve Muffin is going to be a tiny 15 minutes late.", "Sorry darling, but your Smoochy Buckets will be 30 minutes late.\nHow about a coke float while you wait?", "So sorry darling, but your Love Dumpling will be 45 minutes late.\nShake yourself that extra martini.", "Catastrophe has struck darling!\nYour Sweetie can’t make it and I’m sure it is something VERY IMPORTANT.\nBut don’t fret, you’ll hear from them soon."]
            },
            close: "true"
          }
        },
        strings: {
          titlebar: {
            title: "Held Up Events",
            buttons: {
              right: "Create"
            }
          },
          headerfooter: {
            setbypartner: "Events for Me",
            setbyuser: "Events for"
          }
        }
      },

      "dashboard-hello": {
        strings: {
          header: {
            title: "Invite your significant other to an ‘event’ - whilst offering a little incentive for punctual attendance.",
            subtitle: "Remember darlings, rewards can be naughty or nice...<br>Martinis and kisses!<br>xxx"
          },
          buttons: {

            createevent: "Create Event"
          }
        }
      },

      "dashboard-hello-nudge": {
        strings: {
          header: {
            title: "It looks like you’re not in the game yet sweet cheeks. How sad.",
            subtitle: "Have the S.O. of yours invite you to an event – let’s get you off the bench!"
          },
          buttons: {
            createevent: "Create Event"
          }
        }
      },

      "event-changed": {
        strings: {
          label: "Changed",
          neweventlocation: "New Event Location",
          neweventdate: "New Event Date",
          buttons: {
            heldup: "Held Up!",
            reject: "No thanks."
          },
          recurringevents: "Recurring Events"
        }
      },

      "event-deleted-partner": {
        strings: {
          message: "Event Deleted",
          label: "Deleted",
          title: "Your invitation has been deleted.",
          norewardselected: "No Reward Selected",
          buttons: {
            createnew: "Create Event",
            remove: "Dismiss!"
          }
        }
      },

      "event-deleted-user": {
        strings: {
          message: "Event Declined",
          label: "Declined",
          title: "Your event has been declined",
          norewardselected: "No Reward Selected",
          buttons: {
            createnew: "Create Event",
            remove: "I’ll Move On"
          }
        }
      },

      "event-heldup": {
        strings: {
          label: "Held Up!",
          partnerisheldup: "<p>Your Significant Other has been Held Up.</p><p>Don’t be too glum sweetie - go do something lovely that’s just for you.</p><br>",
          recurringevents: "Recurring Events",
          buttons: {
            edit: "Edit",
            remove: "Delete"
          }
        }
      },

      "event-idle-partner": {
        strings: {
          upcominglabel: "Next Event",
          approvedlabel: "Approved!",
          partnerisheldup: "You have said:",
          recurringevents: "Recurring Events",
          buttons: {
            heldup: "Held Up!"
          }
        }
      },

      "event-idle-user": {
        strings: {
          upcominglabel: "Upcoming",
          approvedlabel: "Approved",
          recurringevents: "Recurring Events",
          buttons: {
            remove: "Delete",
            edit: "Edit"
          }
        }
      },

      "event-late": {
        strings: {
          message: "Oh NOOOO darling, you were too late!<br>Better luck next time.",
          buttons: {
            statistics: "View Statistics"
          }
        }
      },

      "event-ontime": {
        strings: {
          message: "Well played.<br>You’re a bit of a big deal - and your sweetie knows it.",
          buttons: {
            rewards: "View Rewards"
          }
        }
      },
      "event-partner-proposal": {
        strings: {
          label: "Action",
          recurringevents: "Recurring Events",
          selectrewardtrophy: "Select your Reward",
          nothappy: "Don’t like what you see?<br>Then suggest your own reward.",
          buttons: {
            accept: "Yes please!",
            reject: "No thanks."
          }
        }
      },
      "event-user-proposal": {
        strings: {
          label: "Action",
          recurringevents: "Recurring Events",
          selectrewardtrophy: "Select your Reward",
          nothappy: "Don’t like what you see?<br>Then suggest your own reward.",
          buttons: {
            accept: "Yes please!",
            reject: "No thanks."
          }
        }
      },
      "event-past": {
        strings: {
          label: "On Time?",
          recurringevents: "Recurring Events",
          partnerisheldup: "Your lover has been Held Up",
          message: "Did they make it on time?",
          buttons: {
            ontime: "On time!",
            late: "Too late."
          }
        }
      },
      "event-pending-partner": {
        strings: {
          label: "Pending",
          recurringevents: "Recurring Events",
          rewardsuggestions: "Reward Suggestions",
          buttons: {
            edit: "Edit",
            remove: "Delete"
          }
        }
      },
      "event-pending-user": {
        strings: {
          label: "Pending",
          recurringevents: "Recurring Events",
          rewardsuggestions: "Reward Suggestions",
          buttons: {
            edit: "Edit",
            remove: "Delete"
          }
        }
      },
      setpwd: {
        validation: {
          rules: {},
          messages: {
            password: {
              required: "Please insert your password. You sexy beast.",
              minlength: "At least {0} characters are required."
            }
          }
        },
        strings: {
          titlebar: {
            title: "Set Password",
            buttons: {
              right: "Next"
            }
          },
          form: {
            header: {
              title: "New Password",
              subtitle: "Your password should be a minimum of 8 characters"
            },
            placeholders: {
              password: "Your password"
            }
          }
        }
      },

      changepwd: {
        validation: {
          rules: {},
          messages: {
            password: {
              required: "Please insert your password.",
              minlength: "At least {0} characters are required."
            }
          }
        },
        strings: {
          titlebar: {
            title: "Change Password",
            buttons: {
              left: "Back",
              right: "Next"
            }
          },
          form: {
            header: {
              title: "New Password",
              subtitle: "Your password should be a minimum of 8 characters"
            },
            placeholders: {
              password: "Your new password"
            }
          }
        }
      },

      login: {
        validation: {
          rules: {},
          messages: {
            key: {
              required: "Please insert your key."
            }
          }
        },
        dialogs: {
          incorrectCredentials: {
            title: "Sorry, that didn't work!",
            content: "Looks like you have incorrect sign in details.",
            primary_btn: "Try Again",
            icon: "ss-alert"
          }
        },
        strings: {
          titlebar: {
            title: "Login",
            buttons: {
              left: "Back",
              right: "Done"
            }
          },
          form: {
            placeholders: {
              email: "Your email",
              password: "Your password"
            },
            value: {
              email: "",
              password: ""
            },
            footer: {
              paragraph: "Have you <a href='#lostpwd'>lost your password?</a>"
            }
          }
        }
      },

      invite: {
        validation: {
          rules: {},
          messages: {}
        },
        dialogs: {
          alreadyConnected: {
            title: "So sorry, this just didn't work",
            content: "It seems that this user is unavailable.<br>They must be very popular.",
            primary_btn: "Fine - blank them!",
            icon: "ss-skull"
          },
          success: {
            title: "Invitation Sent",
            content: "<p>An email is winging its way to your Significant Other.</p><p>Make sure they sign up.</p><p>It’ll be super dull playing only with yourself.</p><p>Unless that’s your thing darling.</p><p>No judgement here.</p>",
            icon: "ss-send"
          }
        },
        strings: {
          titlebar: {
            title: "Invite Significant Other",
            buttons: {
              left: "Sign out",
              right: "Invite"
            }
          },
          form: {
            header: {
              title: "Invitation to ‘connect’.",
              subtitle: "Email a Held Up invitation to your Significant Other."
            },
            placeholders: {
              email: "S.O.'s Email Address"
            }
          }
        }
      },

      createevent: {
        validation: {
          rules: {},
          messages: {
            location: {
              required: "Select a location!"
            },
            name: {
              required: "Enter your event name."
            },
            "dummy-date": {
              required: "Enter the date."
            },
            "dummy-time": {
              required: "Select a time."
            },
            reward1: {
              required: ""
            }
          }
        },
        dialogs: {},
        strings: {
          titlebar: {
            title: "Create Event",
            buttons: {
              left: "Back",
              right: {
                submit: "Done",
                next: "Next"
              }
            }
          },
          form: {
            event_start: {
              header: {
                title: "Start",
                subtitle: "<p>Give your event a name - make it irresistible!</p><p>Then, add how many times your S.O. must turn up to collect a reward!</p>"
              },
              placeholders: {
                name: "Event Name",
                repeating: {
                  title: "Number of event repeats:",
                  once: "Once only",
                  twice: "Twice",
                  threex: "Three times",
                  fourx: "Four times"
                }
              }
            },
            event_datetime: {
              header: {
                title: "Date & Time",
                subtitle: "<p>Now, set a date and time.</p><p> Make it at least one hour in advance. Give your lover time to prepare!</p>"
              },
              placeholders: {
                date: "Enter the date.",
                time: "Select a time."
              }
            },
            event_location: {
              header: {
                title: "Location",
                subtitle: "Where is your cheeky little event taking place?"
              },
              placeholders: {
                location: "Search for that perfect location."
              }
            },
            event_rewards: {
              header: {
                title: "Rewards",
                subtitle: "Add rewards for your significant other to select.<br>Don’t be shy sweetie - be a little creative!<br>Or, if you’re stumped, use the ones below for inspo."
              },
              placeholders: {
                reward1: "Try, wine & lovely expensive chocolates.(Or, vice versa.)",
                reward2: "Or, massage with a VERY happy ending, darling?",
                reward3: "Or even, a ticket to the gun show. Front row seats. Ooooh!"
              }
            },
            event_preview: {
              header: {
                title: "Good for you!",
                subtitle: "Now, tap ‘Done’ and enjoy your delicious little rendezvous."
              }
            }
          }
        }
      },

      editevent: {
        validation: {
          rules: {},
          messages: {}
        },
        dialogs: {},
        strings: {
          titlebar: {
            title: "Edit Event",
            titlelocation: "Edit Location",
            buttons: {
              left: "Back",
              right: "Done"
            }
          },
          form: {
            event_datetime: {
              header: {
                subtitle: "<p>Set a date and time.</p><p>Make it least one hour in advance! Give your S.O. time to prepare - it’s only polite.</p>"
              },
              placeholders: {
                date: "Enter the date.",
                time: "Select a time."
              }
            },
            event_location: {
              header: {
                title: "Location",
                subtitle: "Where is your cheeky little event taking place?"
              },
              placeholders: {
                location: "Search for a location!"
              }
            }
          }
        }
      },

      invitations: {
        strings: {
          titlebar: {
            title: "Invitations",
            buttons: {
              left: "Logout"
            }
          },
          header: {
            title: "Do you want to connect with someone else?",
            subtitle: "Before you can invite another Significant Other, you must first decline all pending invitations."
          }
        }
      },

      invitation: {
        validation: {
          rules: {},
          messages: {}
        },
        dialogs: {
          failsafeReject: {
            title: "Are you completely sure?",
            content: "Let your S.O. know why you won’t make it sweet cheeks...",
            icon: "ss-deleteheart",
            primary_btn: "Decline",
            placeholder: "Please explain why you want to miss out on this event.",
            close: "true"
          },
          failsafeAccept: {
            title: "Are you absolutely sure?",
            content: "<p>Good choice!<p/><p>You are accepting the invitation.</p>",
            icon: "ss-addheart",
            primary_btn: "Yes",
            close: "true"
          }
        },
        strings: {
          title: "Connection Invitation",
          buttons: {
            accept: "Accept",
            reject: "Decline"
          }
        }
      },

      connected: {
        validation: {
          rules: {},
          messages: {}
        },
        dialogs: {
          failsafeDisconnect: {
            title: "Are you absolutely, positively sure?",
            content: "Be warned, this cannot be undone!<br>All your events, rewards and statistics will be deleted from Held Up.<br> :-(",
            icon: "ss-deleteheart",
            close: "true"
          }
        },
        strings: {
          titlebar: {
            title: "Connected",
            buttons: {
              left: "Back"
            }
          },
          title: "You are connected with:",
          buttons: {
            disconnect: "Disconnect!"
          }
        }
      },

      map: {
        strings: {
          titlebar: {
            title: "Event Location",
            buttons: {
              left: "Back"
            }
          }
        }
      },

      pending: {
        validation: {
          rules: {},
          messages: {}
        },
        dialogs: {
          failsafeCancel: {
            title: "Are you 100% sure?",
            content: "You are about to decline this request.",
            icon: "ss-deleteheart",
            placeholder: "<p>Would you like to provide a reason for declining?  It’s best to be polite!</p><p>Especially if they know where you live...</p>",
            close: "true"
          }
        },
        strings: {
          titlebar: {
            title: "Pending",
            buttons: {
              left: "Logout"
            }
          },
          title: "Connection is pending",
          buttons: {
            cancel: "Cancel"
          }
        }
      },

      "default": {
        validation: {
          rules: {},
          messages: {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            accept: "Please enter a value with a valid extension.",
            maxlength: "Please enter no more than {0} characters.",
            minlength: "Please enter at least {0} characters.",
            rangelength: "Please enter a value between {0} and {1} characters long.",
            range: "Please enter a value between {0} and {1}.",
            max: "Please enter a value less than or equal to {0}.",
            min: "Please enter a value greater than or equal to {0}.",
            example_input_name: {
              required: "Please input `Example Input Name`."
            }
          }
        },

        dialogs: {
          failsafeLogout: {
            title: "Are you quite sure?",
            content: "You are about to sign out. It will be super dull without you. Please visit again soon!",
            icon: "ss-skull",
            close: "true"
          },
          connectionlost: {
            title: "Oh no!  Your connection has been lost.",
            content: "Please try again later.",
            icon: "ss-skull",
            close: "false"
          },
          systemerror: {
            title: "Boring Server-side Error",
            content: "Please try again later!",
            icon: "ss-skull",
            close: "false"
          },
          timeout: {
            title: "It’s timed out.",
            content: "Your action took too long time for the server to respond. It’s all too technical for me. But, please do try again later!",
            icon: "ss-skull",
            close: "false"
          },
          aborted: {
            title: "Aborted. :-(",
            content: "Your action has somehow been aborted. No idea why, but please try again later!",
            icon: "ss-skull",
            close: "false"
          },
          templatePrompt: {
            title: "This is the Title",
            content: "This is the Message",
            primary_btn: "Yes",
            close: true,
            icon: "ss-notifications"
          },
          templateAlert: {
            title: "This is the Title",
            content: "This is the Message",
            primary_btn: "Yes",
            close: true,
            icon: "ss-notifications"
          }
        }
      }
    }
  };

  return Language;
});