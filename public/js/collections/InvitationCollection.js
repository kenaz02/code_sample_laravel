// Category Collection
// ===================

// Includes file dependencies
define([ "jquery","backbone", "config", "models/InvitationModel" ], function( $, Backbone, Config, Model ) {

    // Extends Backbone.Router
    var Collection = Backbone.Collection.extend( {
    	model: Model,
    	url: Config.network.url("/invitations")
    	
    } );

    // Returns the Model class
    return Collection;

} );