// Category Collection
// ===================

// Includes file dependencies
define([ "jquery","backbone" ], function( $, Backbone ) {

    // Extends Backbone.Router
    var Collection = Backbone.Collection.extend( {} );

    // Returns the Model class
    return Collection;

} );