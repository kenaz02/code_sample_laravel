
define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",
        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        $ ) {


    var View = BaseView.extend( {
        model: UserSession,
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
        },
        disconnect: function(){
            var _this = this;
            BaseView.prototype.alert.call(this, Language.view[_this.config.name].dialogs.failsafeDisconnect)
            .$primary_btn.on("click", function(){
                _this.model.disconnect({
                    200: function(jd){_this.form.disconnectTriggered(jd, _this)},
                });
            });
            
        },
        form: {
            disconnectTriggered: function(jsondata, _this) { 
                UserSession.fetch({
                    success: function(){
                        window.changePage("#dashboard", "left");
                    }
                });

            }
        },
        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){});
            return _this;
        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{
            "click .disconnect" : "disconnect"
          });
        }
    });
    // Returns the View class
    return View;

});