
define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",

        "views/RewardSubview",
        "models/RewardModel",
        "views/DashboardHelloSubview",
        'goog!visualization,1,packages:[corechart,geochart]',

        "jquery", 
        "jqueryvalidate",


        ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        Subview,
        Model,
        DashboardHelloSubview,
        googleapi,

        $ ) {

    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
            this.loadFooter(this.id);
            this.model = UserSession;
        },

        render: function(){
            window.google.load("visualization", "1", {packages:["corechart"]});
            window.google.setOnLoadCallback(this.renderFeed);
            var _this = this;
            BaseView.prototype.render.call(_this, function(){

                _this.renderFeed();
                
            });
            return _this;
        },
        renderFeed: function(){
            var _this = this;


            var d = [0,0];
            if( ! this.v){
                d[0] = parseInt(UserSession.get('ontime'));
                d[1] = parseInt(UserSession.get('late'));

                if( d[0] == 0 && d[1] == 0 ){
                    var ds = new DashboardHelloSubview({
                        name: "dashboard-hello"
                    });
                    $("#piechart").html(ds.render().$el);
                    return;
                }

            }else
            {
                d[0] = parseInt(UserSession.get('partner').ontime);
                d[1] = parseInt(UserSession.get('partner').late);
                if( d[0] == 0 && d[1] == 0 ){
                    var ds = new DashboardHelloSubview({
                        name: "dashboard-hello"
                    });
                    $("#piechart").html(ds.render().$el);
                    return;
                }
            }

            var maxv = Math.max(d[0], d[1]) + 1;
            maxv = 10*Math.ceil(maxv/10);
            var data = window.google.visualization.arrayToDataTable([
                ['Total', 'Total', { role: 'style' }],
                ['On Time', d[0], '#11ca64'],
                ['Late', d[1], '#e84a4a'],
            ]);

            var options = {
                title: '',
                'tooltip' : {
                trigger: 'true'
                },
                tooltip: {isHtml: true},
                backgroundColor: 'transparent',
                legend: {position: 'none', textStyle: {color: '#222', fontSize: 13}},
                enableInteractivity: true,
                vAxis: {
                    maxValue: maxv,
                    minValue: 0,
                    gridlines: {
                        count: 11,
                        color: '#dcd4d0'
                    }
                }
            };

            var chart = new window.google.visualization.ColumnChart(document.getElementById('piechart'));
            chart.draw(data, options);
        },

        $feed: 0,
        v: 0,
        showPartners: function(){
            var _this = this;
            _this.v = 1;
            $(_this.el).find(".header-footer a").removeClass("active");
            $(_this.el).find(".header-footer .partner").addClass("active");
            _this.renderFeed();
        },
        showUsers: function(){
            var _this = this;
            _this.v = 0;
            $(_this.el).find(".header-footer a").removeClass("active");
            $(_this.el).find(".header-footer .user").addClass("active");
            _this.renderFeed();

        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{
            "click .header-footer .partner": "showPartners",
            "click .header-footer .user": "showUsers"
          });
        },
        sync: function(_this){
            var do_render = 0;
            if( typeof UserSession.changed.ontime != 'undefined' ){
                do_render = 1;
            }
            if( typeof UserSession.changed.late != 'undefined' ){
                do_render = 1;
            }
            if( typeof UserSession.changed.partner != 'undefined' )
            {
                if( typeof UserSession.changed.partner.ontime != 'undefined' ){
                    do_render = 1;
                }
                if( typeof UserSession.changed.partner.late != 'undefined' ){
                    do_render = 1;
                }
            }

            if( do_render )
                _this.renderFeed();
        }

    });

    // Returns the View class
    return View;

} );


