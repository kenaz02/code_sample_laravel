define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",

        "models/UserModel",
        "models/EventModel",
        "views/EventSubview",
        "views/DashboardHelloSubview",

        "jquery", 
        "jqueryvalidate",

        ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        UserModel,
        Model,
        Subview,
        DashboardHelloSubview,
        $ ) {

    // This script is released to the public domain and may be used, modified and
    // distributed without restrictions. Attribution not necessary but appreciated.
    // Source: http://weeknumber.net/how-to/javascript 

    // Returns the ISO week of the date.
    Date.prototype.getWeek = function() {
      var date = new Date(this.getTime());
       date.setHours(0, 0, 0, 0);
      // Thursday in current week decides the year.
      date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
      // January 4 is always in week 1.
      var week1 = new Date(date.getFullYear(), 0, 4);
      // Adjust to Thursday in week 1 and count number of weeks from date to week1.
      return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
                            - 3 + (week1.getDay() + 6) % 7) / 7);
    }

    // Returns the four-digit year corresponding to the ISO week of the date.
    Date.prototype.getWeekYear = function() {
      var date = new Date(this.getTime());
      date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
      return date.getFullYear();
    }

    function intToWord(i){
        switch(i){
            case 1:return "One"; break;
            case 2:return "Two"; break;
            case 3:return "Three"; break;
            case 4:return "Four"; break;
            case 5:return "Five"; break;
            case 6:return "Six"; break;
            case 7:return "Seven"; break;
            case 8:return "Eight"; break;
            case 9:return "Nine"; break;
            case 10:return "Ten"; break;
            case 11:return "Eleven"; break;
            default: return "Unknown"; break;
        }
    }
    function tConvert (time) {
      // Check correct time format and split into components
      time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

      if (time.length > 1) { // If time format correct
        time = time.slice (1);  // Remove full string match value
        time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
      }
      return time.join (''); // return adjusted time or original string
    }

    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
            this.loadFooter(this.id);
            this.model = UserSession;
            this.gid = this.config.id;
        },
        flagoption: {
            sent:{
                pending: 1,
                proposal: 2,
                idle: 4,
                past: 8,
                deleted: 16,
                heldup: 32
            },
            recv:{
                pending: 64,
                proposal: 128,
                idle: 256,
                changed: 512,
                deleted: 1024,
                ontime: 2048,
                late: 4096
            }
        },
        renderUserEvent: function(flag){
            if( flag & this.flagoption.sent.pending)
                return "event-pending-user";
            else if( flag & this.flagoption.sent.proposal)
                return "event-user-proposal";
            else if( flag & this.flagoption.sent.idle)
                return "event-idle-user";
            else if( flag & this.flagoption.sent.past)
                return "event-past";
            else if( flag & this.flagoption.sent.deleted)
                return "event-deleted-user";
           else if( flag & this.flagoption.sent.heldup)
                return "event-heldup";
        },
        renderPartnerEvent: function(flag){
            if( flag & this.flagoption.recv.pending)
                return "event-pending-partner";
            else if( flag & this.flagoption.recv.proposal)
                return "event-partner-proposal";
            else if( flag & this.flagoption.recv.idle)
                return "event-idle-partner";
            else if( flag & this.flagoption.recv.changed)
                return "event-changed";
            else if( flag & this.flagoption.recv.deleted)
                return "event-deleted-partner";
            else if( flag & this.flagoption.recv.ontime)
                return "event-ontime";
            else if( flag & this.flagoption.recv.late)
                return "event-late";
        },
        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){
                _this.renderFeed();
            });        
            return _this;
        },
        seed: {
            UserEvents: [],
            PartnerEvents: []
        },
        renderFeed: function(){
                var _this = this;
                _this.$feed = _this.$el.find(".feed");
                _this.$el.find(".header-footer .bullet").hide();

                var $users = _this.$feed.find(".users").clone();
                var $partners = _this.$feed.find(".partners").clone();
                $users.html("");
                $partners.html("");
                this.seed = {
                    UserEvents: [],
                    PartnerEvents: []
                };

                var mycount = 0;
                var partnercount = 0;
                for(var i in UserSession.get("events")){
                    var now = new Date();
                    var arr = ( UserSession.get("events")[i].date+" "+UserSession.get("events")[i].time + ":00" ).split(/[- :]/);
                    var xdate = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                    
                    //Ordering list of events
                    var SourceRepets = UserSession.get("events")[i].repeating;
                    var EventTimestamp = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                	if(SourceRepets > 0){
                		var Repetions = 0;
        	        	while(EventTimestamp < now && SourceRepets > Repetions){
        	        		EventTimestamp = new Date(EventTimestamp.getTime() + (1000*60*60*24*7));
        	        		Repetions += 1;
        	        	}
                	}
                    var ontime_count = parseInt(UserSession.get("events")[i].ontime_count);

                    xdate.setDate(xdate.getDate() + ontime_count*7);

                    var flag = parseInt(UserSession.get("events")[i].flag);
                    console.log(flag);
                    if( flag & _this.flagoption.recv.deleted && 
                        flag & _this.flagoption.sent.deleted || ! flag ) continue;

                    if( xdate <= now )
                    {
                        if( flag & _this.flagoption.recv.pending) continue;
                        else if(flag & _this.flagoption.recv.proposal) continue;
                        else if(flag & _this.flagoption.sent.proposal) continue;
                        else if(flag & _this.flagoption.sent.pending) continue;
                    }

                    if( UserSession.get("events")[i].recipient == UserSession.get("id") ){
                        if(flag & _this.flagoption.sent.deleted) continue;
                        EventObj = UserSession.get("events")[i];
                        EventObj.sortdate = EventTimestamp.getTime();
                        _this.seed.PartnerEvents.push(EventObj);
                        partnercount++;
                    }else{
                        if(flag & _this.flagoption.recv.deleted ||
                        flag & _this.flagoption.recv.ontime  ||
                        flag & _this.flagoption.recv.late ) continue;
                        EventObj = UserSession.get("events")[i];
                        EventObj.sortdate = EventTimestamp.getTime();
                        _this.seed.UserEvents.push(EventObj);
                        mycount++;
                    }
                }

                if( !  mycount ){
                    var ds = new DashboardHelloSubview({
                        name: "dashboard-hello"
                    });

                    $users.html(ds.render().$el);                        

                }
                if( ! partnercount ){
                    var ds = new DashboardHelloSubview({
                        name: "dashboard-hello-nudge"
                    });

                    $partners.html(ds.render().$el);
                }
                _this.seed.UserEvents.sort(function(a,b){
                    var b = (b.sortdate), a = (a.sortdate);
                    return ( (b) < a) ? 1 : (b > a) ? -1 : 0;
                });
                _this.seed.PartnerEvents.sort(function(a,b){
                    var b = (b.sortdate), a = (a.sortdate);
                    return (b < a) ? 1 : (b > a) ? -1 : 0;
                });


               var date_stack = {};
                var firsta = false, firstb = false;
                var count = {past: 0, proposal: 0, deleted: 0, heldup: 0};
                var upcoming = 0;
                for(index in this.seed.UserEvents)
                {
                    var data = this.seed.UserEvents[index];
                    var model = new Model(data);
                    var config = {"model": model, parent: _this};
                    var flag = model.get("flag"); //real deal

                    if(flag & _this.flagoption.recv.deleted ||
                       flag & _this.flagoption.recv.ontime  ||
                       flag & _this.flagoption.recv.late ){ 
                        continue;
                    }

                    model.set("dates", 0);
                    if( model.get("repeating") > 0 ){
                        var dates = [];
                        var x = parseInt( model.get("repeating") ) + 1;
                        for(var i = 0; i < x; i++ ){
                            var arr = (model.get("date")+" "+model.get("time") + ":00" ).split(/[- :]/);
                            var xdate = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                            xdate.setHours(0,0,0,0);
                            xdate.setDate(xdate.getDate() + (i*7));
                            var days = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
                            var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                            "Aug", "Sep", "Oct", "Nov", "Dec" ];
                            var month = months[xdate.getMonth()];     
                            var date = xdate.getDate() + " " + month + " " + xdate.getFullYear() + ", " + tConvert(model.get("time"));       
                            dates.push(date);
                        }
                        model.set("dates", dates);
                    }

                    var now_only_date = new Date();
                    now_only_date.setHours(0,0,0,0);
                    var now = new Date();
                    var arr = ( model.get("date")+" "+model.get("time") + ":00" ).split(/[- :]/);
                    var xdate = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                    var ontime_count = parseInt(model.get("ontime_count"));
                    xdate.setDate(xdate.getDate() + ontime_count*7);

                    var mdate = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                    mdate.setHours(0,0,0,0);
                    mdate.setDate(mdate.getDate()-7);
                    if( mdate <= now_only_date && upcoming != 1 && ! (flag & _this.flagoption.recv.deleted) ){
                        model.set("upcoming", 1);
                        upcoming = 1;
                    } else model.set("upcoming", 0);

                    var days = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
                    var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                    "Aug", "Sep", "Oct", "Nov", "Dec" ];
                    var month = months[xdate.getMonth()];
                      

                    var date = xdate.getDate() + " " + month + " " + xdate.getFullYear() + ", " + tConvert(model.get("time"));
                    if( xdate <= now ){
                      if( flag & _this.flagoption.recv.pending)
                            flag = 0 | _this.flagoption.sent.deleted | _this.flagoption.recv.deleted;
                        else if(flag & _this.flagoption.recv.proposal)
                            flag = 0 | _this.flagoption.sent.deleted | _this.flagoption.recv.deleted;
                        else{
                            var b = 0;
                            if( flag & 8192 ){b=1;}
                            flag = 0 | _this.flagoption.sent.past | _this.flagoption.recv.idle;
                            if(b==1)
                            flag = flag | 8192;
                        } 
                    }
                    model.set("date", date);

                    config["name"] = _this.renderUserEvent(flag);
                    var subview = new Subview(config);
                    var sel = subview.render();
                    if( ! firsta && typeof _this.gid == 'undefined' ){
                        sel.$el.bind('afterRender', function(){
                            $(this).find('.item').not(".no-click").click();
                        });
                        firsta = true;
                    }
                    
                    if( ! firsta && typeof _this.gid != 'undefined' )
                    {
                        if(config.model.id == _this.gid){
                            console.log(_this.gid);
                            sel.$el.bind('afterRender', function(){
                               //  _this.showUsers(1);
                                // $(this).find('.item').not(".no-click").click();
                            });
                            firsta = true;
                        }
                    }           

                    if( flag & _this.flagoption.sent.pending){
                        
                    }
                    else if( flag & _this.flagoption.sent.heldup){
                        count.heldup++;
                    }
                    else if( flag & _this.flagoption.sent.deleted){
                        count.deleted++;
                    }
                    else if( flag & _this.flagoption.sent.proposal){
                        count.proposal++;
                    }
                    else if( flag & _this.flagoption.sent.idle){
                    }
                        
                    else if( flag & _this.flagoption.sent.past){
                        count.past++;
                    }


 
                    date_stack[date] = $("<div/>").addClass("datetime");

                    var total_count = 0;
                    for(var i in count){
                        total_count += count[i];
                    }
                    if( total_count)
                    $(".header-footer .bullet.partner").html(total_count).show();

                    date_stack[date].append(sel.$el);
                    $users.append(date_stack[date]);
                }

                 date_stack = {};
                 var countp = {changed: 0, proposal: 0, deleted: 0, ontime: 0, late: 0};
                 var upcomingp = 0;
                 for(index in this.seed.PartnerEvents)
                 {
                    var data = this.seed.PartnerEvents[index];
                    var model = new Model(data);
                    var config = {"model": model, parent: _this};
                    var flag = model.get("flag"); //real deal
                    
                    if(flag & _this.flagoption.sent.deleted) continue;

                    model.set("dates", 0);
                    if( model.get("repeating") > 0 ){
                        var dates = [];
                        var x = parseInt( model.get("repeating") ) + 1;
                        for(var i = 0; i < x; i++ ){
                            var arr = (model.get("date")+" "+model.get("time") + ":00" ).split(/[- :]/);
                            var xdate = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                            xdate.setHours(0,0,0,0);
                            xdate.setDate(xdate.getDate() + (i*7));
                            var days = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
                            var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                            "Aug", "Sep", "Oct", "Nov", "Dec" ];
                            var month = months[xdate.getMonth()];     
                            var date = xdate.getDate() + " " + month + " " + xdate.getFullYear() + ", " + tConvert(model.get("time"));       
                            dates.push(date);
                        }
                        model.set("dates", dates);
                    }
                  
                    var now_only_date = new Date();
                    now_only_date.setHours(0,0,0,0);
                    var now = new Date();
                    var arr = ( model.get("date")+" "+model.get("time") + ":00" ).split(/[- :]/);
                    var xdate = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                    var ontime_count = parseInt(model.get("ontime_count"));
                    xdate.setDate(xdate.getDate() + ontime_count*7);

                    var mdate = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                    mdate.setHours(0,0,0,0);
                    mdate.setDate(mdate.getDate()-7);
                    if( mdate <= now_only_date && upcomingp != 1 && ! (flag & _this.flagoption.recv.deleted) ){
                        model.set("upcoming", 1);
                        upcomingp = 1;
                    } else model.set("upcoming", 0);

                    var days = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
                    var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                    "Aug", "Sep", "Oct", "Nov", "Dec" ];
                    var month = months[xdate.getMonth()];
                    console.log("---flag:" + flag + "\n");

                    var date = xdate.getDate() + " " + month + " " + xdate.getFullYear() + ", " + tConvert(model.get("time"));              
                    if( xdate <= now ){
                        console.log("flag:" + flag + "\n");
                        console.log("(flag & _this.flagoption.recv.late):" + (flag & _this.flagoption.recv.late) + "\n");
                        console.log("(flag & _this.flagoption.recv.ontime):" + (flag & _this.flagoption.recv.ontime) + "\n");
                        if( flag & _this.flagoption.recv.late){} 
                        else if (flag & _this.flagoption.recv.ontime){}
                        else
                        {
                            if( flag & _this.flagoption.recv.pending)
                                flag = 0 | _this.flagoption.sent.deleted | _this.flagoption.recv.deleted;
                            else if(flag & _this.flagoption.recv.proposal)
                                flag = 0 | _this.flagoption.sent.deleted | _this.flagoption.recv.deleted;
                            else
                                flag = 0 | _this.flagoption.sent.past | _this.flagoption.recv.idle;                      
                        }
                        
                    }
                    model.set("date", date);


                    config["name"] = _this.renderPartnerEvent(flag);
                    var subview = new Subview(config); 
                    var sel = subview.render();
                    if( ! firstb && typeof _this.gid == 'undefined' ){
                        sel.$el.bind('afterRender', function(){
                            //_this.showPartners(1);
                            // $(this).find('.item').not(".no-click").click();
                        });
                        firstb = true;
                    }

                    if( ! firstb && typeof _this.gid != 'undefined' )
                    {
                        if(config.model.id == _this.gid){
                            console.log(_this.gid);
                            sel.$el.bind('afterRender', function(){
                                 $(this).find('.item').not(".no-click").click();
                            });
                            firstb = true;
                        }
                    }     

                    if( flag & _this.flagoption.recv.pending){

                    }
                    else if( flag & _this.flagoption.recv.deleted){
                        countp.deleted++;
                    }
                    else if( flag & _this.flagoption.recv.proposal){
                        countp.proposal++;
                    }
                    else if( flag & _this.flagoption.recv.changed){
                        countp.changed++;
                    }
                    else if( flag & _this.flagoption.recv.idle){
                    }
                        
                    else if( flag & _this.flagoption.recv.past){
                        countp.past++;
                    }
                    else if( flag & _this.flagoption.recv.ontime){
                        countp.ontime++;
                    }
                    else if( flag & _this.flagoption.recv.late){
                        countp.late++;
                    }

 
                    date_stack[date] = $("<div/>").addClass("datetime");

                    var total_countp = 0;
                    for(var i in countp){
                        total_countp += countp[i];
                    }
                    if( total_countp)
                    $(".header-footer .bullet.user").html(total_countp).show();

                    date_stack[date].append(sel.$el);

                    $partners.append(date_stack[date]);
                }

                _this.$feed.find(".users").replaceWith($users);
                _this.$feed.find(".partners").replaceWith($partners);
        },
        $feed: 0,
        v: 0,
        showPartners: function(q){
            var _this = this;
                _this.v = 1;
                $(_this.el).find(".header-footer a").removeClass("active");
                $(_this.el).find(".header-footer .partner").addClass("active");
                _this.$el.find(".feed .partners, .feed .users").removeClass("show");
                _this.$el.find(".feed .partners").addClass("show");
                if(typeof q == undefined)
                _this.renderFeed();
        },
        showUsers: function(q){
            var _this = this;
            _this.v = 0;
            $(_this.el).find(".header-footer a").removeClass("active");
            $(_this.el).find(".header-footer .user").addClass("active");
            _this.$el.find(".feed .partners, .feed .users").removeClass("show");
            _this.$el.find(".feed .users").addClass("show");
            if(typeof q == undefined)
            _this.renderFeed();          

        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{
            "click .header-footer .partner": "showPartners",
            "click .header-footer .user": "showUsers"
          });
        },
        lastminute: 0,
        past_ev: [],
        wasAttributes: null,
        sync: function(_this) {

            var do_render = 0;

            if (this.wasAttributes != null) {
                if (false != UserSession.changedAttributes()) {
                    this.wasAttributes = null;
                    do_render = 1;
                }
            }
            else {
                if (UserSession.changedAttributes() != null) {
                    this.wasAttributes = UserSession.changedAttributes();
                }
            }


            var minutes_now = new Date().getMinutes();
            if (minutes_now != this.lastminute) {
                this.lastminute = minutes_now;
                var now = new Date();
                for (var index in this.seed.UserEvents) {

                    var model = this.seed.UserEvents[index];

                    var arr = ( model.date + " " + model.time + ":00" ).split(/[- :]/);
                    var xdate = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                    var ontime_count = parseInt(model.ontime_count);
                    xdate.setDate(xdate.getDate() + ontime_count * 7);

                    if (xdate <= now) {
                        do_render = 1;
                        break;
                    }
                }
            }

            console.log('do_render ' + do_render);
            if (do_render){
                this.renderFeed();


                /********************
                 Update reminder
                 ********************/

                    //Set reminder1 and reminder2 and message1 and message2
                rem1=UserSession.get("reminder1");
                rem2=UserSession.get("reminder2");

                if (rem1 == 1){min1=0;msg1 = Language.view["reminders"].notifications.time_event;}
                if (rem1 == 2){min1=5;msg1 = Language.view["reminders"].notifications.mins5;}
                if (rem1 == 3){min1=15;msg1 = Language.view["reminders"].notifications.mins15;}
                if (rem1 == 4){min1=30;msg1 = Language.view["reminders"].notifications.mins30;}
                if (rem1 == 5){min1=60;msg1 = Language.view["reminders"].notifications.hours1;}
                if (rem1 == 6){min1=120;msg1 = Language.view["reminders"].notifications.hours2;}
                if (rem1 == 7){min1=60 * 24;msg1 = Language.view["reminders"].notifications.days1;}
                if (rem1 == 8){min1=60 * 24 * 2;msg1 = Language.view["reminders"].notifications.days2;}
                if (rem1 == 9){min1=60 * 24 * 7;msg1 = Language.view["reminders"].notifications.days7;}

                if (rem2 == 1){min2=0;msg2 = Language.view["reminders"].notifications.time_event;}
                if (rem2 == 2){min2=5;msg2 = Language.view["reminders"].notifications.mins5;}
                if (rem2 == 3){min2=15;msg2 = Language.view["reminders"].notifications.mins15;}
                if (rem2 == 4){min2=30;msg2 = Language.view["reminders"].notifications.mins30;}
                if (rem2 == 5){min2=60;msg2 = Language.view["reminders"].notifications.hours1;}
                if (rem2 == 6){min2=120;msg2 = Language.view["reminders"].notifications.hours2;}
                if (rem2 == 7){min2=60 * 24;msg2 = Language.view["reminders"].notifications.days1;}
                if (rem2 == 8){min2=60 * 24 * 2;msg2 = Language.view["reminders"].notifications.days2;}
                if (rem2 == 9){min2=60 * 24 * 7;msg2 = Language.view["reminders"].notifications.days7;}

                /*window.plugin.notification.local.getScheduledIds(function(scheduledIds) {
                 console.log('1 - Get schedules notification');
                 for (var i = 0; scheduledIds.length > i; i++) {
                 //window.plugin.notification.local.cancel(scheduledIds[i]);
                 console.log(scheduledIds[i]);
                 }
                 });*/

                window.setTimeout(function(){
                    //Cancel all local notification
                    window.plugin.notification.local.cancelAll(function () {
                        //At this point all notifications have been canceled
                        //console.log('2 - Cancel all notification');
                    });
                }, 1000);

                /*window.setTimeout(function(){
                 window.plugin.notification.local.getScheduledIds(function(scheduledIds) {
                 console.log('3 - Get schedules notification');
                 for (var i = 0; scheduledIds.length > i; i++) {
                 console.log(scheduledIds[i]);
                 }
                 });
                 }, 2000);*/

                window.setTimeout(function(){

                    console.log('4 - Reset all notification');

                    //Set new notification
                    var idNotification = 0;
                    var now = new Date;
                    for(var i in UserSession.get("events")) {
                        //Don't send notification for events deleted
                        console.log(UserSession.get("events")[i].name);
                        if (UserSession.get("events")[i].flag != 1028){
                            //new Date(year, month, day [, hour, minute, second, millisecond ])
                            var arr = ( UserSession.get("events")[i].date + " " + UserSession.get("events")[i].time + ":00" ).split(/[- :]/);
                            var reminderEvent = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                            var reminderDate1 = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                            var reminderDate2 = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                            msg1name = msg1 + "\n'" + UserSession.get("events")[i].name+"'";
                            msg2name = msg2 + "\n'" + UserSession.get("events")[i].name+"'";
                            //Send notification only for future events
                            if(reminderEvent > now) {
                                if (rem1 != 0) {
                                    reminderDate1.setMinutes(reminderEvent.getMinutes() - min1);
                                    if(reminderDate1 > now){
                                        console.log('Set not 1, time '+reminderDate1);
                                        window.plugin.notification.local.add({
                                            id:         idNotification,  // A unique id of the notifiction
                                            date:       reminderDate1,    // This expects a date object
                                            message:    msg1name,  // The message that is displayed
                                            //title:      'Held up',  // The title of the message
                                            badge:      1,  // Displays number badge to notification
                                            //autoCancel: Boolean, // Setting this flag and the notification is automatically canceled when the user clicks it
                                        });
                                        idNotification = idNotification + 1;
                                    }
                                }
                                if (rem2 != 0) {
                                    reminderDate2.setMinutes(reminderEvent.getMinutes() - min2);
                                    if(reminderDate2 > now) {
                                        console.log('Set not 2, time '+reminderDate2);
                                        window.plugin.notification.local.add({
                                            id:         idNotification,  // A unique id of the notifiction
                                            date:       reminderDate2,    // This expects a date object
                                            message:    msg2name,  // The message that is displayed
                                            //title:      'Held up',  // The title of the message
                                            badge:      1,  // Displays number badge to notification
                                            //autoCancel: Boolean, // Setting this flag and the notification is automatically canceled when the user clicks it
                                        });
                                        idNotification = idNotification + 1;
                                    }
                                }
                            }
                        }
                    }

                }, 3000);

                /*window.setTimeout(function(){
                 window.plugin.notification.local.getScheduledIds(function(scheduledIds) {
                 console.log('5 - Get schedules notification');
                 for (var i = 0; scheduledIds.length > i; i++) {
                 console.log(scheduledIds[i]);
                 }
                 });
                 }, 4000);
                 */


            }
        }

    });

    // Returns the View class
    return View;

} );





