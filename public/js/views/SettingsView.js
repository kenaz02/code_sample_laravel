
define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",
        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        $ ) {


    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
            this.loadFooter(this.id);      
        },
        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){});
            return _this;
        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{
          });
        }

    });

    // Returns the View class
    return View;

} );