
define([ "backbone", 
        "text!../../../templates/dialog-prompt.html", 
        "jquery", 
        "jqueryvalidate" ], function( Backbone, Template, $ ) {

    // Extends Backbone.View
    var PromptDialogView = Backbone.View.extend( {
        id: "dialog",
        // The View Constructor
        initialize: function(config) {
            this.config = {
                "title":         config.title          || "Prompt",
                "content":       config.content        || "...",
                "primary_btn":   config.primary_btn    || "Yes",
                "secondary_btn": config.secondary_btn  || "No",
                "close":         config.close          && true,
                "icon" :         config.icon           || "ss-notifications"
            };
            this.render();

        },
        events: {
            "click .close,.ok-btn,.bad-btn" : "close"
        },
        popup: function( action ){
            if(action == "close")
            {
                $("#dialog").fadeOut();
            }
            else if(action == "open")
            {
                $("#dialog").fadeIn();
            }
        },
        close: function(){this.popup("close")},
        render: function() {
            $("#dialog, #dialog-view-screen, #dialog-view-popup").remove();
            // Sets the view's template property
            this.template = _.template( Template );
            // Renders the view's template inside of the current listview element
           // this.$el.find("ul").html(this.template);
            $(this.el).html(this.template);

            $(this.el).find(".title").html(this.config.title);
            $(this.el).find("p").html(this.config.content);
            $(this.el).find(".ok-btn").html(this.config.primary_btn);
            $(this.el).find(".bad-btn").html(this.config.secondary_btn);
            $(this.el).find(".circle i").addClass(this.config.icon);
            $(this.el).appendTo( ".ui-page" ).trigger( "create" );
            $("body").prepend($(this.el).hide());
            this.$primary_btn = $("#dialog-view .ok-btn");
            this.$secondary_btn = $("#dialog-view .bad-btn");
            this.config.close ? 0 : $("#dialog-view .close").remove(); 
            return this;
        }

    } );

    return PromptDialogView;

} );