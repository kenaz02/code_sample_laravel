
define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",
        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        $ ) {


    var View = BaseView.extend( {
        initialize: function() {
            BaseView.prototype.initialize.apply(this);
        },

        form: {
            request: function(jsondata, _this)
            {

            },
        },

        render: function(callback) {
            var _this = this;

            $.get("templates/tests/" + _this.config.name + ".html", function(data)
            {
                _this.template = _.template( data );
                $(_this.el).html(_this.template(_this.model.toJSON()));
                callback();
                _this.afterRender();
            });
            return _this;
        },

        events: function(){
          return _.extend({},BaseView.prototype.events,{

          });
        }

    });

    // Returns the View class
    return View;

} );