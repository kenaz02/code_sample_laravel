
define(["backbone", 
        "config",
        "views/BaseView",
        "models/UserSession",
        "models/EventModel",
        "language",
        "gmaps",
        "jquery",
        "jqueryvalidate", ], function( 
        Backbone,
        Config,
        BaseView,
        UserSession,
        Model,
        Language,
        GMaps,
        $ ) {


    var View = BaseView.extend({
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
            this.model = new Model();
        },

        form: {
            request: function(jsondata, _this)
            {
                _this.model[_this.config.name](jsondata, 
                {
                    201: function(jd){_this.form.success(jd, _this)},
                });
            },
            success: function(jsondata, _this) { 
            },
        },
        renderMap: function(lat, lng, marker){
            var map = GMaps({
                div: "#mapcontent",
                lat: lat,
                lng: lng,
                markers: [ {lat: lat, lng: lng} ],
                zoom: 17,
                disableDefaultUI: true
            });
        },
        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){
                _this.renderMap(1,1);
            });
            return _this;
        },

        events: function(){
          return _.extend({},BaseView.prototype.events,{
          });
        }

    });

    // Returns the View class
    return View;

} );