
define(["backbone", 
        "views/BaseSubview",
        "models/UserSession",
        "language",

        "models/InvitationModel",

        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseSubview,
        UserSession,
        Language,

        InvitationModel,
        $ ) {


    var View = BaseSubview.extend( {
        initialize: function(config) {
            BaseSubview.prototype.initialize.apply(this);
            this.config = config;
            this.class = this.config.name;
            this.model = config.model;
        },

        reject: function(){
            var _this = this;
            BaseSubview.prototype.alert.call(this, Language.view[_this.config.name].dialogs.failsafeReject)
            .$primary_btn.on("click", function(){
                _this.model.rejectpartner({
                    200: function(jd){_this.form.rejectTriggered(jd, _this)},
                });
            });
            
        },
        accept: function(){
            var _this = this;
            BaseSubview.prototype.alert.call(this, Language.view[_this.config.name].dialogs.failsafeAccept)
            .$primary_btn.on("click", function(){
                _this.model.acceptpartner({
                    200: function(jd){_this.form.acceptTriggered(jd, _this)}
                });
            });
        },

       form: {
            rejectTriggered: function(jsondata, _this) { 
                UserSession.fetch({
                    success: function(){
                        $(_this.el).remove();
                        console.log($("#invitations").find(".item").length);
                        if( ! $("#invitations").find(".item").length )
                            window.changePage("#invite", "left");
                    }
                });

            },
            acceptTriggered: function(jsondata, _this) { 
                UserSession.fetch({
                    success: function(){
                        window.changePage("#dashboard", "left");
                    }
                });
            },
        },

        events: function(){
          return _.extend({},BaseSubview.prototype.events,{
            "click .accept-partner" : "accept",
            "click .reject-partner" : "reject"
          });
        },

        render: function(){
            BaseSubview.prototype.render.call(this, function(){});
            return this;
        }

    });

    // Returns the View class
    return View;

} );