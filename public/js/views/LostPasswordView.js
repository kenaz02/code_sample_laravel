
define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",
        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        $ ) {


    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.call(this);
            this.config = config;
            this.id = this.config.name;
        },

        form: {
            request: function(jsondata, _this)
            {
                UserSession[_this.config.name](jsondata, 
                {
                    400: function(jd){_this.form.notRegistered(jd, _this)},
                    201: function(jd){_this.form.success(jd, _this)},
                });
            },
            notRegistered: function(jsondata, _this) { 
                BaseView.prototype.alert.call(this, Language.view[_this.config.name].dialogs.notRegistered);
            },
            success: function(jsondata, _this) { 
                window.changePage("#activate", "left");
            },
        },

        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){
                BaseView.prototype.form.setup.call(_this, {
                    "submit_selector" : "#submit",
                    "validation" : _this.config.name
                }); 
            });
            return _this;
        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{

          });
        }

    });

    // Returns the View class
    return View;

} );