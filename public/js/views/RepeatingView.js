
define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",
        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        $ ) {


    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
        },
        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){});
            return _this;
        },
        reminderChanged: function(e){
            console.log($(e.target).val());
            if( $(e.target).val() != "0" ){
                this.$el.find("[name=reminder-secondary]").parents(".selectbox").removeClass("hide");
            }
            else
            {
                 this.$el.find("[name=reminder-secondary]").parents(".selectbox").addClass("hide");
            }
        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{
            "change [name=reminder-first]" : "reminderChanged"
          });
        }

    });

    // Returns the View class
    return View;

} );