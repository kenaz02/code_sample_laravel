
define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",
        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        $ ) {


    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
        },

        form: {
            request: function(jsondata, _this)
            {
                UserSession.invite(jsondata, 
                {
                    400: function(jd){_this.form.alreadyConnected(jd, _this)},
                    201: function(jd){_this.form.success(jd, _this)},
                });
            },
            alreadyConnected: function(jsondata, _this) { 
                BaseView.prototype.alert.call(this, Language.view[_this.config.name].dialogs.alreadyConnected);
            },

            success: function(jsondata, _this) { 
                var alertDialog = BaseView.prototype.alert.call(this, Language.view[_this.config.name].dialogs.success);
                alertDialog.$primary_btn.on("click", function(){
                    UserSession.fetch({success:function(){window.changePage("#pending", "left");}});
                    
                });
            }

         },

        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){
                BaseView.prototype.form.setup.call(_this, {
                    "submit_selector" : "#submit",
                    "validation" : _this.config.name
                }); 
            });
            return _this;
        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{

          });
        },
        sync: function(){
            if(UserSession.get("invitations") != "0")
                window.changePage("#invitations", "left");
        }

    });

    // Returns the View class
    return View;

} );