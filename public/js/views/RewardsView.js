
define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",

        "views/RewardSubview",
        "models/RewardModel",
        "views/DashboardHelloSubview",
        "jquery", 
        "jqueryvalidate",

        ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        Subview,
        Model,
        HelloSubview,

        $ ) {


    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
            this.model = UserSession;
            this.loadFooter(this.id);
        },

        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){

                _this.renderFeed();
                
            });
            return _this;
        },
        renderFeed: function(){
            var _this = this;
            _this.$el.find(".feed .partners").html("");
            _this.$el.find(".feed .users").html("");
            var count = {user:0, partner:0};
            
            RewardUser = new Array();
            RewardPartner = new Array();

            for(var index in UserSession.get("rewards")) {

                var data = UserSession.get("rewards")[index];
                var model = new Model(data);


                if( UserSession.get("user") != UserSession.get("id") ){
                //if( model.get("user") != UserSession.get("id") ){
                    //User
                    RewardUser.push(UserSession.get("rewards")[index]);
                }
                else{
                    //Partner
                    RewardPartner.push(UserSession.get("rewards")[index]);
                }
            }

/*
            RewardUser.sort(function (a, b) {
                return a.received - b.received || a.date - b.date;
            });
            RewardPartner.sort(function (a, b) {
                return a.received - b.received || a.date - b.date;
            });
*/
            //Sort by status, date
            RewardUser.sort(function (a, b) {
                c= a.received;
                d= b.received;
                if (c==2)
                    c=1;
                if (d==2)
                    d=1;

                if (c < d)
                {
                    return -1;
                }
                else if (c > d)
                {
                    return 1;
                }
                else
                {
                    if (a.date > b.date)
                    {
                        return -1;
                    }
                    else if (a.date < b.date)
                    {
                        return 1;
                    }
                    return 0;
                }
            });
            //Sort by status, date
            RewardPartner.sort(function (a, b) {
                c= a.received;
                d= b.received;
                if (c==2)
                    c=1;
                if (d==2)
                    d=1;
                if (c < d)
                {
                    return -1;
                }
                else if (c > d)
                {
                    return 1;
                }
                else
                {
                    if (a.date > b.date)
                    {
                        return -1;
                    }
                    else if (a.date < b.date)
                    {
                        return 1;
                    }
                    return 0;
                }
            });


            for(var index in RewardUser)
            {
                //console.log(RewardUser[index]);
                var data = RewardUser[index];
                var model = new Model(data);
                if( model.get("partner") == UserSession.get("id") ){
                    model.set("fullname", UserSession.get("fullname"));
                    model.set("avatar", UserSession.get("avatar"));
                    model.set("my_rewards", UserSession.get("id"));
                }else{
                    model.set("avatar", UserSession.get("partner").avatar);
                    model.set("fullname", UserSession.get("partner").fullname);
                    model.set("my_rewards", UserSession.get("id"));
                }

                var subview = new Subview({
                    "name" : "reward",
                    "model" : model
                });

                if( model.get("user") != UserSession.get("id") ){
                    count.user++;
                    _this.$el.find(".feed .users").append(subview.render().$el);
                }
                else{
                    count.partner++;
                    _this.$el.find(".feed .partners").append(subview.render().$el);
                }
            }

            for(var index in RewardPartner)
            {
                var data = RewardPartner[index];
                var model = new Model(data);
                if( model.get("partner") == UserSession.get("id") ){
                    model.set("fullname", UserSession.get("fullname"));
                    model.set("avatar", UserSession.get("avatar"));
                    model.set("my_rewards", UserSession.get("id"));
                }else{
                    model.set("avatar", UserSession.get("partner").avatar);
                    model.set("fullname", UserSession.get("partner").fullname);
                    model.set("my_rewards", UserSession.get("id"));
                }

                var subview = new Subview({
                    "name" : "reward",
                    "model" : model
                });

                if( model.get("user") != UserSession.get("id") ){
                    count.user++;
                    _this.$el.find(".feed .users").append(subview.render().$el);
                }
                else{
                    count.partner++;
                    _this.$el.find(".feed .partners").append(subview.render().$el);
                }
            }


/*
            for(var index in UserSession.get("rewards"))
            {
                var data = UserSession.get("rewards")[index];
                var model = new Model(data);

                //console.log(UserSession.get("events")[index].date);

                if( model.get("partner") == UserSession.get("id") ){
                    model.set("fullname", UserSession.get("fullname"));
                    model.set("avatar", UserSession.get("avatar"));
                    model.set("my_rewards", UserSession.get("id"));
                }else{
                    model.set("avatar", UserSession.get("partner").avatar);
                    model.set("fullname", UserSession.get("partner").fullname);
                    model.set("my_rewards", UserSession.get("id"));
                }


                var subview = new Subview({
                    "name" : "reward",
                    "model" : model
                });

                if( model.get("user") != UserSession.get("id") ){
                    count.user++;
                    _this.$el.find(".feed .users").append(subview.render().$el);
                }
                else{
                    count.partner++;
                    _this.$el.find(".feed .partners").append(subview.render().$el);
                }
            }
*/


            $(".header-footer .partner .bullet").html(count.partner);
            $(".header-footer .user .bullet").html(count.user);
            if( ! count.partner ){
                var subview = new HelloSubview({
                    "name" : "dashboard-hello"
                });
                _this.$el.find(".feed .partners").html(subview.render().$el);
            }
            if( ! count.user ){
                var subview = new HelloSubview({
                    "name" : "dashboard-hello"
                });
                _this.$el.find(".feed .users").html(subview.render().$el);
            }
        },
        $feed: 0,
        v: 0,
        showPartners: function(){
            var _this = this;
            _this.v = 1;
            $(_this.el).find(".header-footer a").removeClass("active");
            $(_this.el).find(".header-footer .partner").addClass("active");
            _this.$el.find(".feed .partners, .feed .users").removeClass("show");
            _this.$el.find(".feed .partners").addClass("show");
            _this.renderFeed();
        },
        showUsers: function(){
            var _this = this;
            _this.v = 0;
            $(_this.el).find(".header-footer a").removeClass("active");
            $(_this.el).find(".header-footer .user").addClass("active");
            _this.$el.find(".feed .partners, .feed .users").removeClass("show");
            _this.$el.find(".feed .users").addClass("show");
            _this.renderFeed();          

        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{
            "click .header-footer .partner": "showPartners",
            "click .header-footer .user": "showUsers"
          });
        },
        sync: function(_this){
        }

    });

    // Returns the View class
    return View;

} );


