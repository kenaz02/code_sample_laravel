
define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",
        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        $ ) {


    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
        },

        form: {
            request: function(jsondata, _this)
            {
                UserSession.setpwd(jsondata, 
                {
                    200: function(jd){_this.form.success(jd, _this)},
                });
            },
            success: function(jsondata, _this) { 
                UserSession.fetch({
                    success: function(){
                        window.changePage("#dashboard", "left");
                    }
                });
            }
        },

        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){
                BaseView.prototype.form.setup.call(_this, {
                    "submit_selector" : "#submit",
                    "validation" : _this.config.name
                }); 
            });
            return _this;
        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{

          });
        }

    });

    // Returns the View class
    return View;

} );