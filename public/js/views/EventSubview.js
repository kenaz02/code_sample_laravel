
define(["backbone", 
        "views/BaseSubview",
        "models/UserSession",
        "language",

        "models/EventModel",

        "views/dialogs/SubmitDialogView",
        "views/dialogs/SuggestDialogView",
        "views/dialogs/HeldupDialogView",        
        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseSubview,
        UserSession,
        Language,

        EventModel,

        SubmitDialogView,
        SuggestDialogView,
        HeldupDialogView,

        $ ) {

    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    };
    function tConvert (time) {
      // Check correct time format and split into components
      time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

      if (time.length > 1) { // If time format correct
        time = time.slice (1);  // Remove full string match value
        time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
      }
      return time.join (''); // return adjusted time or original string
    }

    var View = BaseSubview.extend( {
        initialize: function(config) {
            BaseSubview.prototype.initialize.apply(this);
            this.config = config;
            this.class = this.config.name;
            this.model = config.model;
            this.parent = config.parent;
            this.model.set("rewards", JSON.parse(this.model.get("rewards")) );
            this.model.set("partner", UserSession.get("partner") );
            this.deleted = 0;
        },

        triggeredFailsafe: function(btn){
            var _this = this;
            BaseSubview.prototype.alert.call(this, Language.view["dashboard"].dialogs["failsafe" + btn.capitalize()])
            .$primary_btn.on("click", function(){
                _this.model[btn]({
                    200: function(jd){_this.form.success(jd, _this)},
                });
            });
        },
        triggeredFailsafePost: function(btn){
            var _this = this;
            BaseSubview.prototype.alert.call(this, Language.view["dashboard"].dialogs["failsafe" + btn.capitalize()])
            .$primary_btn.on("click", function(){
                var data = _this.$el.find("form").serialize();
                _this.model[btn](data,{
                    200: function(jd){_this.form.success(jd, _this)}
                });
            });
        },
        triggeredExcuse: function(btn){
            var _this = this;
            var sdv = new SubmitDialogView(Language.view["dashboard"].dialogs["modal" + btn.capitalize()]);
            sdv.popup( "open" );
            sdv.$primary_btn.on("click", function(){
                var data = {message: sdv.$el.find("textarea").val()};
                _this.model[btn](decodeURIComponent($.param(data)),{
                    200: function(jd){_this.form.success(jd, _this)},
                });
            });
        },
        accept: function(e){
            e.stopPropagation();
            this.triggeredFailsafePost("accept");
        },
        delete: function(e){
            e.stopPropagation();
            var _this = this;
             _this.model["delete"]({
                200: function(jd){_this.form.success(jd, _this)},
            });
        },
        statistics: function(e){
            e.stopPropagation();
            var _this = this;
             _this.model["delete"]({
                200: function(jd){_this.form.success(jd, _this)},
            });
            window.changePage("#statistics", "left");
        },
        rewards: function(e){
            e.stopPropagation();
            var _this = this;
             _this.model["delete"]({
                200: function(jd){_this.form.success(jd, _this)},
            });
            window.changePage("#rewards", "left");
        },
        remove: function(e){
            e.stopPropagation();
            this.triggeredFailsafe("delete");
        },
        cancel: function(e){
            e.stopPropagation();
            this.triggeredFailsafe("cancel");
        },
        onTime: function(e){
            e.stopPropagation();
            this.triggeredFailsafe("onTime");
        },
        late: function(e){
            e.stopPropagation();
            this.triggeredFailsafe("late");
        },
        reject: function(e){
            e.stopPropagation();
            this.triggeredExcuse("reject");
        },
        reject2: function(e){
            e.stopPropagation();
            var _this = this;
            var sdv = new SubmitDialogView(Language.view["dashboard"].dialogs["modalDelete"]);
            sdv.popup( "open" );
            sdv.$primary_btn.on("click", function(){
                var data = {message: sdv.$el.find("textarea").val()};
                _this.model["reject"](decodeURIComponent($.param(data)),{
                    200: function(jd){_this.form.success(jd, _this)},
                });
            });
        },
        heldup: function(e){
            e.stopPropagation();
            var _this = this;
            var sdv = new HeldupDialogView(Language.view["dashboard"].dialogs["modalHeldup"]);
            sdv.popup( "open" );
            sdv.$primary_btn.on("click", function(){
                var data = {message: sdv.$el.find("select").val()};
                _this.model["heldup"](decodeURIComponent($.param(data)),{
                    200: function(jd){_this.form.success(jd, _this)},
                });
            });
        },
        suggest: function(){
            var _this = this;
            var sdv = new SuggestDialogView(Language.view["dashboard"].dialogs["modalSuggest"]);
            sdv.popup( "open" );
            sdv.$primary_btn.on("click", function(){
                var data = {reward1: sdv.$el.find("textarea").val()};
                _this.model.suggest(decodeURIComponent($.param(data)),{
                    200: function(jd){_this.form.success(jd, _this)},
                });
            });
        },
        form: {
            success: function(jsondata, _this) { 
                UserSession.fetch({
                    success: function(){
                        _this.parent.gid = _this.model.id;
                        _this.parent.renderFeed();
                    }
                });

            }
        },

        toggle: function(event){
            this.$el.find(".msgs").toggleClass("show");
            this.$el.find(".toggle-icon").toggleClass("ss-plus");
            this.$el.find(".toggle-icon").toggleClass("ss-hyphen");


            setTimeout(function(){
            var vpHeight = $('[data-role=content]').height();
            var scrollTop = $('[data-role=content]').scrollTop();
            var link = $(event.currentTarget);
            var position = link.position();
               $('[data-role=content]').animate({
                    scrollTop: (position.top + scrollTop - 94)
                }, 100);
            }, 100);
 

            if( this.model.get("repeating") > 0 ){
                this.$el.find(".item").not(".no-multiple").toggleClass("multiple");
            }
        },

        ignore: function(e){
            e.stopPropagation();
        },

        events: function(){
          return _.extend({},BaseSubview.prototype.events,{
            "click .accept-btn" : "accept",
            "click .reject-btn" : "reject",
            "click .reject2-btn" : "reject2",
            "click .delete-btn" : "delete",
            "click .remove-btn" : "remove",
            "click .ontime-btn" : "onTime",
            "click .cancel-btn" : "cancel",
            "click .heldup-btn" : "heldup",
            "click .suggest-btn" : "suggest",

            "click .rewards-btn" : "rewards",
            "click .statistics-btn" : "statistics",

            "click .item" : "toggle",
            "click .msgs" : "ignore",
            "click .late-btn" : "late"
          });
        },

        render: function(){
            var _this = this;
            BaseSubview.prototype.render.call(this, function(){
                if( _this.model.get("repeating") > 0 ){
                    _this.$el.find(".item").not(".no-multiple").addClass("multiple");
                }

                _this.$el.find('.item').attr("data-id", _this.model.get("id"));
            });
            return this;
        }

    });

    // Returns the View class
    return View;

} );