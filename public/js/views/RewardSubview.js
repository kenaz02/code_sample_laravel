
define(["backbone", 
        "views/BaseSubview",
        "models/UserSession",
        "language",

        "models/RewardModel",

        "jquery", 
        "jqueryvalidate" ], function( 
        Backbone,
        BaseSubview,
        UserSession,
        Language,

        RewardModel,
        $ ) {

    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    };


    var View = BaseSubview.extend( {
        initialize: function(config) {
            BaseSubview.prototype.initialize.apply(this);
            this.config = config;
            this.class = this.config.name;
            this.model = config.model;
            this.parent = config.parent;
        },

        received: function(e){
            e.stopPropagation();
            var _this = this;
                _this.model["received"](decodeURIComponent('1'),{
                    200: function(jd){
                    	UserSession["forceUpdate"](function(){
                    		Backbone.history.loadUrl();
                    	});
                    },
                });
        },
        not_received: function(e){
        	e.stopPropagation();
            var _this = this;
                _this.model["not_received"](decodeURIComponent('2'),{
                    200: function(jd){
                    	UserSession["forceUpdate"](function(){
                    		Backbone.history.loadUrl();
                    	});
                    },
                });
        },
        events: function(){
          return _.extend({},BaseSubview.prototype.events,{
              "click .not_received-btn" : "not_received",
              "click .received-btn" : "received",
          });
        },
        renderFeed: function(){
            var _this = this;

            for(var index in UserSession.get("rewards"))
            {
                var data = UserSession.get("rewards")[index];
                var model = new RewardModel(data);
                model.set("my_rewards", UserSession.get("id"));
            }
   
        },        
        render: function(){
            var _this = this;
            BaseSubview.prototype.render.call(_this, function(){
            	_this.renderFeed();
            });
            return this;
        }

    });

    // Returns the View class
    return View;

} );