define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",

        "models/EventModel",
        "views/EventSubview",
        "views/DashboardHelloSubview",

        "jquery", 
        "jqueryvalidate",

        ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        Model,
        Subview,
        DashboardHelloSubview,
        $ ) {

    // This script is released to the public domain and may be used, modified and
    // distributed without restrictions. Attribution not necessary but appreciated.
    // Source: http://weeknumber.net/how-to/javascript 

    // Returns the ISO week of the date.
    Date.prototype.getWeek = function() {
      var date = new Date(this.getTime());
       date.setHours(0, 0, 0, 0);
      // Thursday in current week decides the year.
      date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
      // January 4 is always in week 1.
      var week1 = new Date(date.getFullYear(), 0, 4);
      // Adjust to Thursday in week 1 and count number of weeks from date to week1.
      return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
                            - 3 + (week1.getDay() + 6) % 7) / 7);
    }

    // Returns the four-digit year corresponding to the ISO week of the date.
    Date.prototype.getWeekYear = function() {
      var date = new Date(this.getTime());
      date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
      return date.getFullYear();
    }

    function intToWord(i){
        switch(i){
            case 1:return "One"; break;
            case 2:return "Two"; break;
            case 3:return "Three"; break;
            case 4:return "Four"; break;
            case 5:return "Five"; break;
            case 6:return "Six"; break;
            case 7:return "Seven"; break;
            case 8:return "Eight"; break;
            case 9:return "Nine"; break;
            case 10:return "Ten"; break;
            case 11:return "Eleven"; break;
            default: return "Unknown"; break;
        }
    }
    function tConvert (time) {
      // Check correct time format and split into components
      time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

      if (time.length > 1) { // If time format correct
        time = time.slice (1);  // Remove full string match value
        time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
      }
      return time.join (''); // return adjusted time or original string
    }

    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
            this.loadFooter(this.id);
        },
        flagoption: {
            sent:{
                pending: 1,
                proposal: 2,
                idle: 4,
                past: 8,
                deleted: 16,
                heldup: 32
            },
            recv:{
                pending: 64,
                proposal: 128,
                idle: 256,
                changed: 512,
                deleted: 1024,
                ontime: 2048,
                late: 4096
            }
        },
        renderUserEvent: function(flag){
            if( flag & this.flagoption.sent.pending)
                return "event-pending-user";
            else if( flag & this.flagoption.sent.proposal)
                return "event-user-proposal";
            else if( flag & this.flagoption.sent.idle)
                return "event-idle-user";
            else if( flag & this.flagoption.sent.past)
                return "event-past";
            else if( flag & this.flagoption.sent.deleted)
                return "event-deleted-user";
           else if( flag & this.flagoption.sent.heldup)
                return "event-heldup";
        },
        renderPartnerEvent: function(flag){
            if( flag & this.flagoption.recv.pending)
                return "event-pending-partner";
            else if( flag & this.flagoption.recv.proposal)
                return "event-partner-proposal";
            else if( flag & this.flagoption.recv.idle)
                return "event-idle-partner";
            else if( flag & this.flagoption.recv.changed)
                return "event-changed";
            else if( flag & this.flagoption.recv.deleted)
                return "event-deleted-partner";
            else if( flag & this.flagoption.recv.ontime)
                return "event-ontime";
            else if( flag & this.flagoption.recv.late)
                return "event-late";
        },        
        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){_this.renderFeed()});        
            return _this;
        },
        renderFeed: function(){
                var _this = this;
                $(".header-footer .bullet").hide();
                _this.$feed = $(".feed");
                _this.$feed.find(".partners").html("");
                _this.$feed.find(".users").html("");
                var seed = {
                    UserEvents: [
                        {
                            created_at: "2014-02-02 01:13:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.sent.pending,
                            id: "1",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "18 Entwood Ave , Coolum Beach",
                            name: "Event Title",
                            recipient: "3",
                            ontime_count: "0",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["Black Forest Gateaxus","Message with happy ending","Dance Party Ya!"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09:19"                   
                        },
                        {
                            created_at: "2014-07-18 01:09:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.sent.proposal,
                            id: "2",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09:19"                   
                        },
                        {
                            created_at: "2014-02-25 01:09:19",
                            date: "2014-02-25",
                            flag: _this.flagoption.sent.idle,
                            id: "3",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-03-02 01:09:19"                   
                        },
                        {
                            created_at: "2014-01-18 01:09:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.sent.idle,
                            id: "4",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09:19"                   
                        },
                        {
                            created_at: "2014-01-18 01:09:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.sent.past | 8192,
                            id: "5",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "1",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            message: "Sorry I am late",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09:19"                   
                        },
                        {
                            created_at: "2014-01-18 01:09:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.sent.deleted,
                            id: "6",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "0",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            message: "Screw you guys I am going home.",
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09:19"                   
                        },
                        {
                            created_at: "2014-01-18 01:09:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.sent.heldup,
                            id: "7",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "0",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            message: "I don't wanna come any more :(",
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09:19"                   
                        },




                        {
                            created_at: "2014-02-02 01:13:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.sent.pending,
                            id: "1",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "18 Entwood Ave , Coolum Beach",
                            name: "Event Title",
                            recipient: "3",
                            ontime_count: "0",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["Black Forest Gateaxus","Message with happy ending","Dance Party Ya!"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09:19"                   
                        },
                        {
                            created_at: "2014-07-18 01:09:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.sent.proposal,
                            id: "2",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09:19"                   
                        },
                        {
                            created_at: "2014-02-25 01:09:19",
                            date: "2014-02-25",
                            flag: _this.flagoption.sent.idle,
                            id: "3",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-03-02 01:09:19"                   
                        },
                        {
                            created_at: "2014-01-18 01:09:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.sent.idle,
                            id: "4",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09:19"                   
                        },
                        {
                            created_at: "2014-01-18 01:09:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.sent.past | 8192,
                            id: "5",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "1",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            message: "Sorry I am late",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09:19"                   
                        },
                        {
                            created_at: "2014-01-18 01:09:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.sent.deleted,
                            id: "6",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "0",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            message: "Screw you guys I am going home.",
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09:19"                   
                        },
                        {
                            created_at: "2014-01-18 01:09:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.sent.heldup,
                            id: "7",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "0",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            message: "I don't wanna come any more :(",
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09:19"                   
                        },


                    ],
                    PartnerEvents: [
                        {
                            created_at: "2014-05-18 01:09:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.pending,
                            id: "5",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"                   
                        },
                        {
                            created_at: "2014-01-18 01:09",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.proposal,
                            id: "6",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"
                        },
                        {
                            created_at: "2014-01-18 01:09",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.idle,
                            id: "7",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"                   
                        },
                        {
                            created_at: "2014-01-18 01:09",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.changed | 8192 | 16384,
                            id: "8",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"
                        },
                        {
                            created_at: "2014-01-18 01:09",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.deleted,
                            id: "49",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"                   
                        },
                        {
                            created_at: "2014-01-18 01:09",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.late,
                            id: "49",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"                   
                        },
                        {
                            created_at: "2014-01-18 01:09",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.ontime,
                            id: "49",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "2",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"                   
                        },





                       {
                            created_at: "2014-05-18 01:09:19",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.pending,
                            id: "5",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"                   
                        },
                        {
                            created_at: "2014-01-18 01:09",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.proposal,
                            id: "6",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"
                        },
                        {
                            created_at: "2014-01-18 01:09",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.idle,
                            id: "7",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"                   
                        },
                        {
                            created_at: "2014-01-18 01:09",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.changed | 8192 | 16384,
                            id: "8",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"
                        },
                        {
                            created_at: "2014-01-18 01:09",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.deleted,
                            id: "49",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"                   
                        },
                        {
                            created_at: "2014-01-18 01:09",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.late,
                            id: "49",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"                   
                        },
                        {
                            created_at: "2014-01-18 01:09",
                            date: "2014-04-06",
                            flag: _this.flagoption.recv.ontime,
                            id: "49",
                            lat: "50.9794044",
                            lng: "-114.52134710000001",
                            location: "asdfghjkl;, Redwood Meadows, AB, Canada",
                            name: "aaaaa",
                            recipient: "3",
                            ontime_count: "2",
                            repeating: "0",
                            reward: "Blowjob on the Beach",
                            rewards: '["a","b","c"]',
                            sender: "1",
                            time: "14:00",
                            updated_at: "2014-01-18 01:09"                   
                        }


                    ]
                };

                /*
                var seed = {
                    UserEvents: [],
                    PartnerEvents: []
                };

                var mycount = 0;
                for(var i in UserSession.get("events")){
                    if( UserSession.get("events")[i].recipient == UserSession.get("id") ){
                        seed.PartnerEvents.push(UserSession.get("events")[i]);
                    }else{
                        seed.UserEvents.push(UserSession.get("events")[i]);
                        mycount++;
                    }
                }

                if( !  mycount ){
                    var ds = new DashboardHelloSubview({
                        name: "dashboard-hello"
                    });

                    _this.$feed.find(".users").html(ds.render().$el);
                }*/

                seed.UserEvents.sort(function(a,b){
                    return (b.date < a.date) ? 1 : (b.date > a.date) ? -1 : 0;;
                });

                seed.PartnerEvents.sort(function(a,b){
                    return (b.date < a.date) ? 1 : (b.date > a.date) ? -1 : 0;;
                });


                var date_stack = {};

                var count = {past: 0, proposal: 0, deleted: 0, heldup: 0};
                for(index in seed.UserEvents)
                {
                    var data = seed.UserEvents[index];
                    var model = new Model(data);
                    var config = {"model": model, parent: _this};
                    var flag = model.get("flag"); //real deal
                    if(flag & _this.flagoption.recv.deleted ||
                       flag & _this.flagoption.recv.ontime  ||
                       flag & _this.flagoption.recv.late ) continue;

                    model.set("dates", 0);
                    if( model.get("repeating") > 0 ){
                        var dates = [];
                        var x = parseInt( model.get("repeating") ) + 1;
                        for(var i = 0; i < x; i++ ){
                            var arr = (model.get("date")+" "+model.get("time") + ":00" ).split(/[- :]/);
                            var xdate = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                            xdate.setHours(0,0,0,0);
                            xdate.setDate(xdate.getDate() + (i*7));
                            var days = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
                            var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                            "Aug", "Sep", "Oct", "Nov", "Dec" ];
                            var month = months[xdate.getMonth()];     
                            var date = xdate.getDate() + " " + month + " " + xdate.getFullYear() + ", " + tConvert(model.get("time"));       
                            dates.push(date);
                        }
                        model.set("dates", dates);
                    }
                    var now_only_date = new Date();
                    now_only_date.setHours(0,0,0,0);
                    var now = new Date();
                    var arr = ( model.get("date")+" "+model.get("time") + ":00" ).split(/[- :]/);
                    var xdate = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);


                    var mdate = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                    mdate.setHours(0,0,0,0);
                    mdate.setDate(mdate.getDate()-7);
                    console.log((model.get("date")+" "+model.get("time") + ":00"  ) + "\n" + mdate + "\nnow:" + now )
                    if( mdate <= now_only_date ){
                        model.set("upcoming", 1);
                    } else model.set("upcoming", 0);

                    var days = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
                    var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                    "Aug", "Sep", "Oct", "Nov", "Dec" ];
                    var month = months[xdate.getMonth()];
                      
 


                    var date = xdate.getDate() + " " + month + " " + xdate.getFullYear() + ", " + tConvert(model.get("time"));               
                    if( xdate <= now ){
                        flag = 0 | _this.flagoption.sent.past | _this.flagoption.recv.idle;
                    }
                    model.set("date", date);

                    config["name"] = _this.renderUserEvent(flag);
                    var subview = new Subview(config);
                    var sel = subview.render();

                    if( flag & _this.flagoption.sent.pending){
                        count.heldup++;
                    }
                    else if( flag & _this.flagoption.sent.heldup){

                    }
                    else if( flag & _this.flagoption.sent.deleted){
                        count.deleted++;
                    }
                    else if( flag & _this.flagoption.sent.proposal){
                        count.proposal++;
                    }
                    else if( flag & _this.flagoption.sent.idle){
                    }
                        
                    else if( flag & _this.flagoption.sent.past){
                        count.past++;
                    }


 
                    date_stack[date] = $("<div/>").addClass("datetime");

                    var total_count = 0;
                    for(var i in count){
                        total_count += count[i];
                    }
                    if( total_count)
                    $(".header-footer .bullet.user").html(total_count).show();

                    date_stack[date].append(sel.$el);
                    _this.$feed.find(".partner").append(date_stack[date]);
                }

                 date_stack = {};
                 var countp = {changed: 0, proposal: 0, deleted: 0, ontime: 0, late: 0};
                 for(index in seed.PartnerEvents)
                 {
                    var data = seed.PartnerEvents[index];
                    var model = new Model(data);
                    var config = {"model": model, parent: _this};
                    var flag = model.get("flag"); //real deal
                    if(flag & _this.flagoption.sent.deleted) continue;
                    model.set("dates", 0);
                    if( model.get("repeating") > 0 ){
                        var dates = [];
                        var x = parseInt( model.get("repeating") ) + 1;
                        for(var i = 0; i < x; i++ ){
                            var arr = (model.get("date")+" "+model.get("time") + ":00" ).split(/[- :]/);
                            var xdate = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                            xdate.setHours(0,0,0,0);
                            xdate.setDate(xdate.getDate() + (i*7));
                            var days = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
                            var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                            "Aug", "Sep", "Oct", "Nov", "Dec" ];
                            var month = months[xdate.getMonth()];     
                            var date = xdate.getDate() + " " + month + " " + xdate.getFullYear() + ", " + tConvert(model.get("time"));       
                            dates.push(date);
                        }
                        model.set("dates", dates);
                    }

                    var now = new Date();
                    var arr = ( model.get("date")+" "+model.get("time") + ":00" ).split(/[- :]/);
                    var xdate = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
                    xdate.setHours(0,0,0,0);



                    var days = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
                    var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                    "Aug", "Sep", "Oct", "Nov", "Dec" ];
                    var month = months[xdate.getMonth()];
        

                    var date = xdate.getDate() + " " + month + " " + xdate.getFullYear() + ", " + tConvert(model.get("time"));               
                    if( !(flag & _this.flagoption.recv.late) || !(flag & _this.flagoption.recv.ontime) )
                    if( xdate <= now ){
                        flag = 0 | _this.flagoption.sent.past | _this.flagoption.recv.idle;
                    }
                    model.set("date", date);


                    config["name"] = _this.renderPartnerEvent(flag);
                    var subview = new Subview(config); 
                    var sel = subview.render();
 
                    if( flag & _this.flagoption.recv.pending){

                    }
                    else if( flag & _this.flagoption.recv.deleted){
                        countp.deleted++;
                    }
                    else if( flag & _this.flagoption.recv.proposal){
                        countp.proposal++;
                    }
                    else if( flag & _this.flagoption.recv.idle){
                    }
                        
                    else if( flag & _this.flagoption.recv.past){
                        countp.past++;
                    }
                    else if( flag & _this.flagoption.recv.ontime){
                        countp.ontime++;
                    }
                    else if( flag & _this.flagoption.recv.late){
                        countp.late++;
                    }

 
                    date_stack[date] = $("<div/>").addClass("datetime");

                    console.log(countp);
                    var total_countp = 0;
                    for(var i in countp){
                        total_countp += countp[i];
                    }
                    if( total_countp)
                    $(".header-footer .bullet.user").html(total_countp).show();

                    date_stack[date].append(sel.$el);

                    _this.$feed.find(".partners").append(date_stack[date]);
                }
        },
        $feed: 0,
        v: 0,
        showPartners: function(){
            var _this = this;
                _this.v = 1;
                $(_this.el).find(".header-footer a").removeClass("active");
                $(_this.el).find(".header-footer .partner").addClass("active");
                _this.$el.find(".feed .partners, .feed .users").removeClass("show");
                _this.$el.find(".feed .partners").addClass("show");
                _this.renderFeed();
        },
        showUsers: function(){
            var _this = this;
            _this.v = 0;
            $(_this.el).find(".header-footer a").removeClass("active");
            $(_this.el).find(".header-footer .user").addClass("active");
            _this.$el.find(".feed .partners, .feed .users").removeClass("show");
            _this.$el.find(".feed .users").addClass("show");
            _this.renderFeed();          

        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{
            "click .header-footer .partner": "showPartners",
            "click .header-footer .user": "showUsers"
          });
        },
        sync: function(_this){
            if( typeof UserSession.changed.events != 'undefined' ){
                this.renderFeed();
            }
        }

    });

    // Returns the View class
    return View;

} );





