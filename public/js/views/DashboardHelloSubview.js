
define(["backbone", 
        "views/BaseSubview",
        "models/UserSession",
        "language",

        "models/EventModel",

        "jquery", 
        "jqueryvalidate" ], function( 
        Backbone,
        BaseSubview,
        UserSession,
        Language,

        $ ) {

    var View = BaseSubview.extend( {
        initialize: function(config) {
            BaseSubview.prototype.initialize.apply(this);
            this.config = config;
            this.class = this.config.name;
            this.parent = config.parent;
        },
        events: function(){
          return _.extend({},BaseSubview.prototype.events,{
          });
        },

        render: function(){
            BaseSubview.prototype.render.call(this, function(){});
            return this;
        }

    });

    // Returns the View class
    return View;

} );