
define(["backbone",
        "config",
        "views/BaseView",
        "models/UserSession",
        "models/EventModel",
        "language",
        "gmaps",
        "jquery",
        "jqueryvalidate", ], function( 
        Backbone,
        Config,
        BaseView,
        UserSession,
        Model,
        Language,
        GMaps,
        $ ) {


    var View = BaseView.extend({
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;

            var now = new Date();
            var month = ("0" + (now.getMonth() + 1)).slice(-2);
            var date = ("0" + (now.getDate())).slice(-2);
            var hours = ("0" + (now.getHours() + 1)).slice(-2);
            var minutes = ("0" + (now.getMinutes()-1)).slice(-2);

            for( var index in UserSession.get("events") ){
                if( parseInt(UserSession.get("events")[index].id) == parseInt(this.config.id) ){
                    this.model = new Model(_.extend(UserSession.get("events")[index],{
                        mindate: now.getFullYear() + "-" + month + "-" + date,
                        mintime: hours + ":" + minutes + ":00"
                    } ));
                    break;
                }
            }
        },
        flagoption: {
            sent:{
                pending: 1,
                proposal: 2,
                idle: 4,
                past: 8,
                deleted: 16,
                heldup: 32
            },
            recv:{
                pending: 64,
                proposal: 128,
                idle: 256,
                changed: 512,
                deleted: 1024,
                ontime: 2048,
                late: 4096
            }
        },
        form: {
            request: function(jsondata, _this)
            {
                _this.model.update(jsondata, 
                {
                    200: function(jd){_this.form.success(jd, _this)},
                });
            },
            success: function(jsondata, _this) { 
                UserSession.fetch({
                    success: function(){
                        window.changePage("#dashboard", "left", function(){
                            $(".header-footer .user").click();
                            setTimeout(function(){
                                $(".item[data-id='"+jsondata.id+"']").click();
                            }, 200)
                        });

                        console.log('set notification after edit');


                        /********************
                         Update reminder
                         ********************/

                            //Set reminder1 and reminder2 and message1 and message2
                        rem1=UserSession.get("reminder1");
                        rem2=UserSession.get("reminder2");

                        if (rem1 == 1){min1=0;msg1 = Language.view["reminders"].notifications.time_event;}
                        if (rem1 == 2){min1=5;msg1 = Language.view["reminders"].notifications.mins5;}
                        if (rem1 == 3){min1=15;msg1 = Language.view["reminders"].notifications.mins15;}
                        if (rem1 == 4){min1=30;msg1 = Language.view["reminders"].notifications.mins30;}
                        if (rem1 == 5){min1=60;msg1 = Language.view["reminders"].notifications.hours1;}
                        if (rem1 == 6){min1=120;msg1 = Language.view["reminders"].notifications.hours2;}
                        if (rem1 == 7){min1=60 * 24;msg1 = Language.view["reminders"].notifications.days1;}
                        if (rem1 == 8){min1=60 * 24 * 2;msg1 = Language.view["reminders"].notifications.days2;}
                        if (rem1 == 9){min1=60 * 24 * 7;msg1 = Language.view["reminders"].notifications.days7;}

                        if (rem2 == 1){min2=0;msg2 = Language.view["reminders"].notifications.time_event;}
                        if (rem2 == 2){min2=5;msg2 = Language.view["reminders"].notifications.mins5;}
                        if (rem2 == 3){min2=15;msg2 = Language.view["reminders"].notifications.mins15;}
                        if (rem2 == 4){min2=30;msg2 = Language.view["reminders"].notifications.mins30;}
                        if (rem2 == 5){min2=60;msg2 = Language.view["reminders"].notifications.hours1;}
                        if (rem2 == 6){min2=120;msg2 = Language.view["reminders"].notifications.hours2;}
                        if (rem2 == 7){min2=60 * 24;msg2 = Language.view["reminders"].notifications.days1;}
                        if (rem2 == 8){min2=60 * 24 * 2;msg2 = Language.view["reminders"].notifications.days2;}
                        if (rem2 == 9){min2=60 * 24 * 7;msg2 = Language.view["reminders"].notifications.days7;}

                        /*window.plugin.notification.local.getScheduledIds(function(scheduledIds) {
                         console.log('1 - Get schedules notification');
                         for (var i = 0; scheduledIds.length > i; i++) {
                         //window.plugin.notification.local.cancel(scheduledIds[i]);
                         console.log(scheduledIds[i]);
                         }
                         });*/

                        window.setTimeout(function(){
                            //Cancel all local notification
                            window.plugin.notification.local.cancelAll(function () {
                                //At this point all notifications have been canceled
                                //console.log('2 - Cancel all notification');
                            });
                        }, 1000);

                        /*window.setTimeout(function(){
                         window.plugin.notification.local.getScheduledIds(function(scheduledIds) {
                         console.log('3 - Get schedules notification');
                         for (var i = 0; scheduledIds.length > i; i++) {
                         console.log(scheduledIds[i]);
                         }
                         });
                         }, 2000);*/

                        window.setTimeout(function(){

                            console.log('4 - Reset all notification');

                            //Set new notification
                            var idNotification = 0;
                            var now = new Date;
                            for(var i in UserSession.get("events")) {
                                //Don't send notification for events deleted
                                console.log(UserSession.get("events")[i].name);
                                if (UserSession.get("events")[i].flag != 1028){
                                    //new Date(year, month, day [, hour, minute, second, millisecond ])
                                    var arr = ( UserSession.get("events")[i].date + " " + UserSession.get("events")[i].time + ":00" ).split(/[- :]/);
                                    var reminderEvent = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                    var reminderDate1 = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                    var reminderDate2 = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                    msg1name = msg1 + "\n'" + UserSession.get("events")[i].name+"'";
                                    msg2name = msg2 + "\n'" + UserSession.get("events")[i].name+"'";
                                    //Send notification only for future events
                                    if(reminderEvent > now) {
                                        if (rem1 != 0) {
                                            reminderDate1.setMinutes(reminderEvent.getMinutes() - min1);
                                            if(reminderDate1 > now){
                                                console.log('Set not 1, time '+reminderDate1);
                                                window.plugin.notification.local.add({
                                                    id:         idNotification,  // A unique id of the notifiction
                                                    date:       reminderDate1,    // This expects a date object
                                                    message:    msg1name,  // The message that is displayed
                                                    //title:      'Held up',  // The title of the message
                                                    badge:      1,  // Displays number badge to notification
                                                    //autoCancel: Boolean, // Setting this flag and the notification is automatically canceled when the user clicks it
                                                });
                                                idNotification = idNotification + 1;
                                            }
                                        }
                                        if (rem2 != 0) {
                                            reminderDate2.setMinutes(reminderEvent.getMinutes() - min2);
                                            if(reminderDate2 > now) {
                                                console.log('Set not 2, time '+reminderDate2);
                                                window.plugin.notification.local.add({
                                                    id:         idNotification,  // A unique id of the notifiction
                                                    date:       reminderDate2,    // This expects a date object
                                                    message:    msg2name,  // The message that is displayed
                                                    //title:      'Held up',  // The title of the message
                                                    badge:      1,  // Displays number badge to notification
                                                    //autoCancel: Boolean, // Setting this flag and the notification is automatically canceled when the user clicks it
                                                });
                                                idNotification = idNotification + 1;
                                            }
                                        }
                                    }
                                }
                            }

                        }, 3000);

                        /*window.setTimeout(function(){
                         window.plugin.notification.local.getScheduledIds(function(scheduledIds) {
                         console.log('5 - Get schedules notification');
                         for (var i = 0; scheduledIds.length > i; i++) {
                         console.log(scheduledIds[i]);
                         }
                         });
                         }, 4000);
                         */


                    }
                });
                
            },
        },
        
        renderMap: function(lat, lng, marker){
            var size = $( window ).width() * 2;
            var url = GMaps.staticMapURL({
              size: [size, size],
              lat: lat,
              lng: lng,
              markers: [ {lat: lat, lng: lng} ],
              zoom: 17
            });
            $("input#lat").val(lat);
            $("input#lng").val(lng);
            $("#map").html("<img class='mapview'/>");
            $("#map").append("<img class='pinx' src='images/gmaps/marker.png'/>");
            $("#map img.mapview").attr("src", url);
            $("#map img.mapview").one("load", function(){
                $("#map").css({visibility: "visible"});
            });
            var _this = this;

        },

        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){
                BaseView.prototype.form.setup.call(_this, {
                    "submit_selector" : "#submit",
                    "validation" : _this.config.name
                }); 
                //_this.renderMap(1, 1);
                var duplicate = false;
                setTimeout(function(){
                $(".dummy").on("click", function(){
                    $(this).find("input[type=text]").parent().remove();
                    var $datetime = $(this).find("input[type=date],input[type=time]");
                    $datetime.css({visibility: "visible"});
                    $datetime.focus().select();
                });
                $(".dummy input[type=text]").on("click focus change", function(){
                    var $datetime = $(this).parents(".dummy")
                    .find("input[type=date],input[type=time]");
                    $datetime.css({visibility: "visible"});
                    $datetime.focus().select();
                    $(this).parent().remove();
                });
                $("input[name=date]").blur(function(){
                    if( $(this).val() == $(this).attr("min") ){
                        //$("input[name=time]").attr("min", _this.model.get("mintime"));
                        if( $("[name=time]").val() != "" )
                        $("[name=time]").valid();
                    }
                    else{
                         $("input[name=time]").removeAttr("min");
                         if( $("[name=time]").val() != "" )
                         $("[name=time]").valid();
                    }
                });
                $("#date, #time").on("change blur keyup", function(){

                    thisDate = $("#date").val();
                    thisTime = $("#time").val();

                    duplicate = false;
                    var BreakException= {};

                    thisEvents = UserSession.get("events");

                    try{
                        thisEvents.forEach(function(entry){
                            if(entry.flag == (_this.flagoption.sent.pending+_this.flagoption.recv.proposal) ||
                                entry.flag == (_this.flagoption.sent.proposal+_this.flagoption.recv.pending) ||
                                entry.flag == (_this.flagoption.sent.idle+_this.flagoption.recv.idle) ||
                                entry.flag == (_this.flagoption.sent.heldup+_this.flagoption.recv.idle)){

                                //The events in not deleted or past
                                if(entry.date == thisDate && entry.time == thisTime){
                                    var year = parseInt(entry.date.substring(0,4));
                                    var month = parseInt(entry.date.substring(5,7));
                                    var day = parseInt(entry.date.substring(8,10));
                                    var hours = parseInt(entry.time.substring(0,2));
                                    var mins = parseInt(entry.time.substring(3,5));

                                    var tempDate = new Date(year, month-1, day, hours, mins, 0, 0);
                                    tempDate = tempDate.toLocaleTimeString(navigator.language, {day: '2-digit', month: '2-digit', year: 'numeric', hour: '2-digit', minute:'2-digit'});
                                    tempDate = tempDate.toLocaleString();
                                    tempDate = tempDate.toLocaleLowerCase();
                                    tempDate = tempDate.replace(',', '');

                                    $('#time-error').html( '<p><b><font color="red">' + tempDate + ' ' + ' is in use</font></b></p>' );
                                    $("#time").addClass("error");
                                    $("#time").attr("data-invalid", "true");
                                    $("#date").addClass("error");
                                    $("#date").attr("data-invalid", "true");
                                    duplicate=true;
                                    throw BreakException;
                                }
                            }
                        });
                    }
                    catch(e){
                        if (e!==BreakException) throw e;
                    }

                    if (duplicate==false){
                        $('#time-error').html( '' );
                        $("#time").attr("data-invalid", "");
                        $("#time").removeClass("error");
                        $("#date").removeClass("error");
                        $("#date").attr("data-invalid", "");
                    }

                });
                }, 100);

                _this.setSearchLocation();
            });
            return _this;
        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{
                "keyup #location" : "search",
                "click #location" : "search",
                "click ul.results a": "selectSearchSuggestion",
                "blur #location": "selectFirstSearchSuggestion"
          });
        },
        selectSearchSuggestion: function(ev){
            $("#location").data("stop", 1);
            $("#location").val($(ev.target).html());
            this.setSearchLocation();
        },
        selectFirstSearchSuggestion: function(ev){
            var _this = this;
            setTimeout(function(){
                if( $("#location").data("stop") ){
                    $("#location").data("stop", 0);
                    return;
                }
                if( $("#location").val().length > 0 ){
                    $("#location").val(
                        $("ul.results").find("a:eq(0)").html()
                    );
                    _this.setSearchLocation();
                }
            }, 100);

        },
        setSearchLocation: function(){
            var _this = this;
            $("ul.results").hide();
            GMaps.geocode({
              address: $('#location').val(),
              callback: function(results, status) {
                if (status == 'OK') {
                    var latlng = results[0].geometry.location;
                    _this.renderMap(latlng.lat(), latlng.lng());
                }
              }
            });
        },
        search: function(){
            var query = $("#location").val();

            if( query.length > 0 ){
                var service = new google.maps.places.AutocompleteService();
                service.getQueryPredictions({ input: query }, function(predictions, status){
                    
                    $("ul.results").html("");
                    for (var i = 0, prediction; prediction = predictions[i]; i++) {
                        $("ul.results").append("<li><a>" + prediction.description + "</a></li>");
                        if( i == 2 ) break;
                    }

                    $("ul.results").show();
                });
            }else{
                $("ul.results").hide();
            }

        }

    });

    // Returns the View class
    return View;

} );