
define(["backbone", 
        "config",
        "views/BaseView",
        "models/UserSession",
        "models/EventModel",
        "language",
        "gmaps",
        "jquery",
        "jqueryvalidate", ], function( 
        Backbone,
        Config,
        BaseView,
        UserSession,
        Model,
        Language,
        GMaps,
        $ ) {


    var View = BaseView.extend({
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
            for( var index in UserSession.get("events") ){
                if( parseInt(UserSession.get("events")[index].id) == parseInt(this.config.id) ){
                    this.model = new Model(UserSession.get("events")[index]);
                    break;
                }
            }
        },

        form: {
            request: function(jsondata, _this)
            {
                _this.model[_this.config.name](jsondata, 
                {
                    201: function(jd){_this.form.success(jd, _this)},
                });
            },
            success: function(jsondata, _this) { 
            },
        },
        
        
        renderMap: function(_this,lat, lng){
              	        	
        	navigator.geolocation.getCurrentPosition(function(position) {
                
            	var markersArray = [];
        		var origin1 = new google.maps.LatLng(lat, lng);
            	var destinationA = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            	var options = {
            		center : new google.maps.LatLng(lat, lng),
            		zoom : 17,
            		visible: true,
                    disableDefaultUI: true
            	};            	    
            	var div = document.getElementById('mapcontent');    
            	var map = new google.maps.Map(div, options);

      		  	//Calculate distance
        		var service = new google.maps.DistanceMatrixService();
        		service.getDistanceMatrix(
      		    {
      		      origins: [origin1],
      		      destinations: [destinationA],
      		      travelMode: google.maps.TravelMode.DRIVING,
      		      //unitSystem: google.maps.UnitSystem.IMPERIAL,
      		      unitSystem: google.maps.UnitSystem.METRIC,
      		      avoidHighways: false,
      		      avoidTolls: false
      		    }, callback);
      		  	
        		function callback(response, status) {
        			if (status != google.maps.DistanceMatrixStatus.OK) {
        			    alert('Error was: ' + status);
        			}
        			else {
        			    deleteOverlays();
        			    var results;
        			    var distance;
        			    var duration;
        			    console.log(response);
        			    console.log(response.rows[0].elements[0].status);
        			    if (response.rows[0].elements[0].status != "OK"){
        			    	distance='Not found';
        			    	duration='Not found';
        			    }
        			    else{
        			    	results = response.rows[0].elements;
        			    	distance=results[0].distance.text;
        			    	duration=results[0].duration.text;
        			    }
        	        	var contentString = '<div id="content"><h1>'+_this.model.get('name')+'</h1><p>'+_this.model.get('location')+'</p><p><span>Distance: </span>'+distance+'</p><p><span>ETA: </span>'+duration+'</p> <i class="ss-icon">␡</i></div>';
        	        	var infowindow = new google.maps.InfoWindow({
        	        		content: contentString
        	        	});

        	        	var marker = new google.maps.Marker({
        	        		position: new google.maps.LatLng( lat,lng),
        	        	    map: map,
        	        	    title: _this.model.get('name'),
        	                icon: "images/gmaps/marker.png"
        	        	});
        	        	
        	         	google.maps.event.addListener(marker, 'click', function() {
        	         		infowindow.open(map,marker);
        	         	});
        	        	 
        	            google.maps.event.addListener(infowindow, 'closeclick', function() {
        	            	openedInfoWindow = null; 
        	            });
        	         	
        			  }
        			}

        			function deleteOverlays() {
        			  for (var i = 0; i < markersArray.length; i++) {
        			    markersArray[i].setMap(null);
        			  }
        			  markersArray = [];
        			}
        	
       	    });
        	     	 
        },
        

        render: function(){
            var _this = this;
            console.log(parseFloat(_this.model.get("lat")));
            BaseView.prototype.render.call(_this, function(){
                _this.renderMap(_this,parseFloat(_this.model.get("lat")), parseFloat(_this.model.get("lng")));
            });
            return _this;
        },

        events: function(){
          return _.extend({},BaseView.prototype.events,{
          });
        }

    });

    // Returns the View class
    return View;

} );