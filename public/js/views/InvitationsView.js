
define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",

        "views/InvitationSubview",
        "models/InvitationModel",

        "jquery", 
        "jqueryvalidate",

        ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        Subview,
        Model,

        $ ) {


    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
        },

        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){

                _.each(UserSession.get("invitations"), function(data)
                {
                        var model = new Model(data);
                        var subview = new Subview({
                            "name" : "invitation",
                            "model" : model
                        });


                        _this.$el.find(".feed").append(subview.render().$el);
                });
                
            });
            return _this;
        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{

          });
        },
        sync: function(_this){
            console.log("sync");
            if( typeof UserSession.changed.invitations !== 'undefined' ){
                console.log("invitations changed");
                if(UserSession.get("invitations") == "0")
                    window.changePage("#invite", "left");
                else
                {
                    console.log("invitations re-rendered");
                    _this.render();
                }
                    
            }
        }

    });

    // Returns the View class
    return View;

} );


