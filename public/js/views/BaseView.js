define([ "backbone",
        "scrollfix",
        "models/BaseModel",
        "models/UserSession",
        "views/dialogs/AlertDialogView", 
        "views/dialogs/PromptDialogView",
        "language",
        "jquery",
        "jqueryvalidate" ], function( 
        Backbone,
        ScrollFix,
        BaseModel,
        UserSession,
        AlertDialogView, 
        PromptDialogView,
        Language,
        $ ) {

    var BaseView = Backbone.View.extend( {

        model: new BaseModel,
        initialize: function(){
            $(".footer").hide();
        },
        destroy: function(){

        },
        form: {
            setup: function(_config){
                var config = {
                    "form_selector" : _config.form_selector || "form",
                    "submit_selector" : _config.submit_selector || null,
                    "validation" : _config.validation || "none"
                };
                var _this = this;
                this.form.$e = $(this.el).find(config.form_selector);
                if( config.validation != "none" ){
                    this.form.$e.validate({
                        errorPlacement: function(error, element){
                            $(element).attr("placeholder", error.text());
                            
                            $(element).parent().find("i").remove();
                            $(element).parent().append("<i class='ss-dislike hatch error'></i>");
                        },
                        success: function(error, element){
                            $(element).parent().find("i").remove();
                            $(element).parent().append("<i class='ss-like valid'></i>");
                        },
                        rules: Language.view[config.validation].validation.rules,
                        messages: Language.view[config.validation].validation.messages,
                        submitHandler: function(form){
                            _this.form.request($(form).serialize(), _this);
                        }
                    });
                    if( config.submit_selector != null )
                        $(this.el).find(config.submit_selector).on("click", function(){_this.form.$e.submit();});
                }
                else
                {
                    if( config.submit_selector != null )
                        $(this.el).find(config.submit_selector).on("click", function(){_this.form.request(_this.form.$e.serialize(), _this);});
                } 
                this.form.$e.on("submit", function(e){e.preventDefault();});

            },

            request: function(jsondata, _this)
            {
                //override this request with child
            }
        },

        alert: function(config){
            var adv = new AlertDialogView(config);
            adv.popup( "open" );
            return adv;
        },
        prompt: function(config){
            var pdv = new PromptDialogView(config);
            pdv.popup( "open" );
            return pdv;
        },
        
        render: function(callback) {
            var _this = this;

            $.get("templates/" + _this.config.name + ".html", function(data)
            {
                _this.template = _.template( data );
                var args;
                if( typeof Language.view[_this.config.name] !== 'undefined'){
                    args = _.extend( {"strings": Language.view[_this.config.name].strings}, _this.model.toJSON());
                }
                else
                    args = _this.model.toJSON();

                $(_this.el).html(_this.template(args));
                callback();
                _this.afterRender();
            });
            return _this;
        },
        genkey: function(){
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for( var i=0; i < 5; i++ )
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        },
        afterRender: function(){
            var _this = this;

            $("img").each(function(){
                $(this).attr("src", $(this).attr("src") + "?unique=" + _this.genkey() );
            });

            $("[data-role=page] > *").not("[data-role=content]").on("touchmove", function(event){
                event.preventDefault();
            });
            $("[data-role=content]").bind('touchmove', function (e){
                 var currentY = e.touches[0].clientY;
                 if(currentY > lastY){  
                     //moved down
                     console.log("swipe down");
                 }else{
                     console.log("swipe up");
                 }
                 lastY = currentY;
            });
            new ScrollFix($(this.el).find("[data-role=content]").get(0));
            $(this.el).find("input").not("[type=radio]").not("[type=hidden]").not("[type=file]").wrap("<div class='ui-input'></div>")
            $(this.el).find("input").on("click touch focus select", function(){
                    console.log( $("[data-role=content]").scrollTop() );
                    console.log( $(this).offset().top );
                    $("[data-role=content]").scrollTop($("[data-role=content]").scrollTop() + $(this).offset().top - 60);
            })
            this.$el.find("input").not("[type=radio]").not("[type=hidden]").not("[type=file]").each(function(){
                $(this).parent().append("<a class='clr ss-delete'></a>");
                if($(this).val() != '')
                    $(this).parent().find('.clr').removeClass("hideall");
                else
                    $(this).parent().find('.clr').addClass("hideall");

                $(this).parent().find('.clr').on('click', function(){
                    $(this).parent().find('input').val('');
                    $(this).removeClass("hideall");
                     $(this).parent().find('input').valid();
                });

            });
            this.$el.find("input").not("[type=radio]").not("[type=hidden]").not("[type=file]").on("keyup", function(){
                    $(this).parent().find('.clr').removeClass("hideall");
                $(this).valid();
            });
            $(this.el).find(".disable-auto")
            .attr("autocomplete","off")
            .attr("autocorrect","off")
            .attr("autocapitalize","off")
            .attr("spellcheck","false");

            $(this.el).find("a[data-page-transition]").on("click", function(e){
                e.preventDefault();
                console.log($(this).attr("data-page-transition"));
                window.changePage($(this).attr("href"), $(this).attr("data-page-transition"));
            });
            $(this.el).find("a[href]").not("[data-page-transition]").on("click", function(e){
                e.preventDefault();
                window.changePage($(this).attr("href"), "left");
            });
            $(this.el).find(".logout").on("click", function(){
                BaseView.prototype.alert.call(this, Language.view["default"].dialogs.failsafeLogout)
                .$primary_btn.on("click", function(){
                    UserSession.logout({
                        200: function(){
                            window.changePage("#", "right");
                            UserSession.clearInterval();
                        }
                    });
                });

            });
            var _this = this;
            $("body").off("updateInterval");
            $("body").on("updateInterval", function(){
                _this.sync(_this);
            });

            $(".back").click(function(){
                window.router.direction = false;
                history.back();
            });

            var delay = (function(){
              var timer = 0;
              return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
              };
            })();
            var t, l = (new Date()).getTime();
            $("[data-role=content]").scroll(function(){
                var now = (new Date()).getTime();

                if(now - l > 400){
                    $(this).trigger('scrollStart');
                    l = now;
                }

                delay(function(){
                    $(".footer").removeClass("ignore");
                },1000);
            });

            $(this.el).trigger("afterRender");
            var lastY;
            $("form").each(function(){
                console.log("trying to append");
                $(this).append("<input type='submit' style='width: 0; height: 0; z-index: -2; top: -10000px; left: -100000px; position: absolute; opacity: 0;'>");
            });
        },

        loadFooter: function(link){
            $(".footer").show();
            $(".footer a").removeClass("active");
            $(".footer a[href='#"+link+"']").addClass("active");

            var _this = this;

            $(this.el).on("afterRender", function(){
                $(_this.el).find(".ui-input").on("click", function(e){
                    if( !$(this).data("focused") ){
                        $(".footer").hide();

                        $(this).find("input,textarea,select").removeClass("no-touch");

                        $(this).data("focused",true);

                        $(this).find("input,textarea,select").select().focus();

                        $(this).find("select").change(function(){
                            $(this).parents(".full-btn").html($(this).val());
                        });

                    }
                });
                $(_this.el).find("input,textarea,select").not("[type=file]").not("[type=radio]")
                .on("blur", function(){
                    $(".footer").show();
                    $(this).addClass("no-touch");
                    $(this).parent().data("focused",false);
                });
                $("select").change(function(){
                    $(".footer").show();
                    $(this).addClass("no-touch");
                    $(this).parent().data("focused",false);
                });
                $(_this.el).find("input,textarea,select").not("[type=file]").not("[type=radio]").not("[type=hidden]").addClass("no-touch");
            });

            $(window).on('scrollStart', function(){
                $(".footer").addClass("ignore");
            });
  

        },

        events: {

        },



        sync: function(){
            
        }

    } );

    // Returns the View class
    return BaseView;

} );