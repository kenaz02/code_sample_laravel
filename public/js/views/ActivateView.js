define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",
        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        $ ) {


    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.call(this);
            this.config = config;
            this.id = this.config.name;
        },

        form: {
            request: function(jsondata, _this)
            {
                UserSession.activate(jsondata, 
                {
                    400: function(jd){_this.form.invalidKey(jd, _this)},
                    200: function(jd){_this.form.keyValid(jd, _this)},
                });
            },
            invalidKey: function(jsondata, _this) { 
                BaseView.prototype.alert.call(this, Language.view[_this.config.name].dialogs.invalidKey);
            },
            keyValid: function(jsondata, _this) { 
                window.changePage("#setpwd", "left");
            },
        },

        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){
                BaseView.prototype.form.setup.call(_this, {
                    "submit_selector" : "#submit"
                }); 
                $(_this.el).trigger("render");
            });
            return _this;
        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{

          });
        }

    });

    // Returns the View class
    return View;

} );