
define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",
        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        $ ) {


    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
            this.model = UserSession;
        },
        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){
                BaseView.prototype.form.setup.call(_this, {
                    "submit_selector" : "#submit"
                });
            });
            return _this;
        },

       form: {
            request: function(jsondata, _this)
            {
                UserSession[_this.config.name](jsondata, 
                {
                    200: function(jd){_this.form.success(jd, _this)}
                });
            },
            success: function(jsondata, _this) { 
                UserSession.fetch({
                    success: function(){


                        /********************
                         Update reminder
                         ********************/

                        //Set reminder1 and reminder2 and message1 and message2
                        rem1=UserSession.get("reminder1");
                        rem2=UserSession.get("reminder2");

                        if (rem1 == 1){min1=0;msg1 = Language.view["reminders"].notifications.time_event;}
                        if (rem1 == 2){min1=5;msg1 = Language.view["reminders"].notifications.mins5;}
                        if (rem1 == 3){min1=15;msg1 = Language.view["reminders"].notifications.mins15;}
                        if (rem1 == 4){min1=30;msg1 = Language.view["reminders"].notifications.mins30;}
                        if (rem1 == 5){min1=60;msg1 = Language.view["reminders"].notifications.hours1;}
                        if (rem1 == 6){min1=120;msg1 = Language.view["reminders"].notifications.hours2;}
                        if (rem1 == 7){min1=60 * 24;msg1 = Language.view["reminders"].notifications.days1;}
                        if (rem1 == 8){min1=60 * 24 * 2;msg1 = Language.view["reminders"].notifications.days2;}
                        if (rem1 == 9){min1=60 * 24 * 7;msg1 = Language.view["reminders"].notifications.days7;}

                        if (rem2 == 1){min2=0;msg2 = Language.view["reminders"].notifications.time_event;}
                        if (rem2 == 2){min2=5;msg2 = Language.view["reminders"].notifications.mins5;}
                        if (rem2 == 3){min2=15;msg2 = Language.view["reminders"].notifications.mins15;}
                        if (rem2 == 4){min2=30;msg2 = Language.view["reminders"].notifications.mins30;}
                        if (rem2 == 5){min2=60;msg2 = Language.view["reminders"].notifications.hours1;}
                        if (rem2 == 6){min2=120;msg2 = Language.view["reminders"].notifications.hours2;}
                        if (rem2 == 7){min2=60 * 24;msg2 = Language.view["reminders"].notifications.days1;}
                        if (rem2 == 8){min2=60 * 24 * 2;msg2 = Language.view["reminders"].notifications.days2;}
                        if (rem2 == 9){min2=60 * 24 * 7;msg2 = Language.view["reminders"].notifications.days7;}

                        /*window.plugin.notification.local.getScheduledIds(function(scheduledIds) {
                            console.log('1 - Get schedules notification');
                            for (var i = 0; scheduledIds.length > i; i++) {
                                //window.plugin.notification.local.cancel(scheduledIds[i]);
                                console.log(scheduledIds[i]);
                            }
                        });*/

                        window.setTimeout(function(){
                            //Cancel all local notification
                            window.plugin.notification.local.cancelAll(function () {
                                //At this point all notifications have been canceled
                                //console.log('2 - Cancel all notification');
                            });
                        }, 1000);

                        /*window.setTimeout(function(){
                            window.plugin.notification.local.getScheduledIds(function(scheduledIds) {
                                console.log('3 - Get schedules notification');
                                for (var i = 0; scheduledIds.length > i; i++) {
                                    console.log(scheduledIds[i]);
                                }
                            });
                        }, 2000);*/

                        window.setTimeout(function(){

                            console.log('4 - Reset all notification');

                            //Set new notification
                            var idNotification = 0;
                            var now = new Date;
                            for(var i in UserSession.get("events")) {
                                //Don't send notification for events deleted
                                console.log(UserSession.get("events")[i].name);
                                if (UserSession.get("events")[i].flag != 1028){
                                    //new Date(year, month, day [, hour, minute, second, millisecond ])
                                    var arr = ( UserSession.get("events")[i].date + " " + UserSession.get("events")[i].time + ":00" ).split(/[- :]/);
                                    var reminderEvent = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                    var reminderDate1 = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                    var reminderDate2 = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                    msg1name = msg1 + "\n'" + UserSession.get("events")[i].name+"'";
                                    msg2name = msg2 + "\n'" + UserSession.get("events")[i].name+"'";
                                    //Send notification only for future events
                                    if(reminderEvent > now) {
                                        if (rem1 != 0) {
                                            reminderDate1.setMinutes(reminderEvent.getMinutes() - min1);
                                            if(reminderDate1 > now){
                                                console.log('Set not 1, time '+reminderDate1);
                                                window.plugin.notification.local.add({
                                                    id:         idNotification,  // A unique id of the notifiction
                                                    date:       reminderDate1,    // This expects a date object
                                                    message:    msg1name,  // The message that is displayed
                                                    //title:      'Held up',  // The title of the message
                                                    badge:      1,  // Displays number badge to notification
                                                    //autoCancel: Boolean, // Setting this flag and the notification is automatically canceled when the user clicks it
                                                });
                                                idNotification = idNotification + 1;
                                            }
                                        }
                                        if (rem2 != 0) {
                                            reminderDate2.setMinutes(reminderEvent.getMinutes() - min2);
                                            if(reminderDate2 > now) {
                                                console.log('Set not 2, time '+reminderDate2);
                                                window.plugin.notification.local.add({
                                                    id:         idNotification,  // A unique id of the notifiction
                                                    date:       reminderDate2,    // This expects a date object
                                                    message:    msg2name,  // The message that is displayed
                                                    //title:      'Held up',  // The title of the message
                                                    badge:      1,  // Displays number badge to notification
                                                    //autoCancel: Boolean, // Setting this flag and the notification is automatically canceled when the user clicks it
                                                });
                                                idNotification = idNotification + 1;
                                            }
                                        }
                                    }
                                }
                            }

                        }, 3000);

                        /*window.setTimeout(function(){
                            window.plugin.notification.local.getScheduledIds(function(scheduledIds) {
                                console.log('5 - Get schedules notification');
                                for (var i = 0; scheduledIds.length > i; i++) {
                                    console.log(scheduledIds[i]);
                                }
                            });
                        }, 4000);
                        */


                        BaseView.prototype.alert.call(_this, Language.view["reminders"].dialogs["remindersUpdated"]);
                    }
                });
            }
        },
        reminderChanged: function(e){
            console.log($(e.target).val());
            if( $(e.target).val() != "0" ){
                this.$el.find("[name=reminder-secondary]").parents(".selectbox").removeClass("hide");
            }
            else
            {
                 this.$el.find("[name=reminder-secondary]").parents(".selectbox").addClass("hide");
            }
        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{
            "change [name=reminder-first]" : "reminderChanged"
          });
        }

    });

    // Returns the View class
    return View;

} );