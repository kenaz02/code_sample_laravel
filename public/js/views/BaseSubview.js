define([ "backbone", 
        "models/BaseModel",
        "models/UserSession",
        "views/dialogs/AlertDialogView", 
        "views/dialogs/PromptDialogView",
        "language",
        "jquery",
        "jqueryvalidate" ], function( 
        Backbone,
        BaseModel,
        UserSession,
        AlertDialogView, 
        PromptDialogView,
        Language, 
        $ ) {

    var BaseSubview = Backbone.View.extend( {


        model: new BaseModel,
        initialize: function(){
        },
        destroy: function(){

        },
        form: {
            setup: function(_config){
                var config = {
                    "form_selector" : _config.form_selector || "form",
                    "submit_selector" : _config.submit_selector || null,
                    "validation" : _config.validation || "none"
                };
                var _this = this;
                this.form.$e = $(this.el).find(config.form_selector);
                if( config.validation != "none" ){
                    this.form.$e.validate({
                        errorPlacement: function(error, element){
                            $(element).attr("placeholder", error.text());
                            
                            $(element).parent().find("i").remove();
                            $(element).parent().append("<i class='ss-dislike hatch error'></i>");
                        },
                        success: function(error, element){
                            $(element).parent().find("i").remove();
                            $(element).parent().append("<i class='ss-like valid'></i>");
                        },
                        rules: Language.view[config.validation].validation.rules,
                        messages: Language.view[config.validation].validation.messages,
                        submitHandler: function(form){
                            _this.form.request($(form).serialize(), _this);
                        }
                    });
                    this.form.$e.find("input").on("keyup", function(){$(this).valid();});
                    if( config.submit_selector != null )
                        $(this.el).find(config.submit_selector).on("click", function(){_this.form.$e.submit();});
                }
                else
                {
                    if( config.submit_selector != null )
                        $(this.el).find(config.submit_selector).on("click", function(){_this.form.request(_this.form.$e.serialize(), _this);});
                } 



                this.form.$e.on("submit", function(e){e.preventDefault();});

            },

            request: function(jsondata, _this)
            {
                //override this request with child
            }
        },

        alert: function(config){
            var adv = new AlertDialogView(config);
            adv.popup( "open" );
            return adv;
        },
        prompt: function(config){
            var pdv = new PromptDialogView(config);
            pdv.popup( "open" );
            return pdv;
        },
        render: function(callback) {
            var _this = this;

            $.get("templates/" + _this.config.name + ".html", function(data)
            {         
                clearTimeout (window.timer);
                window.timer = setTimeout(function(){
                    $(window).trigger("svAfterRender");
                }, 25);

                _this.template = _.template( data );

                var args;
                if( typeof Language.view[_this.config.name] !== 'undefined'){
                    args = _.extend( {"strings": Language.view[_this.config.name].strings}, _this.model.toJSON());
                }
                else
                    args = _this.model.toJSON();

                $(_this.el).html(_this.template(args));
                
                callback();
                _this.afterRender();
            });
            return _this;
        },

        afterRender: function(){
            var _this = this;
            $(this.el).find("input").not("[type=radio]").wrap("<div class='ui-input'></div>");
            $(this.el).find("a[data-page-transition]").on("click", function(e){
                e.preventDefault();
                window.changePage($(this).attr("href"), $(this).attr("data-page-transition"));
            });

            $(this.el).trigger("afterRender");

        },

        events: {

        }

    } );

    // Returns the View class
    return BaseSubview;

} );