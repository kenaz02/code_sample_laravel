define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",
        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,
        $ ) {


    var View = BaseView.extend( {
        model: UserSession,
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
        },
        cancel: function(){
            var _this = this;
            BaseView.prototype.alert.call(this, Language.view[_this.config.name].dialogs.failsafeCancel)
            .$primary_btn.on("click", function(){
                _this.model.deleteInvitation({
                    200: function(jd){_this.form.cancelTriggered(jd, _this)},
                });
            });
            
        },
        form: {
            cancelTriggered: function(jsondata, _this) { 
                UserSession.fetch({
                    success: function(){
                        window.changePage("#dashboard", "left");
                    }
                });

            }
        },
        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){});
            return _this;
        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{
            "click .cancel" : "cancel"
          });
        },

        sync: function(){
            console.log("UserSession updated.");
            if( UserSession.get("partner") != "0" || 
                UserSession.get("invitation") == "0" ){
                //$("body").off("updateInterval");
                window.changePage("#dashboard", "left");
            }
        }
    });
    // Returns the View class
    return View;

});