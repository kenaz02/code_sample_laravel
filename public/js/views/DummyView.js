define(["backbone", 
        "views/BaseView",
        "models/UserSession",
        "language",
        "jquery", 
        "jqueryvalidate", ], function( 
        Backbone,
        BaseView,
        UserSession,
        Language,

        $ ) {


    var View = Backbone.View.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = "dummy";
        },

        render: function(){
            var _this = this;

            $.get("templates/" + _this.config.name + ".html", function(data)
            {
                $(_this.el).html(data);
                _this.afterRender();
            });
            return _this;
        },
        afterRender: function(){
            
        }
    });

    // Returns the View class
    return View;

} );