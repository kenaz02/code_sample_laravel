
define(["backbone",
        "views/BaseView",
        "models/UserSession",
        "config",
        "language",
        "jquery",
        "jqueryvalidate", ], function(
        Backbone,
        BaseView,
        UserSession,
        Config,
        Language,
        $ ) {


    var View = BaseView.extend( {
        initialize: function(config) {
            BaseView.prototype.initialize.apply(this);
            this.config = config;
            this.id = this.config.name;
            this.model = UserSession;
        },

        form: {
            request: function(jsondata, _this)
            {
                UserSession[_this.config.name](jsondata,
                {
                    200: function(jd){_this.form.success(jd, _this)}
                });
            },
            success: function(jsondata, _this) {
                UserSession.fetch({
                    success: function(){
                        BaseView.prototype.alert.call(_this, Language.view["profile"].dialogs["profileUpdated"]);
                    }
                });
            },
        },

        render: function(){
            var _this = this;
            BaseView.prototype.render.call(_this, function(){
                if( UserSession.get("avatar") != "" ){
                    $("#camera .avatar").attr( "src", Config.network.url('/') + UserSession.get("avatar") );
                    console.log(Config.network.url('/') + UserSession.get("avatar"));
                    $("#camera i").remove();
                }
                BaseView.prototype.form.setup.call(_this, {
                    "submit_selector" : "#submit",
                    "validation" : _this.config.name
                });

                $(_this.el).trigger("render");

                $("#camera").on("click", function(){
                    //$("#upload").click();
                    navigator.camera.getPicture(
                        function(imageURI){
                            $("#camera .avatar").attr("src", "data:image/jpeg;base64," + imageURI);
                            $("#camera input[name=avatar]").val("data:image/jpeg;base64," + imageURI);
                            $("#camera i").remove();
                        }, function(message) {
                            alert('Failed because: ' + message);
                        }, { quality: 100, allowEdit : true,
                            destinationType: Camera.DestinationType.DATA_URL,
                            targetWidth: 200, targetHeight: 200
                        });
                });
                $("#upload").off("change").on('change',function(){

                    /*
                    var ctxxx = $("#canvasImage")[0].getContext("2d");
                    ctxxx.clearRect(0,0,1000,1000);
                    var input = $(this).get(0);
                    if (input.files && input.files[0])
                    {
                      var fr = new FileReader, reader = new FileReader;
                      var exif;

                      fr.readAsBinaryString(input.files[0]);
                      fr.onloadend = function() {
                          exif = EXIF.readFromBinaryFile(new BinaryFile(this.result));
                          console.log("onloadend")
                          reader.readAsDataURL(input.files[0]);
                      };

                      reader.onload = function (e)
                      {

                            $("#camera .avatar").attr("src", e.target.result);
                            $("#camera .avatar").one("load", function(){
                            console.log('loaded avatar: orientation(' + exif.Orientation  + ')')
                            var canvas = $('#canvasImage')[0];
                            var ctx = canvas.getContext("2d");
                            var img = $("#camera .avatar")[0];

                            ctx.drawImage(img, 0, 0);

                            var MAX_WIDTH = 800;
                            var MAX_HEIGHT = 600;
                            var width = img.width;
                            var height = img.height;

                            if (width > height) {
                            if (width > MAX_WIDTH) {
                            height *= MAX_WIDTH / width;
                            width = MAX_WIDTH;
                            }
                            } else {
                            if (height > MAX_HEIGHT) {
                            width *= MAX_HEIGHT / height;
                            height = MAX_HEIGHT;
                            }
                            }
                            canvas.width = width;
                            canvas.height = height;
                            var ctx = canvas.getContext("2d");
                            switch(exif.Orientation){
                                case 8:
                                    ctx.translate(width/2, height/2);
                                    ctx.rotate(-90 * (Math.PI/180) )
                                    ctx.translate(-width/2, -height/2);
                                    break;
                                case 3:
                                    ctx.translate(width/2, height/2);
                                    ctx.rotate(90 * (Math.PI/180) )
                                    ctx.translate(-width/2, -height/2);
                                    break;
                                case 6:
                                    ctx.translate(width/2, height/2);
                                    ctx.rotate(90 * (Math.PI/180) )
                                    ctx.translate(-width/2, -height/2);
                                    break;
                                case 1:
                                    break;
                            }
                            ctx.drawImage(img, 0, 0, width, height);

                            var dataurl = canvas.toDataURL("image/png");
                            $("#camera .avatar").attr("src", dataurl);
                            $("#camera input[name=avatar]").val(dataurl);
                            $("#camera i").remove();



                          });
                        };

                    }
                    */
                });
            });
            return _this;
        },
        events: function(){
          return _.extend({},BaseView.prototype.events,{
          });
        }

    });

    // Returns the View class
    return View;

} );
